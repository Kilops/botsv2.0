# -*- coding: utf-8 -*-

def findquestion(num):
    """
    Функция игры "Тест", которая получает номер вопроса, находит по этому номеру текст вопроса и возвращает его.

    num -- номер вопроса, который нужно найти

    returns: question -- текст вопроса
    """
    if num == 1:
        question = "Кто разработал первый работающий печатный станок?" \
                   "\n1)Исаак Ньютон\n2)Галилео Галилей\n3)Альберт Эйнштейн\n4)Иоганн Гутенберг"
        return question
    elif num == 2:
        question = "Какой континент покрыт льдом?" \
                   "\n1)Антарктида\n2)Азия\n3)Северная Америка\n4)Африка"
        return question
    elif num == 3:
        question = "Бёф бургиньон (говядина по-бургундски) - тушеное мясо, приготовленное из тушеной говядины в ..." \
                   "\n1)Белом вине\n2)Ржаном виски\n3)Бурбоне\n4)Красном вине"
        return question
    elif num == 4:
        question = "Кто был первым человеком, который ступил на Луну?" \
                   "\n1)Юрий Гагарин\n2)Джон Гленн\n3)Нил Армстронг\n4)Алан Шепард"
        return question
    elif num == 5:
        question = "Кого называли <Дуче> его последователи? Титул от лат. dux («лидер, вождь») " \
                   "\n1)Бенито Муссолини\n2)Итало Бальбо\n3)Адольф Гитлер\n4)Иосиф Сталин"
        return question
    elif num == 6:
        question = "Кто написал <<Старик и море>>?" \
                   "\n1)Сэлинджер\n2)Ф. Скотт Фицджеральд\n3)Марк Твен\n4)Эрнест Хемингуэй"
        return question
    elif num == 7:
        question = "Какой из этих городов находится севернее?" \
                   "\n1)Милан, Италия\n2)Лондон, Англия\n3)Стокгольм, Швеция\n4)Париж, Франция"
        return question
    elif num == 8:
        question = "Джон Ф. Кеннеди был убит в:" \
                   "\n1)1963\n2)1973\n3)1983\n4)1953"
        return question
    elif num == 9:
        question = "Столица Австрии?" \
                   "\n1)Вена\n2)Брюссель\n3)Минск\n4)Прага"
        return question
    elif num == 10:
        question = "Каким видом спорта известен Андре Агасси?" \
                   "\n1)Теннис\n2)Крикет\n3)Футбол\n4)Бейсбол"
        return question
    elif num == 11:
        question = "На сколько часов могут задерживать дыхание морские котики" \
                   "\n1)Меньше одного часа\n2)Час\n3)Два часа\n4)Больше трёх часов"
        return question
    elif num == 12:
        question = "Какая молодая девушка помогла изгнать англичан с французской земли в 15 веке?" \
                   "\n1)Жанна д'Арк\n2)Хелен Келлер\n3)Мария Королева Шотландии\n4)Елизавета Вторая"
        return question
    elif num == 13:
        question = "От чьего лица ведется рассказ в Великом Гэтсби?" \
                   "\n1)Джордан Бейкер\n2)Том Бьюкенен\n3)Джей Гэтсби\n4)Ник Каррауэй"
        return question
    elif num == 14:
        question = "Где находится Сенегал?" \
                   "\n1)Азия\n2)Африка\n3)Европа\n4)Антарктида"
        return question
    elif num == 15:
        question = "Как Ромео совершает самоубийство?" \
                   "\n1)Ядом\n2)Мечом\n3)Кинжалом\n4)Ножом"
        return question
    elif num == 16:
        question = "Какие членистоногие не являются паукообразными?" \
                   "\n1)Пауки\n2)Скорпионы\n3)Муравьи\n4)Клещи"
        return question
    elif num == 17:
        question = "Кто написал <<Убить Пересмешника>>?" \
                   "\n1)Тони Моррисон\n2)Марк Твен\n3)Харпер Ли\n4)Ф. Скотт Фицджеральд"
        return question
    elif num == 18:
        question = "Столицей какой страны является Джакарта?" \
                   "\n1)Венесуэла\n2)Индонезия\n3)Австралия\n4)Индия"
        return question
    elif num == 19:
        question = "Пабло Пикассо был:" \
                   "\n1)Бразильцем\n2)Итальянцем\n3)Французом\n4)Испанцем"
        return question
    elif num == 20:
        question = "Где находится сердце у креветки?" \
                   "\n1)В груди\n2)В голове\n3)В лапках\n4)В хвосте"
        return question
    elif num == 21:
        question = "Столица Эфиопии?" \
                   "\n1)Могадишо\n2)Найроби\n3)Аддис Абеба\n4)Аккра"
        return question
    elif num == 22:
        question = "Какой напиток из перечисленных ниже используются для изготовления Дайкири?" \
                   "\n1)Джин\n2)Ром\n3)Коньяк\n4)Виски"
        return question
    elif num == 23:
        question = "Сколько секунд память у золотой рыбки?" \
                   "\n1)1\n2)2\n3)3\n4)4"
        return question
    elif num == 24:
        question = "Какой столяр создал Пиноккио?" \
                   "\n1)Чарли Браун\n2)Джеппетто\n3)Пепе\n4)Г-н Магу"
        return question
    elif num == 25:
        question = "С какого возраста львы могут рычать?" \
                   "\n1)1\n2)2\n3)3\n4)4"
        return question
    elif num == 26:
        question = "Кто открыл гравитацию, когда увидел падающее яблоко?" \
                   "\n1)Исаак Ньютон\n2)Чарльз Дарвин\n3)Мария Кюри\n4)Альберт Эйнштейн"
        return question
    elif num == 27:
        question = "Где находится Момбаса?" \
                   "\n1)Эфиопия\n2)Кения\n3)Индия\n4)Марокко"
        return question
    elif num == 28:
        question = "В каком виде спорта прославился Том Брэди?" \
                   "\n1)Бейсбол\n2)Футбол\n3)Теннис\n4)Баскетбол"
        return question
    elif num == 29:
        question = "Сколько красных и белых полосок на американском флаге?" \
                   "\n1)13\n2)11\n3)49\n4)50"
        return question
    elif num == 30:
        question = "Где находится Кот-д'Ивуар?" \
                   "\n1)В Африке\n2)В Азии\n3)В Европе\n4)В Австралии"
        return question
    elif num == 31:
        question = "Древние писания индуизма написаны на:" \
                   "\n1)Иврите\n2)Арабском языке\n3)Санскрите\n4)Персидском"
        return question
    elif num == 32:
        question = "Сьерра-Леоне - страна в:" \
                   "\n1)Европе\n2)Африке\n3)Америке\n4)Азии"
        return question
    elif num == 33:
        question = "Как называется автобиография Адольфа Гитлера?" \
                   "\n1)Моя борьба\n2)Черное сердце\n3)Испытание\n4)Автобиография Адольфа Гитлера"
        return question
    elif num == 34:
        question = "Какие напитки из перечисленных используются для приготовления Мартини?" \
                   "\n1)Водка и вермут\n2)Джин и вермут\n3)Виски и водка\n4)Виски и джин"
        return question
    elif num == 35:
        question = "В информатике, что означает DOS?" \
                   "\n1)Dual Optimum System\n2)Disk Operational Standard\n3)Disk Optimum Standard\n4)Disk Operating System"
        return question
    elif num == 36:
        question = "Какова скорость рыбы-парусника - самой быстрой рыбы? (км/ч)" \
                   "\n1)80\n2)93\n3)100\n4)115"
        return question
    elif num == 37:
        question = "Барбадос является островной страной в..." \
                   "\n1)Карибском бассейне\n2)Центральной Америке\n3)Африке\n4)Азии"
        return question
    elif num == 38:
        question = "Когда открыли Америку?" \
                   "\n1)1492\n2)1592\n3)1692\n4)1792"
        return question
    elif num == 39:
        question = "Сотворение Адама - фресковая живопись..." \
                   "\n1)Рембрандта\n2)Винсента ван Гога\n3)Микеланджело\n4)Сальвадора Дали"
        return question
    elif num == 40:
        question = "Какой нотой жужжат мухи?" \
                   "\n1)До\n2)Ре\n3)Ми\n4)Фа"
        return question
    elif num == 41:
        question = "Сурки свистят когда..." \
                   "\n1)Хотят есть\n2)Им угрожает опасность\n3)Ищут партнера для размножения\n4)Без повода"
        return question
    elif num == 42:
        question = "Вес скольки слонов сопоставим с весом синего кита?" \
                   "\n1)1\n2)2\n3)3\n4)4"
        return question
    elif num == 43:
        question = "Чего нет у лам?" \
                   "\n1)Копыт\n2)Языка\n3)Носа\n4)Хвоста"
        return question
    elif num == 44:
        question = "Столица Аргентины" \
                   "\n1)Сан Пауло\n2)Рио де Жанейро\n3)Сальвадор\n4)Буэнос Айрес"
        return question
    elif num == 45:
        question = "Сколько недель может обходиться без воды верблюд?" \
                   "\n1)1\n2)2\n3)3\n4)4"
        return question
    elif num == 46:
        question = "Скольки км/ч максимально может достигать скорость стрекоз - самых быстрых насекомых?" \
                   "\n1)10\n2)12\n3)23\n4)100"
        return question
    elif num == 47:
        question = "Где зародилось христианство?" \
                   "\n1)Иудея\n2)Германия\n3)Египет\n4)Древняя Русь"
        return question
    elif num == 48:
        question = "Сколько крылья пчелы совершают взмахов в минуту?" \
                   "\n1)меньше 1000\n2)5132\n3)11400\n4)Около 100000"
        return question
    elif num == 49:
        question = "Во сколько раз в козьем молоке меньше жира, чем в коровьем?" \
                   "\n1)В 2\n2)В 3\n3)В 4\n4)В 5"
        return question
    elif num == 50:
        question = "Сколько яиц в год в среднем откладывает одна курица?" \
                   "\n1)50\n2)190\n3)365\n4)больше 600"
        return question
    elif num == 51:
        question = "Какое растение можно найти в пустыне?" \
                   "\n1)Конский щавель\n2)Пастушья сумка\n3)Верблюжья колючка\n4)Заячья капуста"
        return question
    elif num == 52:
        question = "На побережье какого моря стоит Тель-Авив?" \
                   "\n1)Аравийского\n2)Мертвого\n3)Средиземного\n4)Красного"
        return question
    elif num == 53:
        question = "Где находится гробница пророка Мухаммеда?" \
                   "\n1)Медина\n2)Иерусалим\n3)Каир\n4)Мекка"
        return question
    elif num == 54:
        question = "Кем был пёс Шарик из Простоквашино?" \
                   "\n1)Дворнягой\n2)Овчаркой\n3)Сеттером\n4)Болонкой"
        return question
    elif num == 55:
        question = "На кого бежит зверь в известной пословице?" \
                   "\n1)На дудца\n2)На стрельца\n3)На живца\n4)На ловца"
        return question
    elif num == 56:
        question = "Как еще называют кадык на шее мужчины?" \
                   "\n1)Адамов помидор\n2)Адамова груша\n3)Адамово яблоко\n4)Адамов абрикос"
        return question
    elif num == 57:
        question = "Как в дореволюционной России назывался письменный вызов на дуэль?" \
                   "\n1)Картель\n2)Синдикат\n3)Концерн\n4)Трест"
        return question
    elif num == 58:
        question = "Какие московские пруды одно время назывались Пионерскими?" \
                   "\n1)Патриаршие\n2)Сарлетские\n3)Чистые\n4)Борисовские"
        return question
    elif num == 59:
        question = "Разновидностью какой ягоды в быту называют аронию черноплодную?" \
                   "\n1)Рябины\n2)Земляники\n3)Смородины\n4)Малины"
        return question
    elif num == 60:
        question = "Какая карточная масть старше остальных в преферансе?" \
                   "\n1)Черви\n2)Пики\n3)Трефы\n4)Бубны"
        return question
    elif num == 61:
        question = "Какой из этих городов является столицей Сербии?" \
                   "\n1)Варшава\n2)Белград\n3)Берн\n4)Вильнюс"
        return question
    elif num == 62:
        question = "Игрой на каком инструменте прославился Джими Хендрикс?" \
                   "\n1)Рояль\n2)Труба\n3)Гитара\n4)Барабаны"
        return question
    elif num == 63:
        question = "Что за животное бандикут?" \
                   "\n1)Птица\n2)Рептилия\n3)Грызун\n4)Сумчатое млекопитающее"
        return question
    elif num == 64:
        question = "Какому государству принадлежит Суэцкий канал?" \
                   "\n1)Китаю\n2)Египту\n3)АОЭ\n4)Сирии"
        return question
    elif num == 65:
        question = "Как звали митрополита, чье имя породило фразеологизм «филькина грамота»?" \
                   "\n1)Феликс\n2)Филидор\n3)Филипп\n4)Филарет"
        return question
    elif num == 66:
        question = "Кого или что изучает герпетолог?" \
                   "\n1)Бабочек\n2)Герпес\n3)Черепах\n4)Лекарственные травы"
        return question
    elif num == 67:
        question = "Какой из этих народов на Руси именовали самоедами?" \
                   "\n1)Ненцев\n2)Якутов\n3)Эвенков\n4)Чукчей"
        return question
    elif num == 68:
        question = "Куда нужно поехать, чтобы увидеть Моаи?" \
                   "\n1)В Мексику\n2)На остров Пасхи\n3)На остров Норфолк\n4)В Новую Зеландию"
        return question
    elif num == 69:
        question = "У какого из этих растений нет колючек?" \
                   "\n1)У малины\n2)У розы\n3)У астры\n4)У шиповника"
        return question
    elif num == 70:
        question = "Сколько колец на олимпийском флаге?" \
                   "\n1)3\n2)5\n3)7\n4)12"
        return question
    elif num == 71:
        question = "В каком продукте содержится больше витамина С, чем в апельсине?" \
                   "\n1)Банан\n2)Клубника\n3)Арбуз\n4)Дыня"
        return question
    elif num == 72:
        question = "Сколько процентов Земли покрывает вода?" \
                   "\n1)94\n2)81\n3)70\n4)45"
        return question
    elif num == 73:
        question = "В каком из этих видов боевых искусств используется меч?" \
                   "\n1)Дзюдо\n2)Айкидо\n3)Кэндо\n4)Карате"
        return question
    elif num == 74:
        question = "С какого дерева собирают фиги?" \
                   "\n1)Бадьян\n2)Инжир\n3)Мирт\n4)Пальма"
        return question
    elif num == 75:
        question = "Как называются клетки крови, которые переносят кислород?" \
                   "\n1)Лейкоциты\n2)Эритроциты\n3)Тромбоциты\n4)Альвеолы"
        return question
    elif num == 76:
        question = "8:2(2+2)=?" \
                   "\n1)1\n2)8\n3)12\n4)16"
        return question
    elif num == 77:
        question = "Как звали греческую богиню охоты?" \
                   "\n1)Диана\n2)Гера\n3)Артемида\n4)Афина"
        return question
    elif num == 78:
        question = "Канцерофобия — это боязнь чего?" \
                   "\n1)Заболеть раком\n2)Бесконечности\n3)Канцтоваров\n4)Канцерогенов"
        return question
    elif num == 79:
        question = "Как сказать по-испански «до свидания»?" \
                   "\n1)Ciao\n2)Au revoir\n3)Adiós\n4)Sampai jumpa"
        return question
    elif num == 80:
        question = "У какого русского политика было прозвище Кукурузник?" \
                   "\n1)Леонид Брежнев\n2)Владимир Ульянов\n3)Юрий Андропов\n4)Никита Хрущев"
        return question
    elif num == 81:
        question = "Какой океан находится у пляжей Калифорнии?" \
                   "\n1)Индийский океан\n2)Атлантический океан\n3)Тихий океан\n4)Северный Ледовитый океан"
        return question
    elif num == 82:
        question = "Где расположена Саграда-Фамилия?" \
                   "\n1)Рим, Италия\n2)Рио-де-Жанейро, Бразилия\n3)Москва, Россия\n4)Барселона, Испания"
        return question
    elif num == 83:
        question = "Что из этого НЕ название футбольного клуба?" \
                   "\n1)Анжи\n2)Ахмат\n3)Алжир\n4)Крылья Советов"
        return question
    elif num == 84:
        question = "Кто такой Тим Кук?" \
                   "\n1)Американский певец\n2)Генеральный директор Apple\n3)Генеральный директор Microsoft\n4)Американский актер"
        return question
    elif num == 85:
        question = "Выберите самый большой по площади субъект Российской Федерации" \
                   "\n1)Красноярский край\n2)Ямало-Ненецкий автономный округ\n3)Республика Саха (Якутия)\n4)Камчатский край"
        return question
    elif num == 86:
        question = "Из чего сделан хумус?" \
                   "\n1)Из гороха\n2)Из нута\n3)Из кунжута\n4)Из булгура"
        return question
    elif num == 87:
        question = "Какое из этих млекопитающих откладывает яйца?" \
                   "\n1)Ехидна\n2)Сумчатый муравьед\n3)Хоботноголовый кускус\n4)Валлаби"
        return question
    elif num == 88:
        question = "Какая из этих птиц умеет летать задом наперед?" \
                   "\n1)Многоцветный лорикет\n2)Колибри\n3)Квезаль\n4)Таких птиц нет"
        return question
    elif num == 89:
        question = "Сколько в среднем весит новорожденная панда?" \
                   "\n1)2 кг\n2)5 кг\n3)800 г\n4)100 г"
        return question
    elif num == 90:
        question = "Какие из этих рыб считаются живыми ископаемыми?" \
                   "\n1)Латимерия и арапаима\n2)Барракуда и бикуда\n3)Окунь-ауха и меланоцет Джонсона\n4)Обыкновенный горчак и морской черт"
        return question
    elif num == 91:
        question = "В каком азиатском городе живут леопарды (да, прямо в парке)?" \
                   "\n1)Сингапур\n2)Мумбаи\n3)Гуанчжоу\n4)Катманду"
        return question
    elif num == 92:
        question = "Полупрозрачные шерстинки белых медведей помогают им сливаться со снегом. А какого цвета языки у этих животных?" \
                   "\n1)Зеленые\n2)Розовые\n3)Красные\n4)Синие"
        return question
    elif num == 93:
        question = "Какое из этих насекомых находится под землей в течение 17 лет, чтобы потом прожить один месяц на поверхности?" \
                   "\n1)Гигантский роющий таракан-носорог\n2)Цикада\n3)Жук-олень\n4)Гигантская уэта"
        return question
    elif num == 94:
        question = "Какая змея считается самой ядовитой на планете?" \
                   "\n1)Черная мамба\n2)Островной ботропс\n3)Бушмейстер\n4)Тайпан"
        return question
    elif num == 95:
        question = "С наступлением темноты самцы горилл остаются спать на земле. Почему?" \
                   "\n1)Они боятся высоты\n2)Они не умеют лазить по деревьям\n3)Они слишком много весят\n4)Самки не пускают их наверх"
        return question
    elif num == 96:
        question = "Какое из этих животных улавливает звуки с помощью рта?" \
                   "\n1)Зооглоссус Гарднера\n2)Индийская кобра\n3)Морской еж\n4)Улитка ахатина"
        return question
    elif num == 97:
        question = "Какого размера самая маленькая рыба в мире?" \
                   "\n1)2,8 мм\n2)5,1 мм\n3)7,9 мм\n4)11,4 мм"
        return question
    elif num == 98:
        question = "В каком из этих мест не живут пингвины?" \
                   "\n1)Южная Америка\n2)Южная Африка\n3)Антарктида\n4)Арктика"
        return question
    elif num == 99:
        question = "Сколько различных вирусов могут вызвать насморк?" \
                   "\n1)1\n2)Примерно 10\n3)Не более 50\n4)Около 100"
        return question
    elif num == 100:
        question = "Какого цвета кости у рыбы сарган" \
                   "\n1)Серые\n2)Зеленые\n3)Желтые\n4)Черные"
        return question
    else:
        return 0


def checkisitright(num, answer):
    """
    Функция игры "Тест", которая проверяет был ли правильным полученный от пользователя ответ на выведенный вопрос.

    num -- номер вопроса, который нужно проверить
    answer -- номер ответа, который дал пользователь

    returns: 1 - если верно, 0 - если неверно.
    """
    if num == 1:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 2:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 3:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 4:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 5:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 6:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 7:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 8:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 9:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 10:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 11:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 12:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 13:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 14:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 15:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 16:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 17:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 18:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 19:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 20:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 21:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 22:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 23:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 24:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 25:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 26:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 27:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 28:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 29:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 30:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 31:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 32:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 33:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 34:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 35:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 36:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 37:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 38:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 39:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 40:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 41:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 42:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 43:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 44:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 45:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 46:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 47:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 48:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 49:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 50:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 51:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 52:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 53:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 54:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 55:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 56:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 57:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 58:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 59:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 60:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 61:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 62:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 63:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 64:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 65:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 66:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 67:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 68:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 69:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 70:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 71:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 72:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 73:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 74:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 75:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 76:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 77:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 78:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 79:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 80:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 81:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 82:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 83:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 84:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 85:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 86:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 87:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 88:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 89:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 90:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 91:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 92:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 93:
        if answer == 2:
            return 1
        else:
            return 0
    elif num == 94:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 95:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 96:
        if answer == 1:
            return 1
        else:
            return 0
    elif num == 97:
        if answer == 3:
            return 1
        else:
            return 0
    elif num == 98:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 99:
        if answer == 4:
            return 1
        else:
            return 0
    elif num == 100:
        if answer == 2:
            return 1
        else:
            return 0

