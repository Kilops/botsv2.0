# -*- coding: utf-8 -*-

import pymysql.cursors
import telebot
from telebot import types

bot = telebot.TeleBot(token='1041130127:AAEv4v1NMoOUUkSUjqmSsH5pgaESAkcW73w', threaded=False)
weaponCost1 = 5
weaponCost2 = 30
weaponCost3 = 50
weaponCost4 = 70
weaponCost5 = 100
weaponCost6 = 150
weaponCost7 = 200
weaponCost8 = 250
weaponCost9 = 300
weaponCost10 = 350
weaponCost11 = 400
weaponCost12 = 550
weaponCost13 = 700
weaponCost14 = 850
weaponCost15 = 1000
armorCost1 = 5
armorCost2 = 30
armorCost3 = 50
armorCost4 = 55
armorCost5 = 75
armorCost6 = 100
armorCost7 = 120
armorCost8 = 150
armorCost9 = 300
armorCost10 = 350
armorCost11 = 400
armorCost12 = 550
armorCost13 = 650
armorCost14 = 800
armorCost15 = 1000

def get_connection():
    """
    Функция, устанавливающая связь с базой данных.

    returns: connection -- данные для соединения
    """
    connection = pymysql.connect(host='vonerval.mysql.pythonanywhere-services.com',
                                 user='vonerval',
                                 password='yaN-x5g-WxA-w9c',
                                 db='vonerval$default',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


def help_string():
    """
    Функция, формирующая строку с помощью.

    returns: string -- сформированная строка
    """
    string = '///Предложить нововведения или сообщить об ошибках можно автору: @placeholder_text///\n\n'
    string += 'Привет 😁. Я - игровой бот 😌. Я могу вас развлечь забавными игрушками 🎮.  '
    string += ' Доступные на данный момент игры:\n'
    string += '🧠 Тест\n'
    string += '🎲 Кости\n'
    string += '⚔ RPG\n'
    string += '📖 Текстовый Квест\n'
    string += '🥚 Разбей Яйцо\n\n'
    string += '🧠 Тест: Я каждый раз буду составлять тест из 5 случайных вопросов. Всего у меня есть 100 вопросов, так что они иногда будут повторяться. '
    string += 'Возможно и такое, что три теста подряд попадется один и тот же вопрос, а какой-то вопрос не попадется и в десятый раз. В случае '
    string += 'правильного ответа добавляется балл. Общее количество баллов я сообщу по завершению теста.\n\n'
    string += '🎲 Кости: Суть игры в удаче. Вы выбираете количество костей и количество бросков. Каждая кость может принести от 1 до 6 очков. '
    string += 'Играть можно со мной или с другом на одном и том же устройстве (например, чтобы решить какой-то спор). Сначала первый игрок (либо бот) бросает '
    string += 'кости указанное количество раз, затем второй. У кого выпало больше очков, тот и победитель. '
    string += 'При игре с ботом вы можете посмотреть кому за все матчи повезло больше раз.\n\n'
    string += '⚔ RPG (дополнительная помощь доступна из самой игры во вкладке "Персонаж"): Чтобы преуспеть в этой игре, необходимо развивать своего персонажа и '
    string += 'одолевать все более и более сильных противников. Всего в игре 15 врагов и последний из них это всемогущее зло, которое терроризирует мир уже десятки лет. '
    string += 'Предстоит попотеть, чтобы его одолеть. Сама по себе игра представляет собой текстовую пошаговую рпг. Нужно обнулить здоровье врага, сохранив при этом свое.\n\n'
    string += '📖 Текстовый Квест: Примите участие в типа захватывающей истории. Никаких подробностей не скажу, просто попробуйте. От каждого вашего '
    string += 'решения будет зависеть судьба героя. При неудаче придется начинать с самого начала. Хватит ли вам терпения и смекалки, чтобы '
    string += 'дойти до конца? В любой момент напишите /start, если захотите прервать игру.\n\n'
    string += '🥚 Разбей Яйцо: это старинная кликер-игра, наверное, основоположник жанра "кликер" в принципе. Суть игры заключается в том, чтобы '
    string += 'кликнуть ровно столько раз, сколько нужно для того, чтобы яйцо разбилось. Интересно посмотреть что там, да? А я скажу. '
    string += 'В яйцах котики!! Они там сидят голодают. Нужно спасти всех котиков, только от тебя зависит их судьба!\n'
    string += 'P.S. Постарайся не спамить слишком много кликов быстро, а то словишь ошибку 420 и телеграм перестанет отправлять твои сообщения. '
    string += 'В идеале лучше кликать не больше раза-двух в секунду, либо очень быстро, но короткими сериями.\n\n'
    string += 'Как вы могли заметить, помимо игр здесь также есть достижения 🏆. Всего их больше 50, но посмотреть их до открытия не получится '
    string += '(разве что спросить у других игроков хехех). Каждое полученное достижение фиксируется в списке, который вызывается кнопкой "Достижения" из главного меню. '
    string += 'Если у вас нет достижений - не печальтесь. Самые тривиальные под силу получить каждому буквально опробовав каждую игру по чуть-чуть. '
    string += 'Некоторые достижения требуют грайнда, а какие-то заработают только самые везучие или находчивые игроки. В скобках рядом с названием достижения указано '
    string += 'сколько человек получило его, а также можно узнать на каком вы месте в списке игроков по количеству достижений.\n\n'
    return string


def update_string():
    """
    Функция, формирующая строку с описанием обновления.

    returns: string -- сформированная строка
    """
    string = 'Версия 0.9\n\nВ РПГ добавлены статистика и достижения! Также в игре "Разбей Яйцо" появился режим глобального яйца. А еще у достижений можно '
    string += 'посмотреть сколько раз их получили другие игроки.'
    return string


def achievements_init(m):
    """
    Функция, сохраняющая очки в игре тест и открывающая достижения.

    m -- сообщение пользователя
    """
    nick = m.from_user.username
    if nick is None:
        nick = 'no nickname'
    connection = get_connection()
    cursor = connection.cursor()
    sql = "UPDATE `start` set isRunning = %s where id = %s"
    cursor.execute(sql, (0, m.chat.id))
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for i in cursor:
        len += 1
    if len == 0:
        sql = "INSERT INTO `achievements`(`id`, `nickname`) VALUES (%s, %s)"
        cursor.execute(sql, (m.chat.id, nick))
    connection.commit()
    connection.close()


def achievements_load(m):
    """
    Функция, загружающая достижения игрока.

    m -- сообщение пользователя

    returns: string -- список достижений
    """
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    nickname = ''
    TEST1 = 0
    TEST2 = 0
    TEST3 = 0
    TEST4 = 0
    TEST5 = 0
    KOSTI1 = 0
    KOSTI2 = 0
    KOSTI3 = 0
    KOSTI4 = 0
    KOSTI5 = 0
    TXT1 = 0
    TXT2 = 0
    TXT3 = 0
    TXT4 = 0
    TXT5 = 0
    EGG1 = 0
    EGG2 = 0
    EGG3 = 0
    EGG4 = 0
    EGG5 = 0
    EGG6 = 0
    EGG7 = 0
    EGG8 = 0
    RPG1 = 0
    RPG2 = 0
    RPG3 = 0
    RPG4 = 0
    RPG5 = 0
    RPG6 = 0
    RPG7 = 0
    RPG8 = 0
    RPG9 = 0
    RPG10 = 0
    RPG11 = 0
    RPG12 = 0
    RPG13 = 0
    RPG14 = 0
    RPG15 = 0
    RPG16 = 0
    RPG17 = 0
    RPG18 = 0
    RPG19 = 0
    RPG20 = 0
    RPG21 = 0
    RPG22 = 0
    RPG23 = 0
    RPG24 = 0
    RPG25 = 0
    RPG26 = 0
    RPG27 = 0
    RPG28 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        nickname = element['nickname'].encode('utf-8')
        TEST1 = element['TEST1']
        TEST2 = element['TEST2']
        TEST3 = element['TEST3']
        TEST4 = element['TEST4']
        TEST5 = element['TEST5']
        KOSTI1 = element['KOSTI1']
        KOSTI2 = element['KOSTI2']
        KOSTI3 = element['KOSTI3']
        KOSTI4 = element['KOSTI4']
        KOSTI5 = element['KOSTI5']
        TXT1 = element['TXT1']
        TXT2 = element['TXT2']
        TXT3 = element['TXT3']
        TXT4 = element['TXT4']
        TXT5 = element['TXT5']
        EGG1 = element['EGG1']
        EGG2 = element['EGG2']
        EGG3 = element['EGG3']
        EGG4 = element['EGG4']
        EGG5 = element['EGG5']
        EGG6 = element['EGG6']
        EGG7 = element['EGG7']
        EGG8 = element['EGG8']
        RPG1 = element['RPG1']
        RPG2 = element['RPG2']
        RPG3 = element['RPG3']
        RPG4 = element['RPG4']
        RPG5 = element['RPG5']
        RPG6 = element['RPG6']
        RPG7 = element['RPG7']
        RPG8 = element['RPG8']
        RPG9 = element['RPG9']
        RPG10 = element['RPG10']
        RPG11 = element['RPG11']
        RPG12 = element['RPG12']
        RPG13 = element['RPG13']
        RPG14 = element['RPG14']
        RPG15 = element['RPG15']
        RPG16 = element['RPG16']
        RPG17 = element['RPG17']
        RPG18 = element['RPG18']
        RPG19 = element['RPG19']
        RPG20 = element['RPG20']
        RPG21 = element['RPG21']
        RPG22 = element['RPG22']
        RPG23 = element['RPG23']
        RPG24 = element['RPG24']
        RPG25 = element['RPG25']
        RPG26 = element['RPG26']
        RPG27 = element['RPG27']
        RPG28 = element['RPG28']
    if nickname != m.from_user.username and m.from_user.username != None:
        sql = "UPDATE `achievements` SET nickname = %s WHERE `id` = %s"
        cursor.execute(sql, (m.from_user.username, m.chat.id))
    elif nickname != m.from_user.username and m.from_user.username is None:
        sql = "UPDATE `achievements` SET nickname = %s WHERE `id` = %s"
        cursor.execute(sql, ('no nickname', m.chat.id))
    string = ''
    if achCount == 0:
        string = 'К сожалению у вас нет достижений. Попробуйте поиграть в мои чудесные игрушки.'
    else:
        if TEST1 == 1:
            num = 0
            sql = "SELECT TEST1 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TEST1'] == 1:
                    num += 1
            string += '🥉Достижение №1 (Обычное):🥉\n"Отличник" (' + str(num) + ')\n'
            string += 'Ответьте верно на все 5 вопросов партии из 5 в игре "Тест"\n\n'
        if TEST2 == 1:
            num = 0
            sql = "SELECT TEST2 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TEST2'] == 1:
                    num += 1
            string += '🤡Достижение №2 (никакое):🤡\n"Ты хоть пытался?" (' + str(num) + ')\n'
            string += 'Ответьте неверно на 5 вопросов партии из 5 в игре "Тест"\n\n'
        if TEST3 == 1:
            num = 0
            sql = "SELECT TEST3 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TEST3'] == 1:
                    num += 1
            string += '🥈Достижение №3 (Необычное):🥈\n"Уже не новичок" (' + str(num) + ')\n'
            string += 'Ответье верно суммарно на 30 вопросов в игре "Тест"\n\n'
        if TEST4 == 1:
            num = 0
            sql = "SELECT TEST4 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TEST4'] == 1:
                    num += 1
            string += '🥇Достижение №4 (Редкое):🥇\n"Притворяющийся умником" (' + str(num) + ')\n'
            string += 'Ответье верно суммарно на 100 вопросов в игре "Тест"\n\n'
        if TEST5 == 1:
            num = 0
            sql = "SELECT TEST5 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TEST5'] == 1:
                    num += 1
            string += '🏅🏅Достижение №5 (НЕВЕРОЯТНОЕ):🏅🏅\n"Всезнайка" (' + str(num) + ')\n'
            string += 'Ответье верно суммарно на 1000 вопросов в игре "Тест"\n\n'
        if KOSTI1 == 1:
            num = 0
            sql = "SELECT KOSTI1 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['KOSTI1'] == 1:
                    num += 1
            string += '🥉Достижение №6 (Обычное):🥉\n"Пластмассовый мир победил" (' + str(num) + ')\n'
            string += 'Бот одолел вас 10 раз в игре "Кости"\n\n'
        if KOSTI2 == 1:
            num = 0
            sql = "SELECT KOSTI2 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['KOSTI2'] == 1:
                    num += 1
            string += '🥉Достижение №7 (Обычное):🥉\n"ИИ не пройдет" (' + str(num) + ')\n'
            string += 'Вы одолели бота 10 раз в игре "Кости"\n\n'
        if KOSTI3 == 1:
            num = 0
            sql = "SELECT KOSTI3 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['KOSTI3'] == 1:
                    num += 1
            string += '🥈Достижение №8 (Необычное):🥈\n"Чертов рандом" (' + str(num) + ')\n'
            string += 'Бот одолел вас 100 раз в игре "Кости"\n\n'
        if KOSTI4 == 1:
            num = 0
            sql = "SELECT KOSTI4 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['KOSTI4'] == 1:
                    num += 1
            string += '🥇Достижение №9 (Редкое):🥇\n"Обожаю этот рандом" (' + str(num) + ')\n'
            string += 'Вы одолели бота 100 раз в игре "Кости"\n\n'
        if KOSTI5 == 1:
            num = 0
            sql = "SELECT KOSTI5 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['KOSTI5'] == 1:
                    num += 1
            string += '🥉Достижение №10 (Обычное):🥉\n"Никто не в обиде" (' + str(num) + ')\n'
            string += 'Вы разыграли с ботом ничью в игре "Кости"\n\n'
        if TXT1 == 1:
            num = 0
            sql = "SELECT TXT1 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TXT1'] == 1:
                    num += 1
            string += '🥈Достижение №11 (Необычное):🥈\n"Сапер" (' + str(num) + ')\n'
            string += 'Вы выбрались на свободу путем выбора нужного ящика в игре "Текстовый Квест"\n\n'
        if TXT2 == 1:
            num = 0
            sql = "SELECT TXT2 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TXT2'] == 1:
                    num += 1
            string += '🏅🏅Достижение №12 (НЕВЕРОЯТНОЕ):🏅🏅\n"Спидранер" (' + str(num) + ')\n'
            string += 'Вы прошли игру "Текстовый Квест" за наименьшее количество действий - 14.\n\n'
        if TXT3 == 1:
            num = 0
            sql = "SELECT TXT3 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TXT3'] == 1:
                    num += 1
            string += '🤡Достижение №13 (никакое):🤡\n"С первого раза ни у кого не получалось..." (' + str(num) + ')\n'
            string += 'Вы погибли 10 раз в игре "Текстовый Квест"\n\n'
        if TXT4 == 1:
            num = 0
            sql = "SELECT TXT4 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TXT4'] == 1:
                    num += 1
            string += '🥈Достижение №14 (Необычное):🥈\n"Король суицида" (' + str(num) + ')\n'
            string += 'Вы погибли 100 раз в игре "Текстовый Квест"\n\n'
        if TXT5 == 1:
            num = 0
            sql = "SELECT TXT5 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['TXT5'] == 1:
                    num += 1
            string += '🥉Достижение №15 (Обычное):🥉\n"Выживший" (' + str(num) + ')\n'
            string += 'Вы прошли игру "Текстовый Квест"\n\n'
        if EGG1 == 1:
            num = 0
            sql = "SELECT EGG1 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG1'] == 1:
                    num += 1
            string += '🥉Достижение №16 (Обычное):🥉\n"Начало положено" (' + str(num) + ')\n'
            string += 'Вы разбили первое яйцо в игре "Разбей Яйцо"\n\n'
        if EGG2 == 1:
            num = 0
            sql = "SELECT EGG2 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG2'] == 1:
                    num += 1
            string += '🥈Достижение №17 (Необычное):🥈\n"Крушитель яиц" (' + str(num) + ')\n'
            string += 'Вы разбили пятое яйцо в игре "Разбей Яйцо"\n\n'
        if EGG3 == 1:
            num = 0
            sql = "SELECT EGG3 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG3'] == 1:
                    num += 1
            string += '🥇Достижение №18 (Редкое):🥇\n"Конец лишь начало пути" (' + str(num) + ')\n'
            string += 'Вы разбили десятое яйцо в игре "Разбей Яйцо"\n\n'
        if EGG4 == 1:
            num = 0
            sql = "SELECT EGG4 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG4'] == 1:
                    num += 1
            string += '🥈Достижение №19 (Необычное):🥈\n"Уставший палец" (' + str(num) + ')\n'
            string += 'Вы кликнули в общей сумме 1000 раз в игре "Разбей Яйцо"\n\n'
        if EGG5 == 1:
            num = 0
            sql = "SELECT EGG5 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG5'] == 1:
                    num += 1
            string += '🏅🏅Достижение №20 (НЕВЕРОЯТНОЕ):🏅🏅\n"Сгоревший палец" (' + str(num) + ')\n'
            string += 'Вы кликнули в общей сумме 100000 раз в игре "Разбей Яйцо"\n\n'
        if EGG6 == 1:
            num = 0
            sql = "SELECT EGG6 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG6'] == 1:
                    num += 1
            string += '🏅🏅Достижение №21 (НЕВЕРОЯТНОЕ):🏅🏅\n"Решающий удар" (' + str(num) + ')\n'
            string += 'Ваш клик разбил Глобальное Яйцо в игре "Разбей Яйцо"\n\n'
        if EGG7 == 1:
            num = 0
            sql = "SELECT EGG7 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG7'] == 1:
                    num += 1
            string += '🥈Достижение №22 (Необычное):🥈\n"Командная работа" (' + str(num) + ')\n'
            string += 'Вы нанесли Глобальному Яйцу суммарно 1000 единиц урона в игре "Разбей Яйцо"\n\n'
        if EGG8 == 1:
            num = 0
            sql = "SELECT EGG8 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['EGG8'] == 1:
                    num += 1
            string += '🥇Достижение №23 (Редкое):🥇\n"Неоценимый вклад" (' + str(num) + ')\n'
            string += 'Вы нанесли Глобальному Яйцу суммарно 10000 единиц урона в игре "Разбей Яйцо"\n\n'
        if RPG1 == 1:
            num = 0
            sql = "SELECT RPG1 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG1'] == 1:
                    num += 1
            string += '🥇Достижение №24 (Редкое):🥇\n"Повзрослел" (' + str(num) + ')\n'
            string += 'Вы достигли 15-ого уровня в игре "РПГ"\n\n'
        if RPG2 == 1:
            num = 0
            sql = "SELECT RPG2 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG2'] == 1:
                    num += 1
            string += '🥇Достижение №25 (Редкое):🥇\n"Крохобор" (' + str(num) + ')\n'
            string += 'Вы накопили 5000 золота в игре "РПГ"\n\n'
        if RPG3 == 1:
            num = 0
            sql = "SELECT RPG3 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG3'] == 1:
                    num += 1
            string += '🥈Достижение №26 (Необычное):🥈\n"Мастер оружейник" (' + str(num) + ')\n'
            string += 'Вы скупили все оружие в игре "РПГ"\n\n'
        if RPG4 == 1:
            num = 0
            sql = "SELECT RPG4 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG4'] == 1:
                    num += 1
            string += '🥈Достижение №27 (Необычное):🥈\n"Мастер бронник" (' + str(num) + ')\n'
            string += 'Вы скупили все брони в игре "РПГ"\n\n'
        if RPG5 == 1:
            num = 0
            sql = "SELECT RPG5 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG5'] == 1:
                    num += 1
            string += '🥉Достижение №28 (Обычное):🥉\n"Я убивал когда-то монстров..." (' + str(num) + ')\n'
            string += 'Вы убили 10 врагов в игре "РПГ"\n\n'
        if RPG6 == 1:
            num = 0
            sql = "SELECT RPG6 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG6'] == 1:
                    num += 1
            string += '🥈Достижение №29 (Необычное):🥈\n"Профессиональный убийца" (' + str(num) + ')\n'
            string += 'Вы убили 100 врагов в игре "РПГ"\n\n'
        if RPG7 == 1:
            num = 0
            sql = "SELECT RPG7 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG7'] == 1:
                    num += 1
            string += '🥇Достижение №30 (Редкое):🥇\n"Уничтожитель" (' + str(num) + ')\n'
            string += 'Вы убили 1000 врагов в игре "РПГ"\n\n'
        if RPG8 == 1:
            num = 0
            sql = "SELECT RPG8 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG8'] == 1:
                    num += 1
            string += '🏅🏅Достижение №31 (НЕВЕРОЯТНОЕ):🏅🏅\n"Вижу - убиваю" (' + str(num) + ')\n'
            string += 'Вы убили 5000 врагов в игре "РПГ"\n\n'
        if RPG9 == 1:
            num = 0
            sql = "SELECT RPG9 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG9'] == 1:
                    num += 1
            string += '🥉Достижение №32 (Обычное):🥉\n"Преступник тоже монстр" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Злоумышленника в игре "РПГ"\n\n'
        if RPG10 == 1:
            num = 0
            sql = "SELECT RPG10 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG10'] == 1:
                    num += 1
            string += '🥉Достижение №33 (Обычное):🥉\n"Ненавижу гоблинов" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Гоблина в игре "РПГ"\n\n'
        if RPG11 == 1:
            num = 0
            sql = "SELECT RPG11 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG11'] == 1:
                    num += 1
            string += '🥉Достижение №34 (Обычное):🥉\n"Зомби-апокалипсис предотвращен" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Зомби в игре "РПГ"\n\n'
        if RPG12 == 1:
            num = 0
            sql = "SELECT RPG12 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG12'] == 1:
                    num += 1
            string += '🥉Достижение №35 (Обычное):🥉\n"Вой на Луну" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Оборотня в игре "РПГ"\n\n'
        if RPG13 == 1:
            num = 0
            sql = "SELECT RPG13 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG13'] == 1:
                    num += 1
            string += '🥉Достижение №36 (Обычное):🥉\n"Быка за рога" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Минотавра в игре "РПГ"\n\n'
        if RPG14 == 1:
            num = 0
            sql = "SELECT RPG14 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG14'] == 1:
                    num += 1
            string += '🥉Достижение №37 (Обычное):🥉\n"Больше не пошипишь" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Василиска в игре "РПГ"\n\n'
        if RPG15 == 1:
            num = 0
            sql = "SELECT RPG15 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG15'] == 1:
                    num += 1
            string += '🥈Достижение №38 (Необычное):🥈\n"Пожарный" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Огненного элементаля в игре "РПГ"\n\n'
        if RPG16 == 1:
            num = 0
            sql = "SELECT RPG16 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG16'] == 1:
                    num += 1
            string += '🥈Достижение №39 (Необычное):🥈\n"Дезинсектор" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Инсектоида в игре "РПГ"\n\n'
        if RPG17 == 1:
            num = 0
            sql = "SELECT RPG17 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG17'] == 1:
                    num += 1
            string += '🥈Достижение №40 (Необычное):🥈\n"Безрукий" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Многорукого в игре "РПГ"\n\n'
        if RPG18 == 1:
            num = 0
            sql = "SELECT RPG18 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG18'] == 1:
                    num += 1
            string += '🥈Достижение №41 (Необычное):🥈\n"Жало отжали" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Скорпиона в игре "РПГ"\n\n'
        if RPG19 == 1:
            num = 0
            sql = "SELECT RPG19 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG19'] == 1:
                    num += 1
            string += '🥇Достижение №42 (Редкое):🥇\n"Одна голова хорошо, а две тоже не помогли" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Двуглавого Гиганта в игре "РПГ"\n\n'
        if RPG20 == 1:
            num = 0
            sql = "SELECT RPG20 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG20'] == 1:
                    num += 1
            string += '🥇Достижение №43 (Редкое):🥇\n"В могиле тебе и место" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Могильщика в игре "РПГ"\n\n'
        if RPG21 == 1:
            num = 0
            sql = "SELECT RPG21 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG21'] == 1:
                    num += 1
            string += '🥇Достижение №44 (Редкое):🥇\n"Регенератор" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Войда в игре "РПГ"\n\n'
        if RPG22 == 1:
            num = 0
            sql = "SELECT RPG22 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG22'] == 1:
                    num += 1
            string += '🥇Достижение №45 (Редкое):🥇\n"Хорошо, что я не герой" (' + str(num) + ')\n'
            string += 'Вы 100 раз убили Пожирателя Героев в игре "РПГ"\n\n'
        if RPG23 == 1:
            num = 0
            sql = "SELECT RPG23 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG23'] == 1:
                    num += 1
            string += '🏅🏅Достижение №46 (НЕВЕРОЯТНОЕ):🏅🏅\n"Древнее зло побеждено!" (' + str(num) + ')\n'
            string += 'Вы убили Лидера в игре "РПГ"\n\n'
        if RPG24 == 1:
            num = 0
            sql = "SELECT RPG24 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG24'] == 1:
                    num += 1
            string += '🥇Достижение №47 (Редкое):🥇\n"Золотая ручка" (' + str(num) + ')\n'
            string += 'За все время вы получили 100000 золота в игре "РПГ"\n\n'
        if RPG25 == 1:
            num = 0
            sql = "SELECT RPG25 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG25'] == 1:
                    num += 1
            string += '🤡Достижение №48 (Никакое):🤡\n"Так, давай по новой" (' + str(num) + ')\n'
            string += 'Вы погибли 100 раз в игре "РПГ"\n\n'
        if RPG26 == 1:
            num = 0
            sql = "SELECT RPG26 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG26'] == 1:
                    num += 1
            string += '🥉Достижение №49 (Обычное):🥉\n"В атаку!" (' + str(num) + ')\n'
            string += 'Вы использовали базовую атаку 1000 раз в игре "РПГ"\n\n'
        if RPG27 == 1:
            num = 0
            sql = "SELECT RPG27 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG27'] == 1:
                    num += 1
            string += '🥉Достижение №50 (Обычное):🥉\n"Отступление!" (' + str(num) + ')\n'
            string += 'Вы пропустили ход 1000 раз в игре "РПГ"\n\n'
        if RPG28 == 1:
            num = 0
            sql = "SELECT RPG28 FROM `achievements`"
            cursor.execute(sql)
            for element in cursor:
                if element['RPG28'] == 1:
                    num += 1
            string += '🥉Достижение №51 (Обычное):🥉\n"Почувствуй мою мощь!" (' + str(num) + ')\n'
            string += 'Вы использовали способности 1000 раз в игре "РПГ"\n\n'
    connection.commit()
    connection.close()
    return string


def achievement_leaders(m):
    """
    Функция, показывающая лидеров по достижениям.

    m -- сообщение пользователя

    returns: string -- сформированная строка
    """
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT id, achCount FROM `achievements` order by achCount desc"
    cursor.execute(sql)
    string = '👑СПИСОК ЛИДЕРОВ:👑\n\n'
    num = 1
    for element in cursor:
        string += '#' + str(num) + ') '
        if int(element['id']) == m.chat.id:
            string += '💫ЭТО ВЫ: '
        string += '_' + element['id'].encode('utf-8') + '_ ➖ '
        string += '*' + str(element['achCount']) + '*\n'
        num += 1
        if num == 101:
            break
    return string


def test_save_and_achievements(m, allPoints, points):
    """
    Функция, сохраняющая очки в игре тест и открывающая достижения.

    m -- сообщение пользователя
    allPoints -- очки, набранные пользователем за все время
    points -- очки, набранные пользователем за игру

    returns: allPoints -- обновленное количество очков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `test_points` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for kek in cursor:
        len += 1
    if len != 0:                                                            #если пользователь с таким айди уже есть в базе данных
        sql = "SELECT * FROM `test_points` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            allPoints = element['points']                                   #записываем в переменную очки набранные пользователем за все время
        allPoints += points                                                 #добавляем к ним набранные за эту игру очки
        sql = "UPDATE `test_points` SET `points` = %s WHERE `id` = %s"      #и обновляем данные в базе
        cursor.execute(sql, (allPoints, m.chat.id))
    else:                                                                   #если пользователь проходит тест впервые
        sql = "INSERT INTO `test_points`(`id`, `points`) VALUES (%s, %s)"   #создаем в базе запись с его айди и очками
        allPoints += points                                                 #все равно записываем в переменную очки набранные пользователем за все время
        cursor.execute(sql, (m.chat.id, points))
    achCount = 0
    TEST1 = 0
    TEST2 = 0
    TEST3 = 0
    TEST4 = 0
    TEST5 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        TEST1 = element['TEST1']
        TEST2 = element['TEST2']
        TEST3 = element['TEST3']
        TEST4 = element['TEST4']
        TEST5 = element['TEST5']
    if TEST1 == 0 and points == 5:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TEST1 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Отличник". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TEST2 == 0 and points == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TEST2 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Ты хоть пытался?". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TEST3 == 0 and allPoints >= 30:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TEST3 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Уже не новичок". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TEST4 == 0 and allPoints >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TEST4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Притворяющийся умником". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TEST5 == 0 and allPoints >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TEST5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Всезнайка". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()
    return allPoints


def test_leaders(m):
    """
    Функция, показывающая лидеров игры "Тест" по очкам.

    m -- сообщение пользователя

    returns: string -- сформированная строка
    """
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT id, points FROM `test_points` order by points desc"
    cursor.execute(sql)
    string = '👑СПИСОК ЛИДЕРОВ:👑\n\n'
    num = 1
    for element in cursor:
        string += '#' + str(num) + ') '
        if int(element['id']) == m.chat.id:
            string += '💫ЭТО ВЫ: '
        string += '_' + element['id'].encode('utf-8') + '_ ➖ '
        string += '*' + str(element['points']) + '*\n'
        num += 1
        if num == 101:
            break
    return string


def kosti_database(m, playerWins, botWins):
    """
    Функция игры "Кости", которая сохраняет количество побед.

    m -- сообщение пользователя
    playerWins -- количество побед игрока
    botWins -- количество побед бота

    returns: string -- сформированная строка
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    playerWon = playerWins
    botWon = botWins
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `kosti` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for kek in cursor:
        len += 1
    if len != 0:
        sql = "SELECT * FROM `kosti` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            playerWins = element['playerWins']
            botWins = element['botWins']
        playerWins += playerWon
        botWins += botWon
        sql = "UPDATE `kosti` SET `botWins` = %s, playerWins = %s WHERE `id` = %s"
        cursor.execute(sql, (botWins, playerWins, m.chat.id))
    else:
        sql = "INSERT INTO `kosti`(`id`, `botWins`, `playerWins`) VALUES (%s, %s, %s)"
        cursor.execute(sql, (m.chat.id, botWins, playerWins))
    achCount = 0
    KOSTI1 = 0
    KOSTI2 = 0
    KOSTI3 = 0
    KOSTI4 = 0
    KOSTI5 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        KOSTI1 = element['KOSTI1']
        KOSTI2 = element['KOSTI2']
        KOSTI3 = element['KOSTI3']
        KOSTI4 = element['KOSTI4']
        KOSTI5 = element['KOSTI5']
    if KOSTI1 == 0 and botWins >= 10:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, KOSTI1 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Пластмассовый мир победил". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if KOSTI2 == 0 and playerWins >= 10:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, KOSTI2 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "ИИ не пройдет". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if KOSTI3 == 0 and botWins >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, KOSTI3 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Чертов рандом". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if KOSTI4 == 0 and playerWins >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, KOSTI4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Обожаю этот рандом". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if KOSTI5 == 0 and playerWon == 1 and botWon == 1:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, KOSTI5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Никто не в обиде". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()
    string = 'Статистика побед 🏆:\n\nБот: ' + str(botWins) + '\nВы: ' + str(playerWins) + '\n\nЕщё раз?'
    return string


def egg_achievements_and_save(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter):
    """
    Функция игры "Разбей Яйцо", которая отвечает за сохранение игры и открытие достижений.

    m -- сообщение пользователя
    eggHP -- здоровье текущего яйца
    currentEgg -- номер текущего яйца
    clickPower -- сила клика игрока
    clickCriticalChance -- критический шанс игрока
    clickCritMultiplier -- множитель критического клика
    clickCounter -- общее количество кликов
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    sql = "UPDATE `egg` SET `eggHP` = %s, `currentEgg` = %s, `clickPower` = %s, `clickCriticalChance` = %s, `clickCritMultiplier` = %s, `clickCounter` = %s WHERE `id` = %s"
    cursor.execute(sql, (eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter, m.chat.id))
    achCount = 0
    EGG1 = 0
    EGG2 = 0
    EGG3 = 0
    EGG4 = 0
    EGG5 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        EGG1 = element['EGG1']
        EGG2 = element['EGG2']
        EGG3 = element['EGG3']
        EGG4 = element['EGG4']
        EGG5 = element['EGG5']
    if EGG1 == 0 and currentEgg >= 2:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG1 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Начало положено". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG2 == 0 and currentEgg >= 6:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG2 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Крушитель яиц". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG3 == 0 and currentEgg >= 11:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG3 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Конец лишь начало пути". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG4 == 0 and clickCounter >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Уставший палец". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG5 == 0 and clickCounter >= 100000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Сгоревший палец". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def global_egg_achievements_and_save(m, clickCounter, playerDamag, killedTheEgg):
    """
    Функция игры "Разбей Яйцо", которая отвечает за сохранение глобального яйца и выдачу достижений.

    m -- сообщение пользователя
    clickCounter -- общее количество кликов
    playerDamag -- урон, нанесенный игроком
    killedTheEgg -- был ли последний решающий удар по яйцу нанесен игроком
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    sql = "UPDATE `egg` SET `clickCounter` = %s, playerDamag = %s WHERE `id` = %s"
    cursor.execute(sql, (clickCounter, playerDamag, m.chat.id))
    achCount = 0
    EGG4 = 0
    EGG5 = 0
    EGG6 = 0
    EGG7 = 0
    EGG8 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        EGG4 = element['EGG4']
        EGG5 = element['EGG5']
        EGG6 = element['EGG6']
        EGG7 = element['EGG7']
        EGG8 = element['EGG8']
    if EGG4 == 0 and clickCounter >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Уставший палец". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG5 == 0 and clickCounter >= 100000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Сгоревший палец". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG6 == 0 and killedTheEgg == 1:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG6 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Решающий удар". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG7 == 0 and playerDamag == 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG7 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Командная работа". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if EGG8 == 0 and playerDamag == 10000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, EGG8 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Неоценимый вклад". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def global_egg_leaders(m):
    """
    Функция, показывающая лидеров игры "Разбей яйцо" по урону, нанесенному глобальному яйцу.

    m -- сообщение пользователя

    returns: string -- сформированная строка
    """
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT id, playerDamag FROM `egg` order by playerDamag desc"
    cursor.execute(sql)
    string = '👑СПИСОК ЛИДЕРОВ:👑\n\n'
    num = 1
    for element in cursor:
        string += '#' + str(num) + ') '
        if int(element['id']) == m.chat.id:
            string += '💫ЭТО ВЫ: '
        string += '_' + element['id'].encode('utf-8') + '_ ➖ '
        string += '*' + str(element['playerDamag']) + '*\n'
        num += 1
        if num == 101:
            break
    return string


def saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая сохраняет показатели игрока.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    connection = get_connection()
    cursor = connection.cursor()
    sql = "UPDATE `rpg` SET characterName = %s, characterClass = %s, characterLevel = %s, health = %s, armor = %s, stamina = %s, lowDamage = %s, highDamage = %s, agility = %s, critChance = %s, critMultiplier = %s, gold = %s, experience = %s, experienceTarget = %s, critUpgrades = %s, agilityUpgrades = %s, staminaUpgrades = %s, currentWeapon = %s, currentArmor = %s, curYourPower = %s, curYourPowerStaminaCost = %s WHERE `id` = %s"
    cursor.execute(sql, (characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, m.chat.id))
    connection.commit()
    connection.close()


def powers_list(m, characterLevel, keyboard):
    """
    Функция игры "РПГ", которая формирует строку со списком способностей

    m -- сообщение пользователя
    characterLevel -- уровень персонажа
    keyboard -- клавиатура, для добавления кнопок

    returns: powerString -- сформированная строка
    """
    powerString = ''
    keyboard.add(*[types.KeyboardButton(name) for name in ['1']])
    powerString += 'Выберите способность вашему персонажу из списка (с повышением уровня открываются новые способности):\n\n'
    powerString += '1 Способность: "Мощный удар". Вы наносите удар, сильнее вашего базового на 1 единицу урона. Требует 2 единицы выносливости.\n\n'
    if characterLevel >= 2:
        keyboard.add(*[types.KeyboardButton(name) for name in ['2']])
        powerString += '2 Способность: "Удар по нервам". Вы наносите удар в слабое место противника, заставляя того пропустить 2 хода. Требует 4 единицы выносливости.\n\n'
    if characterLevel >= 3:
        keyboard.add(*[types.KeyboardButton(name) for name in ['3']])
        powerString += '3 Способность: "Ослабление". Вы пинаете противника в живот, заставляя того потерять 3 единицы выносливости. Требует 6 единиц выносливости.\n\n'
    if characterLevel >= 4:
        keyboard.add(*[types.KeyboardButton(name) for name in ['4']])
        powerString += '4 Способность: "Серия ударов". Вы наносите противнику 3 ваших базовых атаки за ход. Требует 3 единицы выносливости.\n\n'
    if characterLevel >= 5:
        keyboard.add(*[types.KeyboardButton(name) for name in ['5']])
        powerString += '5 Способность: "Удар победителя". Вы наносите противнику мощный удар сильнее вашей базовой атаки в 2 раза. Он гарантированно критический, всегда попадает и игнорирует броню противника. Требует 2 единицы выносливости.\n\n'
    if characterLevel >= 6:
        keyboard.add(*[types.KeyboardButton(name) for name in ['6']])
        powerString += '6 Способность: "Перевязка ран". Вы перевязываете свои раны, восстанавливая 100 единиц здоровья. Требует 6 единиц выносливости.\n\n'
    if characterLevel >= 7:
        keyboard.add(*[types.KeyboardButton(name) for name in ['7']])
        powerString += '7 Способность: "Зоркий глаз". Изучая противника, вы повышаете множитель своего крита на 0.1 до конца боя. Требует 1 единицу выносливости и дает право походить еще раз.\n\n'
    if characterLevel >= 8:
        keyboard.add(*[types.KeyboardButton(name) for name in ['8']])
        powerString += '8 Способность: "Ловкач". Вы нашли свиток с заклинанием, которое увеличивает вашу ловкость до 100 % на следующий ход. Требует 4 единицы выносливости.\n\n'
    if characterLevel >= 9:
        keyboard.add(*[types.KeyboardButton(name) for name in ['9']])
        powerString += '9 Способность: "Удар Бога". Вы наносите противнику невообразимо мощный удар сильнее вашей базовой атаки в 3 раза. Он гарантированно критический, всегда попадает и игнорирует броню противника. Требует 2.5 единицы выносливости.\n\n'
    if characterLevel >= 10:
        keyboard.add(*[types.KeyboardButton(name) for name in ['10']])
        powerString += '10 Способность: "Магическое ослабление". Вы нашли свиток с заклинанием, которое заставляет противника потерять 5 единиц выносливости. Требует 5 единиц выносливости.\n\n'
    if characterLevel >= 11:
        keyboard.add(*[types.KeyboardButton(name) for name in ['11']])
        powerString += '11 Способность: "Крепкий орешек" (пассивная). Перед боем вы использовали свиток стойкости и не получаете урон от негативных эффектов.\n\n'
    if characterLevel >= 12:
        keyboard.add(*[types.KeyboardButton(name) for name in ['12']])
        powerString += '12 Способность: "Остолбеней". Вы нашли свиток, который обездвиживает вашего врага, заставляя того пропустить 3 хода. Требует 3 единицы выносливости.\n\n'
    if characterLevel >= 13:
        keyboard.add(*[types.KeyboardButton(name) for name in ['13']])
        powerString += '13 Способность: "Быстр как ветер". Вы нашли свиток, который ускоряет вас до такой степени, что вы наносите 5 своих базовых атак за ход. Требует 3 единицы выносливости.\n\n'
    if characterLevel >= 14:
        keyboard.add(*[types.KeyboardButton(name) for name in ['14']])
        powerString += '14 Способность: "Бессмертие". Вы нашли свиток, который восстанавливает 500 единиц вашего здоровья. Требует 3.5 единицы выносливости. Также сила имеет '
        powerString += 'пассивный эффект восстановления. Вы имеете шанс 30% вернуть в начале хода 15 единиц здоровья.\n\n'
    if characterLevel >= 15:
        keyboard.add(*[types.KeyboardButton(name) for name in ['15']])
        powerString += '15 Способность: "Лишение сил". Вы нашли свиток с заклинанием, которое выкачивает из противника всю его выносливость. Требует 4 единицы выносливости.\n\n'
    keyboard.add(*[types.KeyboardButton(name) for name in ['Назад']])
    return powerString


def rpg_statistics(m, characterLevel):
    """
    Функция игры "РПГ", которая формирует строку со статистикой

    m -- сообщение пользователя
    characterLevel -- уровень персонажа

    returns: string -- сформированная строка
    """
    enemiesKilled = 0
    enemy1killed = 0
    enemy2killed = 0
    enemy3killed = 0
    enemy4killed = 0
    enemy5killed = 0
    enemy6killed = 0
    enemy7killed = 0
    enemy8killed = 0
    enemy9killed = 0
    enemy10killed = 0
    enemy11killed = 0
    enemy12killed = 0
    enemy13killed = 0
    enemy14killed = 0
    enemy15killed = 0
    allTimeGold = 0
    deaths = 0
    attackUse = 0
    losedTurns = 0
    powerUse = 0
    power1use = 0
    power2use = 0
    power3use = 0
    power4use = 0
    power5use = 0
    power6use = 0
    power7use = 0
    power8use = 0
    power9use = 0
    power10use = 0
    power12use = 0
    power13use = 0
    power14use = 0
    power15use = 0
    restarts = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        enemiesKilled = element['enemiesKilled']
        enemy1killed = element['enemy1killed']
        enemy2killed = element['enemy2killed']
        enemy3killed = element['enemy3killed']
        enemy4killed = element['enemy4killed']
        enemy5killed = element['enemy5killed']
        enemy6killed = element['enemy6killed']
        enemy7killed = element['enemy7killed']
        enemy8killed = element['enemy8killed']
        enemy9killed = element['enemy9killed']
        enemy10killed = element['enemy10killed']
        enemy11killed = element['enemy11killed']
        enemy12killed = element['enemy12killed']
        enemy13killed = element['enemy13killed']
        enemy14killed = element['enemy14killed']
        enemy15killed = element['enemy15killed']
        allTimeGold = element['allTimeGold']
        deaths = element['deaths']
        attackUse = element['attackUse']
        losedTurns = element['losedTurns']
        powerUse = element['powerUse']
        power1use = element['power1use']
        power2use = element['power2use']
        power3use = element['power3use']
        power4use = element['power4use']
        power5use = element['power5use']
        power6use = element['power6use']
        power7use = element['power7use']
        power8use = element['power8use']
        power9use = element['power9use']
        power10use = element['power10use']
        power12use = element['power12use']
        power13use = element['power13use']
        power14use = element['power14use']
        power15use = element['power15use']
        restarts = element['restarts']
    string = '📃СТАТИСТИКА📃\n\n'
    string += '💰Полученное за все время золото: ' + str(allTimeGold) + '\n'
    string += '💀Количество смертей: ' + str(deaths) + '\n\n'
    string += '🪓УБИЙСТВА ВРАГОВ:\n'
    string += '☠Всего убито врагов: ' + str(enemiesKilled) + '\n'
    string += '🦹‍♂️Первый враг был убит: ' + str(enemy1killed) + '\n'
    if characterLevel >= 2:
        string += '🐲Второй враг был убит: ' + str(enemy2killed) + '\n'
    if characterLevel >= 3:
        string += '🧟Третий враг был убит: ' + str(enemy3killed) + '\n'
    if characterLevel >= 4:
        string += '🐺Четвертый враг был убит: ' + str(enemy4killed) + '\n'
    if characterLevel >= 5:
        string += '🐃Пятый враг был убит: ' + str(enemy5killed) + '\n'
    if characterLevel >= 6:
        string += '🐍Шестой враг был убит: ' + str(enemy6killed) + '\n'
    if characterLevel >= 7:
        string += '🔥Седьмой враг был убит: ' + str(enemy7killed) + '\n'
    if characterLevel >= 8:
        string += '🦟Восьмой враг был убит: ' + str(enemy8killed) + '\n'
    if characterLevel >= 9:
        string += '🤛🤚✋🤜Девятый враг был убит: ' + str(enemy9killed) + '\n'
    if characterLevel >= 10:
        string += '🦂Десятый враг был убит: ' + str(enemy10killed) + '\n'
    if characterLevel >= 11:
        string += '👨👨Одиннадцатый враг был убит: ' + str(enemy11killed) + '\n'
    if characterLevel >= 12:
        string += '⚰Двенадцатый враг был убит: ' + str(enemy12killed) + '\n'
    if characterLevel >= 13:
        string += '🦴Тринадцатый враг был убит: ' + str(enemy13killed) + '\n'
    if characterLevel >= 14:
        string += '🧛Четырнадцатый враг был убит: ' + str(enemy14killed) + '\n'
    if characterLevel >= 15:
        string += '\xF0\x9F\x87\xB7\xF0\x9F\x87\xBAПятнадцатый враг был убит: ' + str(enemy15killed) + '\n'
    string += '\n👊Использование базовой атаки: ' + str(attackUse) + '\n'
    string += '🌀Пропущено ходов: ' + str(losedTurns) + '\n'
    string += '\n⚜ИСПОЛЬЗОВАНИЕ СПОСОБНОСТЕЙ:\n'
    string += 'Всего использовано способностей: ' + str(powerUse) + '\n'
    string += 'Первая способность: ' + str(power1use) + '\n'
    if characterLevel >= 2:
        string += 'Вторая способность: ' + str(power2use) + '\n'
    if characterLevel >= 3:
        string += 'Третья способность: ' + str(power3use) + '\n'
    if characterLevel >= 4:
        string += 'Четвертая способность: ' + str(power4use) + '\n'
    if characterLevel >= 5:
        string += 'Пятая способность: ' + str(power5use) + '\n'
    if characterLevel >= 6:
        string += 'Шестая способность: ' + str(power6use) + '\n'
    if characterLevel >= 7:
        string += 'Седьмая способность: ' + str(power7use) + '\n'
    if characterLevel >= 8:
        string += 'Восьмая способность: ' + str(power8use) + '\n'
    if characterLevel >= 9:
        string += 'Девятая способность: ' + str(power9use) + '\n'
    if characterLevel >= 10:
        string += 'Десятая способность: ' + str(power10use) + '\n'
    if characterLevel >= 12:
        string += 'Двенадцатая способность: ' + str(power12use) + '\n'
    if characterLevel >= 13:
        string += 'Тринадцатая способность: ' + str(power13use) + '\n'
    if characterLevel >= 14:
        string += 'Четырнадцатая способность: ' + str(power14use) + '\n'
    if characterLevel >= 15:
        string += 'Пятнадцатая способность: ' + str(power15use) + '\n'
    string += '\n🚽Сбросы прогресса: ' + str(restarts) + '\n'
    return string


def rpg_help():
    """
    Функция игры "РПГ", которая формирует строку с помощью

    returns: help -- сформированная строка
    """
    help ='Описание характеристик:\n\n'
    help +='🦸 ИМЯ ПЕРСОНАЖА: используется в топе игроков. Да и вообще просто приятно как-нибудь обозвать персонажа, не так ли? :) \n\n'
    help +='🎭 ХАРАКТЕР: опция, которая совсем незначительно, но влияет на игровой процесс, особенно в самом его начале. Каждый выбранный характер дает какой-то бонус. '
    help +='Одновременно может быть выбран только один характер, следовательно, смена характера забирает бонус от выбранного ранее. На данный момент имеется выбор между '
    help +='АГРЕССИВНЫМ и ОСТОРОЖНЫМ. Агрессивный тип характера дает вам +10 к шансу критического удара и особенно полезен, когда шанс вашего крита еще невелик. В то же '
    help +='время осторожный тип характера дает +5 к ловкости. Учитывая, что ловкость снижается при улучшении брони, это позволит, особенно на первых порах игры, нивелировать '
    help +='этот минус до тех пор, пока ваша ловкость не станет повышаться за уровни. В целом трудно сказать какой тип характера полезнее. Они оба не дают какого-то колоссального '
    help +='преимущества, но в то же время позволяют немного скорректировать параметры персонажа под свой стиль игры. Кому-то удобнее будет наносить больше урона за счет более '
    help +='частого критования, а кто-то предпочтет дольше держаться в бою, за счет повышенного шанса вывести вражеский урон в ноль.\n\n'
    help +='💰 ЗОЛОТО: внутриигровая валюта, которая позволяет улучшать собственные показатели, а также покупать оружие и броню. В будущем возможны дополнительные способы вложить свое золото. '
    help +='Также следует отметить, что улучшать за золото безгранично можно только свое здоровье. Остальные же показатели имеют предел повышения за золото.\n\n'
    help +='💪 УРОВЕНЬ: уровень вашего персонажа отвечает за многое. Во первых, при повышении уровня, вы становитесь сильнее. Повышаются те показатели, которые нельзя повысить '
    help +='за золото, либо можно, но лишь ограниченное количество раз. На последних уровнях особенно сильно возрастает здоровье, так что не спешите вкладывать все золото в его '
    help +='повышение. Лучше потратиться на оружие и броню, ведь эти показатели не возрастают с повышением уровня. Чем выше ваш уровень, тем больше противников вам доступно. '
    help +='Это не значит, что вы справитесь с только что появившимся противником, но во всяком случае узнаете к чему стремиться. На каждом уровне открываются новые способности. '
    help +='Нет ничего зазорного в том, чтобы на 15ом уровне использовать способность третьего уровня. Новее не значит сильнее. Но во всяком случае у вас появляется выбор и возможность '
    help +='значительно менять стиль боя против разных противников. И да, помните, что золото и опыт дают только противники вашего уровня и ниже него на 3 уровня. Так что '
    help +='тратьте золото разумно и старайтесь приспосабливаться к новым противникам. Однако самый первый враг всегда дает золото и опыт, пусть и мало, но он и убивается за ход :)\n\n'
    help +='📖 ОПЫТ: собственно это и есть тот ресурс, за счет которого растет ваш уровень. Заработать можно только в бою. Второе число это граница, при достижении/пересечении '
    help +='которой ваш уровень будет повышен. На данный момент в игре 15 уровней персонажа, после 15ого опыт не зарабатывается.\n\n'
    help +='🩸 ЗДОРОВЬЕ: как только ваше здоровье в бою достигает нуля - вы проигрываете. Ваша цель снизить до нуля здоровье врага, сохранив при этом свое. Это основополагающий '
    help +='параметр, но точно не единственный важный.\n\n'
    help +='🛡 БРОНЯ: снижает получаемый урон. То есть если вы должны были потерять 3 единицы здоровья, а ваш показатель брони равен 2, то вы получите 1 единицу урона. '
    help +='Броня не снижает урон от негативных эффектов, которые наносят урон с течением времени. Также помните, что улучшать броню до бесконечности нельзя. Как только вы '
    help +='скупите все брони из лавки - этот показатель больше меняться не будет. Некоторые брони также понижают вашу ловкость и могут вывести ее в ноль, а то и в минус.\n\n'
    help +='🧿 ВЫНОСЛИВОСТЬ: энергия персонажа, которая позволяет использовать способности. Чтобы использовать способность, нужно иметь столько выносливости, сколько она требует. '
    help +='Если враг уклонится от способности - выносливость все равно потратится. Вы начинаете бой с показателем выносливости, указанным здесь, но в ходе игры будете его '
    help +='увеличивать и сможете начинать бой со все большим и большим количеством выносливости, сразу же атакуя врага серией способностей, что может в корне переломить ход боя. '
    help +='Израсходовав выносливость вы не потеряете возможность использовать способность, но выносливость придется зарабатывать в бою. +0.5 выносливости дается за каждое '
    help +='успешное попадание по врагу базовой атакой и +1 выносливость за каждый пропущенный ход, не будучи в состоянии оглушения. Таким образом вам дается выбор: продолжать '
    help +='бить противника без остановок и накапливать выносливость долго, либо пропуская ходы копить выносливость быстро. Самым правильным решением будет комбинация этих '
    help +='тактик в зависимости от ситуации. И да, враги не пропускают ходы и за каждое успешное попадание по вам получают сразу 1 выносливость.\n\n'
    help +='⚜ СПОСОБНОСТЬ: номер выбранной в данный момент способности. Вспомнить что это за способность можно в одноименной вкладке со списком способностей. Там же можно и '
    help +='сменить способность на другую при необходимости.\n\n'
    help +='⚔ АТАКА: эти два числа отвечают за разброс урона, наносимого вами вашей базовой атакой. Также от этого показателя зависит и урон ваших сил. Если ваша атака, например, '
    help +='1-3, то не кританув вы можете нанести как 1 урон, так 2, так и 3. Какой же силы будет удар определяет случайность. Это позволяет делать битвы непредсказуемыми. '
    help +='Помните, что враг может снижать ваш урон своей броней, а так же улучшать атаку до бесконечности нельзя. Как только вы скупите все оружие из лавки - этот '
    help +='показатель больше меняться не будет.\n\n'
    help +='⚡ ЛОВКОСТЬ: этот показатель измеряется от 0 до 100 в процентах и определяет шанс вашего уклонения. Спойлер: улучшить его до 100 вам не удастся вообще никак, но '
    help +='даже 40% будет более чем достаточно, чтобы выводить достаточное количество атак противника в ноль. Уклониться можно даже от способности. '
    help += 'Улучшая броню, вы можете вывести этот показатель в минус и тогда вам придется компенсировать его, чтобы снова начать хоть как-то уклоняться.\n\n'
    help +='🔆 ШАНС КРИТА: этот показатель измеряется от 0 до 100 в процентах и определяет шанс того, что удар будет критическим и нанесет гораздо больше урона. Спойлер: '
    help +='его улучшить до 100 тоже не получится, но до 80% более чем реально, а это уже практически все ваши атаки делает критующими.\n\n'
    help +='💥 МНОЖИТЕЛЬ КРИТА: показатель, который определяет во сколько раз больше урона получит противник в случае критического попадания. Этот показатель улучшается только '
    help +='при покупке нового оружия и при достижении последних уровней персонажа.\n\n'
    return help


def clear_RPGBD(m):
    """
    Функция игры "РПГ", которая удаляет прогресс пользователя.

    m -- сообщение пользователя
    """
    connection = get_connection()
    cursor = connection.cursor()
    restarts = 0
    allTimeGold = 0
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        restarts = element['restarts']
        allTimeGold = element['allTimeGold']
    restarts += 1
    allTimeGold += 10
    sql = "UPDATE `rpg_stat` SET restarts = %s, allTimeGold = %s WHERE `id` = %s"
    cursor.execute(sql, (restarts, allTimeGold, m.chat.id))
    sql = "DELETE FROM `rpg` WHERE `id` = %s"
    cursor.execute(sql, m.chat.id)
    connection.commit()
    connection.close()


def nextWeapon(currentWeapon):
    """
    Функция игры "РПГ", которая получает номер текущего оружия и выводит строку с его описанием и ценой.

    currentWeapon -- номер текущего оружия

    returns: строка с описанием оружия
    """
    if currentWeapon == 1:
        return 'Деревянный Меч. +1 к максимальному урону. Цена: ' + str(weaponCost1) + ' монет'
    elif currentWeapon == 2:
        return 'Каменный Меч. +1 к максимальному и минимальному урону. Цена: ' + str(weaponCost2) + ' монет'
    elif currentWeapon == 3:
        return 'Железный Меч. +1 к максимальному урону, +2 к шансу крита. Цена: ' + str(weaponCost3) + ' монет'
    elif currentWeapon == 4:
        return 'Алмазный Меч. +1 к максимальному урону, +5 к шансу крита. Цена: ' + str(weaponCost4) + ' монет'
    elif currentWeapon == 5:
        return 'Железный Двуручный Меч. +2 к минимальному урону, +1 к максимальному урону. Цена: ' + str(weaponCost5) + ' монет'
    elif currentWeapon == 6:
        return 'Алмазный Двуручный Меч. +1 к минимальному урону, +2 к максимальному урону. Цена: ' + str(weaponCost6) + ' монет'
    elif currentWeapon == 7:
        return 'Железная Секира. +1 к минимальному урону, +1 к максимальному урону. Цена: ' + str(weaponCost7) + ' монет'
    elif currentWeapon == 8:
        return 'Алмазная Секира. +2 к минимальному урону, +2 к максимальному урону, +5 к шансу крита. Цена: ' + str(weaponCost8) + ' монет'
    elif currentWeapon == 9:
        return 'Железная Кувалда. +2 к минимальному урону, +1 к максимальному урону. Цена: ' + str(weaponCost9) + ' монет'
    elif currentWeapon == 10:
        return 'Алмазная Кувалда. +1 к минимальному урону, +2 к максимальному урону, +5 к шансу крита. Цена: ' + str(weaponCost10) + ' монет'
    elif currentWeapon == 11:
        return 'Железная Булава. +2 к минимальному урону, +2 к максимальному урону. Цена: ' + str(weaponCost11) + ' монет'
    elif currentWeapon == 12:
        return 'Алмазная Булава. +2 к минимальному урону, +3 к максимальному урону, +5 к шансу крита. Цена: ' + str(weaponCost12) + ' монет'
    elif currentWeapon == 13:
        return 'Два железных отравленных меча. +3 к минимальному урону, +3 к максимальному урону. Цена: ' + str(weaponCost13) + ' монет'
    elif currentWeapon == 14:
        return 'Два алмазных отравленных меча. +4 к минимальному урону, +4 к максимальному урону. Цена: ' + str(weaponCost14) + ' монет'
    elif currentWeapon == 15:
        return 'Оружие Бога. +9 к минимальному урону, +5 к максимальному урону, +5 к шансу крита, +0.3 ко множителю крита. Цена: ' + str(weaponCost15) + ' монет'


def nextArmor(currentArmor):
    """
    Функция игры "РПГ", которая получает номер текущей брони и выводит строку с ее описанием и ценой.

    currentArmor -- номер текущей брони

    returns: строка с описанием брони
    """
    if currentArmor == 1:
        return 'Железная Кольчужка. +1 к броне. Цена: ' + str(armorCost1) + ' монет'
    elif currentArmor == 2:
        return 'Стальная Кольчужка. +1 к броне. Цена: ' + str(armorCost2) + ' монет'
    elif currentArmor == 3:
        return 'Алмазная Кольчужка. +2 к броне, -1 от ловкости. Цена: ' + str(armorCost3) + ' монет'
    elif currentArmor == 4:
        return 'Железные Штаны. +1 к броне. Цена: ' + str(armorCost4) + ' монет'
    elif currentArmor == 5:
        return 'Стальные Штаны. +1 к броне. Цена: ' + str(armorCost5) + ' монет'
    elif currentArmor == 6:
        return 'Алмазные Штаны. +2 к броне, -2 от ловкости. Цена: ' + str(armorCost6) + ' монет'
    elif currentArmor == 7:
        return 'Железный Шлем. +2 к броне. Цена: ' + str(armorCost7) + ' монет'
    elif currentArmor == 8:
        return 'Стальной Шлем. +2 к броне. Цена: ' + str(armorCost8) + ' монет'
    elif currentArmor == 9:
        return 'Алмазный Шлем. +3 к броне, -3 от ловкости. Цена: ' + str(armorCost9) + ' монет'
    elif currentArmor == 10:
        return 'Железные Нарукавники. +3 к броне. Цена: ' + str(armorCost10) + ' монет'
    elif currentArmor == 11:
        return 'Стальные Нарукавники. +3 к броне. Цена: ' + str(armorCost11) + ' монет'
    elif currentArmor == 12:
        return 'Алмазные Нарукавники. +4 к броне, -4 от ловкости. Цена: ' + str(armorCost12) + ' монет'
    elif currentArmor == 13:
        return 'Железная Кираса. +5 к броне. Цена: ' + str(armorCost13) + ' монет'
    elif currentArmor == 14:
        return 'Стальная Кираса. +7 к броне. Цена: ' + str(armorCost14) + ' монет'
    elif currentArmor == 15:
        return 'Алмазная Кираса. +8 к броне, -1 от ловкости. Цена: ' + str(armorCost15) + ' монет'


def enemies_list(m, removeUseless, characterLevel, keyboard):
    """
    Функция игры "РПГ", которая формирует список врагов.

    m -- сообщение пользователя
    removeUseless -- показывать бесполезных врагов или нет
    characterLevel -- уровень персонажа
    keyboard -- клавиатура для добавления кнопок

    returns: enemyList1 -- сформированная строка
    """
    keyboard.add(*[types.KeyboardButton(name) for name in ['1']])
    enemyList = 'Отсюда вы можете выбрать себе противника из списка, прислав мне соответствующее число. За победу вы будете получать опыт и монеты. '
    enemyList += 'Чем выше будет ваш уровень - тем с большим количеством врагов вы сможете сразиться. Возвращаться к более слабым противникам можно, но '
    enemyList += 'если ваш уровень будет слишком высок, то вы не получите ничего... Таковы правила. '
    enemyList += 'Только противники вам по уровню (и недалеко от него) смогут дать достаточный прирост опыта и золота.\n\n🦹Доступные противники:🦹'
    enemyList += '\n\n&&&1)🦹‍♂️ Злоумышленник\n🩸Здоровье: 10\n🛡Броня: 0\n🧿Выносливость: 0\n⚔Атака: 1-2\n⚡Ловкость: 0\n🔆Шанс Крита: 2\n💥Множитель Крита: 1.5\n'
    enemyList += '\n⚜ Способность: Отсутствует.\n\nНаграда за победу: +1 опыта 📖 и +5 золота 💰'
    if characterLevel >= 2:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 5:
            keyboard.add(*[types.KeyboardButton(name) for name in ['2']])
            enemyList += '\n\n&&&2) 🐲️ Гоблин\n🩸Здоровье: 15\n🛡Броня: 1\n🧿Выносливость: 1\n⚔Атака: 1-3\n⚡Ловкость: 5\n🔆Шанс Крита: 3\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность: "Эликсир". Гоблин использует свое зелье, чтобы обнулить вашу выносливость. Требует 3 единицы выносливости.'
            enemyList += '\n\nНаграда за победу: +2 опыта 📖 и +10 золота 💰 (0, если ваш уровень 5 или выше)'
    if characterLevel >= 3:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 6:
            keyboard.add(*[types.KeyboardButton(name) for name in ['3']])
            enemyList += '\n\n&&&3) 🧟 Зомби\n🩸Здоровье: 25\n🛡Броня: 0\n🧿Выносливость: 0\n⚔Атака: 3-4\n⚡Ловкость: 1\n🔆Шанс Крита: 5\n💥Множитель Крита: 2\n'
            enemyList += '\n⚜ Способность: "Укус". Зомби кусает вас, нанося 5 единиц урона. Через 3 хода вы превратитесь в зомби и проиграете бой. Требует 3 единицы выносливости.'
            enemyList += '\n\nНаграда за победу: +3 опыта 📖 и +20 золота 💰 (0, если ваш уровень 6 или выше)'
    if characterLevel >= 4:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 7:
            keyboard.add(*[types.KeyboardButton(name) for name in ['4']])
            enemyList += '\n\n&&&4)🐺 Оборотень\n🩸Здоровье: 50\n🛡Броня: 2\n🧿Выносливость: 2\n⚔Атака: 5-7\n⚡Ловкость: 7\n🔆Шанс Крита: 10\n💥Множитель Крита: 2\n'
            enemyList += '\n⚜ Способность: "Когти". Оборотень совершает мощную атаку когтями, нанося 8-10 урона и оставляя кровотечение, наносящее 1-2 ед урона 3 хода. Требует 6 единиц выносливости.'
            enemyList += '\n\nНаграда за победу: +5 опыта 📖 и +30 золота 💰 (0, если ваш уровень 7 или выше)'
    if characterLevel >= 5:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 8:
            keyboard.add(*[types.KeyboardButton(name) for name in ['5']])
            enemyList += '\n\n&&&5)🐃 Минотавр\n🩸Здоровье: 75\n🛡Броня: 3\n🧿Выносливость: 1\n⚔Атака: 8-10\n⚡Ловкость: 5\n🔆Шанс Крита: 15\n💥Множитель Крита: 2\n'
            enemyList += '\n⚜ Способность: "Таран". Минотавр сбивает вас с ног, заставляя пропустить ход. Требует 5 единиц выносливости.\n\n'
            enemyList += 'Награда за победу: +7 опыта 📖 и +40 золота 💰 (0, если ваш уровень 8 или выше)'
    if characterLevel >= 6:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 9:
            keyboard.add(*[types.KeyboardButton(name) for name in ['6']])
            enemyList += '\n\n&&&6)🐍 Василиск\n🩸Здоровье: 100\n🛡Броня: 4\n🧿Выносливость: 2\n⚔Атака: 11-13\n⚡Ловкость: 10\n🔆Шанс Крита: 20\n💥Множитель Крита: 2\n'
            enemyList += '\n⚜ Способность: "Яд". Василиск кусает вас, нанося 10 урона и впрыскивая смертоносный яд, отнимающий 1-3 единиц здоровья 5 ходов. Требует 5 единиц выносливости.'
            enemyList += '\n\nНаграда за победу: +10 опыта 📖 и +45 золота 💰 (0, если ваш уровень 9 или выше)'
    if characterLevel >= 7:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 10:
            keyboard.add(*[types.KeyboardButton(name) for name in ['7']])
            enemyList += '\n\n&&&7)🔥 Огненный Элементаль\n🩸Здоровье: 200\n🛡Броня: 5\n🧿Выносливость: 0\n⚔Атака: 15-18\n⚡Ловкость: 5\n🔆Шанс Крита: 25\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность (пассивная): "Жар". Элементаль заставляет вас гореть весь бой, что отнимает 1-5 единиц здоровья на каждом ходу.\n\n'
            enemyList += 'Награда за победу: +15 опыта 📖 и +50 золота 💰 (0, если ваш уровень 10 или выше)'
    if characterLevel >= 8:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 11:
            keyboard.add(*[types.KeyboardButton(name) for name in ['8']])
            enemyList += '\n\n&&&8)🦟 Инсектоид\n🩸Здоровье: 300\n🛡Броня: 6\n🧿Выносливость: 4\n⚔Атака: 20-25\n⚡Ловкость: 15\n🔆Шанс Крита: 30\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность: "Смертоносное комбо". Инсектоид атакует вас двумя своими базовыми атаками за 1 ход. Требует выносливости: 4\n\n'
            enemyList += 'Награда за победу: +20 опыта 📖 и +55 золота 💰(0, если ваш уровень 11 или выше)'
    if characterLevel >= 9:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 12:
            keyboard.add(*[types.KeyboardButton(name) for name in ['9']])
            enemyList += '\n\n&&&9)🤛🤚✋🤜 Многорукий\n🩸Здоровье: 400\n🛡Броня: 7\n🧿Выносливость: 5\n⚔Атака: 28-30\n⚡Ловкость: 5\n🔆Шанс Крита: 50\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность: "Мастер рукопашного боя". Многорукий атакует вас четырьмя своими базовыми атаками за 1 ход. Требует выносливости: 6'
            enemyList += '\n\nНаграда за победу: +25 опыта 📖 и +60 золота 💰 (0, если ваш уровень 12 или выше)'
    if characterLevel >= 10:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 13:
            keyboard.add(*[types.KeyboardButton(name) for name in ['10']])
            enemyList += '\n\n&&&10)🦂 Скорпион\n🩸Здоровье: 500\n🛡Броня: 8\n🧿Выносливость: 5\n⚔Атака: 31-33\n⚡Ловкость: 15\n🔆Шанс Крита: 32\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность: "Жало". Скорпион атакует вас хвостом, нанося 40 урона и оставля на 3 хода отравление, отнимающее 3-5 ед здоровья на каждом ходу. Требует выносливости: 5'
            enemyList += '\n\nНаграда за победу: +30 опыта 📖 и +65 золота 💰 (0, если ваш уровень 13 или выше)'
    if characterLevel >= 11:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 14:
            keyboard.add(*[types.KeyboardButton(name) for name in ['11']])
            enemyList += '&&&\n\n11)👨👨 Двуглавый Гигант\n🩸Здоровье: 600\n🛡Броня: 9\n🧿Выносливость: 4\n⚔Атака: 34-35\n⚡Ловкость: 0\n🔆Шанс Крита: 60\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность: "Глыба". Гигант бросает в вас огромную каменную глыбу, нанося 40 урона. Эта способность игнорирует уклонения и гарантированно критическая. Требует выносливости: 6'
            enemyList += '\n\nНаграда за победу: +35 опыта 📖 и +70 золота 💰 (0, если ваш уровень 14 или выше)'
    if characterLevel >= 12:
        if removeUseless == 0 or removeUseless == 1 and characterLevel < 15:
            keyboard.add(*[types.KeyboardButton(name) for name in ['12']])
            enemyList += '\n\n&&&12)⚰ Могильщик\n🩸Здоровье: 700\n🛡Броня: 10\n🧿Выносливость: 0\n⚔Атака: 38-40\n⚡Ловкость: 5\n🔆Шанс Крита: 35\n💥Множитель Крита: 1.5\n'
            enemyList += '\n⚜ Способность (пассивная): "Восставший из мертвых". За счет связи с мертвыми, могильщик имеет шанс 25% на то, чтобы после смерти возродиться с половиной здоровья.'
            enemyList += '\n\nНаграда за победу: +40 опыта 📖 и +75 золота 💰 (0, если ваш уровень 15)'
    if characterLevel >= 13:
        keyboard.add(*[types.KeyboardButton(name) for name in ['13']])
        enemyList += '\n\n&&&13)🦴 Войд\n🩸Здоровье: 800\n🛡Броня: 0\n🧿Выносливость: 0\n⚔Атака: 41-42\n⚡Ловкость: 55\n🔆Шанс Крита: 40\n💥Множитель Крита: 1.5\n'
        enemyList += '\n⚜ Способность (пассивная): "Регенератор". В начале хода Войд имеет 55% шанс на то, чтобы восстановить 30 здоровья.'
        enemyList += '\n\nНаграда за победу: +45 опыта 📖 и +85 золота 💰'
    if characterLevel >= 14:
        keyboard.add(*[types.KeyboardButton(name) for name in ['14']])
        enemyList += '\n\n&&&14)🧛 Пожиратель Героев\n🩸Здоровье: 1000\n🛡Броня: 15\n🧿Выносливость: 9\n⚔Атака: 44-45\n⚡Ловкость: 30\n🔆Шанс Крита: 45\n💥Множитель Крита: 2\n'
        enemyList += '\n⚜ Способность: "Пожиратель". Пожиратель Героев кусает вас, игнорируя уклонение. Наносит 100 урона и оставляет до конца боя кровотечение, наносящее 35 единиц урона за ход. Требует 3 выносливости.'
        enemyList += '\n\nНаграда за победу: +50 опыта 📖 и +100 золота 💰'
    if characterLevel >= 15:
        keyboard.add(*[types.KeyboardButton(name) for name in ['15']])
        enemyList += '\n\n&&&15)\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA Лидер\n🩸Здоровье: 5000\n🛡Броня: 30\n🧿Выносливость: 0\n⚔Атака: 50\n⚡Ловкость: 30\n🔆Шанс Крита: 90\n💥Множитель Крита: 2\n'
        enemyList += '\n⚜ Способность: "Обнуление". Лидер обнуляет ваше здоровье и выигрывает бой. Требует 8 единиц выносливости.\n\nНаграда за победу: +500 золота 💰&&&'
    return enemyList


def rpg_deaths_update(m):
    """
    Функция игры "РПГ", которая обновляет статистику по смертям.

    m -- сообщение пользователя
    """
    connection = get_connection()
    cursor = connection.cursor()
    deaths = 0
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        deaths = element['deaths']
    deaths += 1
    sql = "UPDATE `rpg_stat` SET deaths = %s WHERE `id` = %s"
    cursor.execute(sql, (deaths, m.chat.id))
    achCount = 0
    RPG25 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG25 = element['RPG25']
    if RPG25 == 0 and deaths >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG25 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Так, давай по новой". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_attacks_update(m):
    """
    Функция игры "РПГ", которая обновляет статистику по совершенным атакам.

    m -- сообщение пользователя
    """
    connection = get_connection()
    cursor = connection.cursor()
    attackUse = 0
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        attackUse = element['attackUse']
    attackUse += 1
    sql = "UPDATE `rpg_stat` SET attackUse = %s WHERE `id` = %s"
    cursor.execute(sql, (attackUse, m.chat.id))
    achCount = 0
    RPG26 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG26 = element['RPG26']
    if RPG26 == 0 and attackUse >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG26 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "В атаку!". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_lost_turns_update(m):
    """
    Функция игры "РПГ", которая обновляет статистику по пропущенным ходам.

    m -- сообщение пользователя
    """
    connection = get_connection()
    cursor = connection.cursor()
    losedTurns = 0
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        losedTurns = element['losedTurns']
    losedTurns += 1
    sql = "UPDATE `rpg_stat` SET losedTurns = %s WHERE `id` = %s"
    cursor.execute(sql, (losedTurns, m.chat.id))
    achCount = 0
    RPG27 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG27 = element['RPG27']
    if RPG27 == 0 and losedTurns >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG27 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Отступление!". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def enemy_kills_update(m, curGold, curBattle):
    """
    Функция игры "РПГ", которая обновляет статистику по убийствам врагов.

    m -- сообщение пользователя
    curGold -- заработанное золото
    curBattle -- с кем был бой
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    enemiesKilled = 0
    allTimeGold = 0
    enemy1killed = 0
    enemy2killed = 0
    enemy3killed = 0
    enemy4killed = 0
    enemy5killed = 0
    enemy6killed = 0
    enemy7killed = 0
    enemy8killed = 0
    enemy9killed = 0
    enemy10killed = 0
    enemy11killed = 0
    enemy12killed = 0
    enemy13killed = 0
    enemy14killed = 0
    enemy15killed = 0
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        enemiesKilled = element['enemiesKilled']
        allTimeGold = element['allTimeGold']
        enemy1killed = element['enemy1killed']
        enemy2killed = element['enemy2killed']
        enemy3killed = element['enemy3killed']
        enemy4killed = element['enemy4killed']
        enemy5killed = element['enemy5killed']
        enemy6killed = element['enemy6killed']
        enemy7killed = element['enemy7killed']
        enemy8killed = element['enemy8killed']
        enemy9killed = element['enemy9killed']
        enemy10killed = element['enemy10killed']
        enemy11killed = element['enemy11killed']
        enemy12killed = element['enemy12killed']
        enemy13killed = element['enemy13killed']
        enemy14killed = element['enemy14killed']
        enemy15killed = element['enemy15killed']
    enemiesKilled += 1
    allTimeGold += curGold
    if curBattle == 1:
        enemy1killed += 1
    elif curBattle == 2:
        enemy2killed += 1
    elif curBattle == 3:
        enemy3killed += 1
    elif curBattle == 4:
        enemy4killed += 1
    elif curBattle == 5:
        enemy5killed += 1
    elif curBattle == 6:
        enemy6killed += 1
    elif curBattle == 7:
        enemy7killed += 1
    elif curBattle == 8:
        enemy8killed += 1
    elif curBattle == 9:
        enemy9killed += 1
    elif curBattle == 10:
        enemy10killed += 1
    elif curBattle == 11:
        enemy11killed += 1
    elif curBattle == 12:
        enemy12killed += 1
    elif curBattle == 13:
        enemy13killed += 1
    elif curBattle == 14:
        enemy14killed += 1
    elif curBattle == 15:
        enemy15killed += 1
    sql = "UPDATE `rpg_stat` SET enemiesKilled = %s, allTimeGold = %s, enemy1killed = %s, enemy2killed = %s, enemy3killed = %s, enemy4killed = %s, enemy5killed = %s, enemy6killed = %s, enemy7killed = %s, enemy8killed = %s, enemy9killed = %s, enemy10killed = %s, enemy11killed = %s, enemy12killed = %s, enemy13killed = %s, enemy14killed = %s, enemy15killed = %s WHERE `id` = %s"
    cursor.execute(sql, (enemiesKilled, allTimeGold, enemy1killed, enemy2killed, enemy3killed, enemy4killed, enemy5killed, enemy6killed, enemy7killed, enemy8killed, enemy9killed, enemy10killed, enemy11killed, enemy12killed, enemy13killed, enemy14killed, enemy15killed, m.chat.id))
    achCount = 0
    RPG5 = 0
    RPG6 = 0
    RPG7 = 0
    RPG8 = 0
    RPG9 = 0
    RPG10 = 0
    RPG11 = 0
    RPG12 = 0
    RPG13 = 0
    RPG14 = 0
    RPG15 = 0
    RPG16 = 0
    RPG17 = 0
    RPG18 = 0
    RPG19 = 0
    RPG20 = 0
    RPG21 = 0
    RPG22 = 0
    RPG23 = 0
    RPG24 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG5 = element['RPG5']
        RPG6 = element['RPG6']
        RPG7 = element['RPG7']
        RPG8 = element['RPG8']
        RPG9 = element['RPG9']
        RPG10 = element['RPG10']
        RPG11 = element['RPG11']
        RPG12 = element['RPG12']
        RPG13 = element['RPG13']
        RPG14 = element['RPG14']
        RPG15 = element['RPG15']
        RPG16 = element['RPG16']
        RPG17 = element['RPG17']
        RPG18 = element['RPG18']
        RPG19 = element['RPG19']
        RPG20 = element['RPG20']
        RPG21 = element['RPG21']
        RPG22 = element['RPG22']
        RPG23 = element['RPG23']
        RPG24 = element['RPG24']
    if RPG5 == 0 and enemiesKilled >= 10:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Я убивал когда-то монстров...". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG6 == 0 and enemiesKilled >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG6 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Профессиональный убийца". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG7 == 0 and enemiesKilled >= 1000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG7 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Уничтожитель". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG8 == 0 and enemiesKilled >= 5000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG8 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Вижу - убиваю". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG9 == 0 and enemy1killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG9 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Преступник тоже монстр". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG10 == 0 and enemy2killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG10 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Ненавижу гоблинов". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG11 == 0 and enemy3killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG11 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Зомби апокалипсис предотвращен". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG12 == 0 and enemy4killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG12 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Вой на Луну". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG13 == 0 and enemy5killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG13 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Быка за рога". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG14 == 0 and enemy6killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG14 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Больше не пошипишь". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG15 == 0 and enemy7killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG15 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Пожарный". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG16 == 0 and enemy8killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG16 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Дезинсектор". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG17 == 0 and enemy9killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG17 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Безрукий". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG18 == 0 and enemy10killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG18 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Жало отжали". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG19 == 0 and enemy11killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG19 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Одна голова хорошо, а две тоже не помогли". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG20 == 0 and enemy12killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG20 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "В могиле тебе и место". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG21 == 0 and enemy13killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG21 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Регенератор". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG22 == 0 and enemy14killed >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG22 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Хорошо, что я не герой". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG23 == 0 and enemy15killed >= 1:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG23 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Древнее зло побеждено!". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if RPG24 == 0 and allTimeGold >= 100000:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG24 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Золотая ручка". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def txt_deaths_update(m):
    """
    Функция игры "Текстовый Квест", которая обновляет статистику по смертям.

    m -- сообщение пользователя

    returns: deaths -- общее количество смертей
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    deaths = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `quest_deaths` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for kek in cursor:
        len += 1
    if len != 0:
        sql = "SELECT * FROM `quest_deaths` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            deaths = element['deaths']
        deaths += 1
        sql = "UPDATE `quest_deaths` SET `deaths` = %s WHERE `id` = %s"
        cursor.execute(sql, (deaths, m.chat.id))
    else:
        sql = "INSERT INTO `quest_deaths`(`id`, `deaths`) VALUES (%s, %s)"
        deaths += 1
        cursor.execute(sql, (m.chat.id, deaths))
    achCount = 0
    TXT3 = 0
    TXT4 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        TXT3 = element['TXT3']
        TXT4 = element['TXT4']
    if TXT3 == 0 and deaths >= 10:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT3 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "C первого раза ни у кого не получалось...". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TXT4 == 0 and deaths >= 100:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Король суицида". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()
    return deaths


def txt_hard_complete(m, speedRun):
    """
    Функция игры "Текстовый Квест", которая открывает достижения за сложное прохождение.

    m -- сообщение пользователя
    speedRun -- переменная для проверки на кратчайшее прохождение
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    TXT1 = 0
    TXT2 = 0
    TXT5 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        TXT1 = element['TXT1']
        TXT2 = element['TXT2']
        TXT5 = element['TXT5']
    if TXT1 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT1 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Cапер". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TXT2 == 0 and speedRun == 14:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT2 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Спидранер". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    if TXT5 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Выживший". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def txt_easy_complete(m):
    """
    Функция игры "Текстовый Квест", которая открывает достижения за простое прохождение.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    TXT5 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        TXT5 = element['TXT5']
    if TXT5 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, TXT5 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Выживший". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_level_achievement(m):
    """
    Функция игры "РПГ", которая открывает достижение за максимальный уровень.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    RPG1 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG1 = element['RPG1']
    if RPG1 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG1 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Повзрослел". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_gold_achievement(m):
    """
    Функция игры "РПГ", которая открывает достижение за накопление 5000 золота.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    RPG2 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG2 = element['RPG2']
    if RPG2 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG2 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Крохобор". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_weapons_achievement(m):
    """
    Функция игры "РПГ", которая открывает достижение за покупку всего оружия.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    RPG3 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG3 = element['RPG3']
    if RPG3 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG3 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Мастер оружейник". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()


def rpg_armors_achievement(m):
    """
    Функция игры "РПГ", которая открывает достижение за покупку всех броней.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    achCount = 0
    RPG4 = 0
    sql = "SELECT * FROM `achievements` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        achCount = element['achCount']
        RPG4 = element['RPG4']
    if RPG4 == 0:
        achCount += 1
        sql = "UPDATE `achievements` SET achCount = %s, RPG4 = %s WHERE `id` = %s"
        cursor.execute(sql, (achCount, 1, m.chat.id))
        bot.send_message(m.chat.id, '🏆Вы открыли достижение "Мастер бронник". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
    connection.commit()
    connection.close()

