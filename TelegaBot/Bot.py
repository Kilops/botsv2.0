# -*- coding: utf-8 -*-

import random
from datetime import datetime
import test_questions                                                            #файл с вопросами для теста
from some_database_functions import *                                            #глобальные перменные, длинные строки, а также многие функции базы данных

alive = False
globalEggHP = 0

@bot.message_handler(commands=['start'])
def start(m):
    """
    Стартовая точка, в которой бот приветствует пользователя, добавляет первые кнопки и посылает ответ пользователя в функцию menu.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    connection = get_connection()
    cursor = connection.cursor()
    global alive
    if alive == False:
        alive = True
        sql = "UPDATE `start` SET isRunning = %s"
        cursor.execute(sql, (0))
    isRunning = 0
    random.seed(datetime.now())
    sql = "SELECT * FROM `start` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for i in cursor:
        len += 1
    if len != 0:
        sql = "SELECT * FROM `start` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            isRunning = element['isRunning']
    else:
        sql = "INSERT INTO `start`(`id`, isRunning) VALUES (%s, %s)"
        cursor.execute(sql, (m.chat.id, isRunning))
    if isRunning == 0:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Хочу поиграть', 'Достижения', 'Помощь', 'Что нового?']])
        msg = bot.send_message(m.chat.id, 'Приветствую! Чего вам надобно?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, menu)
        sql = "UPDATE `start` SET isRunning = %s WHERE `id` = %s"
        cursor.execute(sql, (1, m.chat.id))
    connection.commit()
    connection.close()


@bot.message_handler(content_types=['text', 'sticker', 'pinned_message', 'photo', 'audio', 'document', 'video', 'video_note', 'voice', 'location', 'contact'])
def send_text(m):
    """
    Функция, срабатывающая в случае, если бот был перезагружен, а пользователь находится не в стартовой точке.

    m -- какое-либо сообщения пользователя.
    """
    bot.send_message(m.chat.id, 'Я был перезагружен. Нажми сюда:\n/start\nА можешь просто отправить эту команду вручную и попадешь в главное меню.')


def menu(m):
    """
    Функция, обрабатывающая сообщение, полученное в функции start. Создает кнопки с играми и посылает ответ в функцию submenu или присылает сообщение с описанием игр.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    achievements_init(m)
    if m.text == u'Хочу поиграть':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Тест', 'Кости', 'RPG', 'Текстовый Квест', 'Разбей Яйцо', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Во что вы хотите поиграть?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, submenu)
    elif m.text == u'Достижения':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Рейтинг игроков', 'Назад']])
        info = achievements_load(m)
        list = info.split("\n\n")
        string = ''
        for x in range(0, len(list)):
            string += list[x]
            string += '\n\n'
            if x % 15 == 0 and x != 0 or x == len(list)-1:
                bot.send_message(m.chat.id, string.decode('utf-8'))
                string = ''
        msg = bot.send_message(m.chat.id, 'Можешь посмотреть сколько достижений у других игроков.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, achievementMenu)
    elif m.text == u'Помощь':
        info = help_string()
        list = info.split("\n\n")
        string = ''
        for x in range(0, len(list)):
            string += list[x]
            string += '\n\n'
            if x % 5 == 0 and x != 0 or x == len(list)-1:
                bot.send_message(m.chat.id, string.decode('utf-8'))
                string = ''
        msg = bot.send_message(m.chat.id, 'Надеюсь, что теперь у тебя не осталось вопросов, но если что, то всегда можно связаться с создателем.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, menu)
    elif m.text == u'Что нового?':
        msg = bot.send_message(m.chat.id, update_string(), reply_markup=keyboard)
        bot.register_next_step_handler(msg, menu)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, menu)


def submenu(m):
    """
    Функция, обрабатывающая сообщение, полученное в функции menu. В зависимости от полученного сообщения перенаправляет в функцию выбранной игры.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Тест':
        starttest(m)
    elif m.text == u'Кости':
        startKosti(m)
    elif m.text == u'RPG':
        startRPG(m)
    elif m.text == u'Текстовый Квест':
        startTXT(m)
    elif m.text == u'Разбей Яйцо':
        startEgg(m)
    elif m.text == u'Назад' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, submenu)


def achievementMenu(m):
    """
    Функция, которая показывает пользователю его достижения и позволяет посмотреть на каком он месте среди других игроков по их количеству.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Рейтинг игроков':
        msg = bot.send_message(m.chat.id, achievement_leaders(m), reply_markup=keyboard, parse_mode= "Markdown")
        bot.register_next_step_handler(msg, achievementMenu)
    elif m.text == u'Назад' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, achievementMenu)


def starttest(m):
    """
    Функция игры "Тест", которая начинает тест, генерируя порядок вопросов, а также задает первый из них.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    number = 0                                                       #номер вопроса в списке вопросов, который нужно найти
    i = 0                                                            #порядковый номер текущего вопроса
    ques = list(range(1, 101))                                       #список чисел для поиска случайного вопроса. Верхний предел должен быть на 1 больше, чем всего вопросов
    points = 0                                                       #очки пользователя, набирающиеся в текущей серии вопросов
    random.seed(datetime.now())
    random.shuffle(ques)                                             #случайным образом перемешиваем список цифр
    number = ques[i]                                                 #получаем первую цифру из списка, это будет номер текущего вопроса
    testquestion = test_questions.findquestion(number)                              #находим вопрос в списке вопросов по случайно полученному номеру
    keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Вопрос №' + str(i + 1) + '\n' + testquestion, reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: test(m, number, i, ques, points))                  #создаем кнопки для вариантов ответа, задаем вопрос и ждем на него ответа


def test(m, number, i, ques, points):
    """
    Функция игры "Тест", которая посылает ответ пользователя в функцию, проверяющую ответ на правильность, а затем выводит сообщение о том, был ли ответ правильным.

    m -- сообщение пользователя
    number -- номер текущего вопроса из списка
    i -- порядковый номер заданного пользователю вопроса
    ques -- перемешанный список номеров вопросов, откуда берется number
    points -- текущий счет пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    wasAnswered = False
    if m.text == '1':
        answer = 1                                                                 #узнаем, как ответил пользователь
        wasAnswered = True
    elif m.text == '2':
        answer = 2
        wasAnswered = True
    elif m.text == '3':
        answer = 3
        wasAnswered = True
    elif m.text == '4':
        answer = 4
        wasAnswered = True
    elif m.text == u'Назад' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: test(m, number, i, ques, points))
    if wasAnswered == True:
        right = test_questions.checkisitright(number, answer)                                      #сравниваем ответ данный пользователем, с заготовленным по номеру вопроса
        if right == 1:
            bot.send_message(m.from_user.id, "✅")
            points = points + 1                                                     #если ответ был верным, то добавляем очко
        else:
            bot.send_message(m.from_user.id, "❌")                        #иначе не добавляем
        newquestion(m, number, i, ques, points)                                     #узнаем нужно ли задавать следующий вопрос


def newquestion(m, number, i, ques, points):
    """
    Функция игры "Тест", которая выводит пользователю новый вопрос.

    m -- сообщение пользователя
    number -- номер текущего вопроса из списка
    i -- порядковый номер заданного пользователю вопроса
    ques -- перемешанный список номеров вопросов, откуда берется number
    points -- текущий счет пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    allPoints = 0
    i = i + 1                                                                                        #увеличиваем счетчик вопросов
    if i < 5:                                                                                         #если вопросов было задано меньше пяти, то задаем следующий
        number = ques[i]                                                                              #узнаем номер следующего вопроса
        testquestion = test_questions.findquestion(number)                                                          #находим вопрос по номеру в списке вопросов
        keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Вопрос №' + str(i+1) + '\n' + testquestion, reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: test(m, number, i, ques, points))
    else:                                                                                               #в противном случае завершаем игру
        keyboard.add(*[types.KeyboardButton(name) for name in ['Да, давай ещё разок', 'Нет, достаточно', 'Список лидеров']])
        bot.send_message(m.chat.id, 'Вы прошли тест и набрали ' + str(points) + ' баллов.', reply_markup=keyboard)
        allPoints = test_save_and_achievements(m, allPoints, points)
        msg = bot.send_message(m.chat.id, 'За все время вы набрали ' + str(allPoints) + ' баллов. Хотите ещё?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, yesnoaftercompletedtest)            #предлагаем пользователю сыграть еще раз


def yesnoaftercompletedtest(m):
    """
    Функция игры "Тест", которая обрабатывает решение пользователя о продолжении игры и либо начинает новый тест, либо выпускает его из игры.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Да, давай ещё разок':
        starttest(m)
    elif m.text == u'Нет, достаточно':
        bot.send_message(m.chat.id, 'Спасибо за игру.', reply_markup=keyboard)
        start(m)
    elif m.text == u'Список лидеров':
        msg = bot.send_message(m.chat.id, test_leaders(m), reply_markup=keyboard, parse_mode= "Markdown")
        bot.register_next_step_handler(msg, yesnoaftercompletedtest)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, yesnoaftercompletedtest)


def startKosti(m):
    """
    Функция игры "Кости", которая начинает игру, узнавая какое количество костей предстоит бросить.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', '5', 'Назад']])
    msg = bot.send_message(m.chat.id, '🎲 Укажите количество костей, которые будут бросаться 🎲', reply_markup=keyboard)
    bot.register_next_step_handler(msg, checkKostiInput)


def checkKostiInput(m):
    """
    Функция игры "Кости", которая выставляет количество костей в зависимости от решения пользователя.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    kostiCount = 0
    if m.text == '1':                                                           #выставляем количество костей таким, каким решит пользователь
        kostiCount = 1
        startKosti2(m, kostiCount)
    elif m.text == '2':
        kostiCount = 2
        startKosti2(m, kostiCount)
    elif m.text == '3':
        kostiCount = 3
        startKosti2(m, kostiCount)
    elif m.text == '4':
        kostiCount = 4
        startKosti2(m, kostiCount)
    elif m.text == '5':
        kostiCount = 5
        startKosti2(m, kostiCount)
    elif m.text == u'Назад' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, checkKostiInput)


def startKosti2(m, kostiCount):
    """
    Функция игры "Кости", которая узнает сколько раз бросать кости.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', '5', '6', 'Назад']])
    msg = bot.send_message(m.chat.id, '🎲 Укажите количество бросков 🎲', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: checkTrowsInput(m, kostiCount))


def checkTrowsInput(m, kostiCount):
    """
    Функция игры "Кости", которая выставляет количество бросков в зависимости от решения пользователя.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throws = 0
    if m.text == '1':                                                           #теперь принимаем количество бросков
        throws = 1
        botOrHuman(m, kostiCount, throws)
    elif m.text == '2':
        throws = 2
        botOrHuman(m, kostiCount, throws)
    elif m.text == '3':
        throws = 3
        botOrHuman(m, kostiCount, throws)
    elif m.text == '4':
        throws = 4
        botOrHuman(m, kostiCount, throws)
    elif m.text == '5':
        throws = 5
        botOrHuman(m, kostiCount, throws)
    elif m.text == '6':
        throws = 6
        botOrHuman(m, kostiCount, throws)
    elif m.text == u'Назад':
        startKosti(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: checkTrowsInput(m, kostiCount))


def botOrHuman(m, kostiCount, throws):
    """
    Функция игры "Кости", которая узнает хочет ли игрок сыграть с ботом или с другом.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['С Ботом', 'С другом', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Вы хотите сыграть со мной 🤖 или с другом 👨?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: checkBotOrHuman(m, kostiCount, throws))


def checkBotOrHuman(m, kostiCount, throws):
    """
    Функция игры "Кости", которая меняет формат игры в зависимости от принятого ранее решения играть с ботом или с другом.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'С Ботом':                                                    #функции немного отличаются в зависимости от режима игры
        findBotPointsAndStartTrowingPlayer(m, kostiCount, throws)
    elif m.text == u'С другом':
        friendThrowing(m, kostiCount, throws)
    elif m.text == u'Назад':
        startKosti2(m, kostiCount)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: checkBotOrHuman(m, kostiCount, throws))


def friendThrowing(m, kostiCount, throws):
    """
    Функция игры "Кости", которая спрашивает каким образом хочет бросить кости первый игрок.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Давай сразу', 'По порядку', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Решите кто будет первым игроком. Итак, уважаемый игрок номер 1️⃣, бросить сразу или последовательно?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKosteiFriend(m, kostiCount, throws))


def choTamPoResheniuNaschetKosteiFriend(m, kostiCount, throws):
    """
    Функция игры "Кости", которая реализует выбор формата бросков первого игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throwTempThrows = 0                                                         #зануляем эти переменные для пошагового бросания
    friendPoints = 0
    if m.text == u'Давай сразу':
        findFriendPointsFast(m, kostiCount, throws)
    elif m.text == u'По порядку':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'Ну давай, бросай.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findFriendPointsSlow(m, kostiCount, throws, throwTempThrows, friendPoints))
    elif m.text == u'Назад':
        botOrHuman(m, kostiCount, throws)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKosteiFriend(m, kostiCount, throws))


def findFriendPointsFast(m, kostiCount, throws):
    """
    Функция игры "Кости", которая быстро бросает кости за первого игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    friendPoints = 0
    curThrow = 1
    curKost = 1
    for curThrow in range(throws):                                              #бросаем кости заданное количество раз
        curKost = 1
        for curKost in range(kostiCount):                                       #бросаем заданное количество костей
            random.seed(datetime.now())
            friendPoints += random.randint(1,6)
    bot.send_message(m.chat.id, 'Бросая кости, первый игрок набрал ' + str(friendPoints) + ' очков.', reply_markup=keyboard)
    youThrowing(m, kostiCount, throws, friendPoints)


def findFriendPointsSlow(m, kostiCount, throws, throwTempThrows, friendPoints):
    """
    Функция игры "Кости", которая дает первому врагу бросать кости последовательно.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    friendPoints -- набранные первым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Бросок':
        makeFriendThrow(m, kostiCount, throws, throwTempThrows, friendPoints)
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findFriendPointsSlow(m, kostiCount, throws, throwTempThrows, friendPoints))


def makeFriendThrow(m, kostiCount, throws, throwTempThrows, friendPoints):
    """
    Функция игры "Кости", которая реализует каждый бросок первого игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    friendPoints -- набранные первым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throwTempKostiCount = 1
    if throwTempThrows < throws:
        throwTempThrows += 1
        for throwTempKostiCount in range(kostiCount):
            random.seed(datetime.now())
            friendPoints += random.randint(1,6)
    if throwTempThrows == throws:
        bot.send_message(m.chat.id, 'Бросая кости, первый игрок набрал ' + str(friendPoints) + ' очков.', reply_markup=keyboard)
        youThrowing(m, kostiCount, throws, friendPoints)
    else:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'На данный момент у первого игрока ' + str(friendPoints) + ' очков. Бросаем дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findFriendPointsSlow(m, kostiCount, throws, throwTempThrows, friendPoints))


def youThrowing(m, kostiCount, throws, friendPoints):
    """
    Функция игры "Кости", которая спрашивает каким образом хочет бросить кости второй игрок.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    friendPoints -- набранные первым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Давай сразу', 'По порядку', 'Выход']])
    msg = bot.send_message(m.chat.id, 'Итак, теперь игрок номер 2️⃣. Бросить сразу или последовательно?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKosteiYou(m, kostiCount, throws, friendPoints))


def choTamPoResheniuNaschetKosteiYou(m, kostiCount, throws, friendPoints):
    """
    Функция игры "Кости", которая реализует выбор формата бросков второго игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    friendPoints -- набранные первым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throwTempThrows = 0
    yourPoints = 0
    if m.text == u'Давай сразу':
        findYourPointsFast(m, kostiCount, throws, friendPoints)
    elif m.text == u'По порядку':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'Ну давай, бросай.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findYourPointsSlow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints))
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKosteiYou(m, kostiCount, throws, friendPoints))


def findYourPointsFast(m, kostiCount, throws, friendPoints):
    """
    Функция игры "Кости", которая быстро бросает кости за второго игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    friendPoints -- набранные первым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    yourPoints = 0
    curThrow = 1
    curKost = 1
    for curThrow in range(throws):
        curKost = 1
        for curKost in range(kostiCount):
            random.seed(datetime.now())
            yourPoints += random.randint(1,6)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Да!', 'Не...']])
    if yourPoints > friendPoints:
        msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это победа второго игрока! Ещё раз?', reply_markup=keyboard)
    elif yourPoints < friendPoints:
        msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это победа первого игрока! Ещё раз?', reply_markup=keyboard)
    elif yourPoints == friendPoints:
        msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это ничья! Ещё раз?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, againKostiOrNo)


def findYourPointsSlow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints):
    """
    Функция игры "Кости", которая дает второму врагу бросать кости последовательно.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    friendPoints -- набранные первым игроком очки
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    yourPoints -- набранные вторым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Бросок':
        makeYourThrow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints)
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findYourPointsSlow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints))


def makeYourThrow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints):
    """
    Функция игры "Кости", которая реализует каждый бросок второго игрока.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    friendPoints -- набранные первым игроком очки
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    yourPoints -- набранные вторым игроком очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throwTempKostiCount = 1
    if throwTempThrows < throws:
        throwTempThrows += 1
        for throwTempKostiCount in range(kostiCount):
            random.seed(datetime.now())
            yourPoints += random.randint(1,6)
    if throwTempThrows == throws:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Да!', 'Не...']])
        if yourPoints > friendPoints:
            msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это победа второго игрока! Ещё раз?', reply_markup=keyboard)
        elif yourPoints < friendPoints:
            msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это победа первого игрока! Ещё раз?', reply_markup=keyboard)
        elif yourPoints == friendPoints:
            msg = bot.send_message(m.chat.id, 'Бросая кости, второй игрок набрал ' + str(yourPoints) + ' очков. Это ничья! Ещё раз?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, againKostiOrNo)
    else:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'На данный момент у второго игрока ' + str(yourPoints) + ' очков. Бросаем дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findYourPointsSlow(m, kostiCount, throws, friendPoints, throwTempThrows, yourPoints))


def findBotPointsAndStartTrowingPlayer(m, kostiCount, throws):
    """
    Функция игры "Кости", которая бросает кости за бота и дает игроку выбор: бросить кости сразу или бросать по порядку столько раз, сколько он до этого выбрал.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    """
    botPoints = 0
    curThrow = 1
    curKost = 1
    for curThrow in range(throws):
        curKost = 1
        for curKost in range(kostiCount):
            random.seed(datetime.now())
            botPoints += random.randint(1,6)
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Давай сразу', 'По порядку', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Бросая кости, бот набрал ' + str(botPoints) + ' очков. Теперь ваша очередь. Бросить сразу или последовательно?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKostei(m, kostiCount, throws, botPoints))


def choTamPoResheniuNaschetKostei(m, kostiCount, throws, botPoints):
    """
    Функция игры "Кости", которая обрабатывает решения пользователя о том, бросать последовательно или моментально.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    botPoints -- очки, набранные ботом
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    throwTempThrows = 0
    yourPoints = 0
    if m.text == u'Давай сразу':
        findPlayerPointsFast(m, kostiCount, throws, botPoints)
    elif m.text == u'По порядку':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'Ну давай, бросай.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findPlayerPointsSlow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints))
    elif m.text == u'Назад':
        botOrHuman(m, kostiCount, throws)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: choTamPoResheniuNaschetKostei(m, kostiCount, throws, botPoints))


def saveKosti(m, playerWins, botWins):
    """
    Функция игры "Кости", которая сохраняет количество побед.

    m -- сообщение пользователя
    playerWins -- количество побед игрока
    botWins -- количество побед бота
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    msg = bot.send_message(m.chat.id, kosti_database(m, playerWins, botWins), reply_markup=keyboard)
    bot.register_next_step_handler(msg, againKostiOrNo)


def findPlayerPointsFast(m, kostiCount, throws, botPoints):
    """
    Функция игры "Кости", которая быстро бросает кости за игрока и сообщает ему результат, сравнивая его очки с очками, набранными ботом.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    botPoints -- очки, набранные ботом
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    yourPoints = 0
    botWins = 0
    playerWins = 0
    curThrow = 1
    curKost = 1
    for curThrow in range(throws):
        curKost = 1
        for curKost in range(kostiCount):
            random.seed(datetime.now())
            yourPoints += random.randint(1,6)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Да!', 'Не...']])
    if yourPoints > botPoints:
        bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Это победа! 🏅', reply_markup=keyboard)
        playerWins += 1
    elif yourPoints < botPoints:
        bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Увы, но вы проиграли!😭😭😭', reply_markup=keyboard)
        botWins += 1
    elif yourPoints == botPoints:
        bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Прям как бот. Это ничья! 🤝', reply_markup=keyboard)
        playerWins += 1
        botWins += 1
    saveKosti(m, playerWins, botWins)


def findPlayerPointsSlow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints):
    """
    Функция игры "Кости", которая позволяет игроку сделать отдельный бросок.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    botPoints -- очки, набранные ботом
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    yourPoints -- набранные вами очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Бросок':
        makeThrow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints)
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findPlayerPointsSlow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints))


def makeThrow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints):
    """
    Функция игры "Кости", которая будет добавлять очки пользователю за каждый бросок до тех пор, пока его броски не закончатся.

    m -- сообщение пользователя
    kostiCount -- количество бросаемых костей
    throws -- количество бросков
    botPoints -- очки, набранные ботом
    throwTempThrows -- временная переменная, чтобы знать, сколько бросков осталось
    yourPoints -- набранные вами очки
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    botWins = 0
    playerWins = 0
    throwTempKostiCount = 1
    if throwTempThrows < throws:
        throwTempThrows += 1
        for throwTempKostiCount in range(kostiCount):
            random.seed(datetime.now())
            yourPoints += random.randint(1,6)
    if throwTempThrows == throws:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Да!', 'Не...']])
        if yourPoints > botPoints:
            bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Это победа! 🏅', reply_markup=keyboard)
            playerWins += 1
        elif yourPoints < botPoints:
            bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Увы, но вы проиграли!😭😭😭', reply_markup=keyboard)
            botWins += 1
        elif yourPoints == botPoints:
            bot.send_message(m.chat.id, 'Бросая кости, вы набрали ' + str(yourPoints) + ' очков. Прям как бот. Это ничья! 🤝', reply_markup=keyboard)
            playerWins += 1
            botWins += 1
        saveKosti(m, playerWins, botWins)
    else:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Бросок', 'Выход']])
        msg = bot.send_message(m.chat.id, 'На данный момент у вас ' + str(yourPoints) + ' очков. Бросаем дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: findPlayerPointsSlow(m, kostiCount, throws, botPoints, throwTempThrows, yourPoints))


def againKostiOrNo(m):
    """
    Функция игры "Кости", которая узнает, хочет ли пользователь начать игру заново.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Да!':
        startKosti(m)
    elif m.text == u'Не...' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, againKostiOrNo)


def startRPG(m):
    """
    Функция игры "РПГ", которая начинает игру и приветствует пользователя.

    m -- сообщение пользователя
    """
    characterName = 'Имени нет'
    characterClass = 'Не выбран'
    characterLevel = 1
    health = 10
    armor = 0
    stamina = 3
    lowDamage = 1
    highDamage = 2
    agility = 1
    critChance = 5
    critMultiplier = 1.5
    gold = 10
    experience = 0
    experienceTarget = 3
    critUpgrades = 0
    agilityUpgrades = 0
    staminaUpgrades = 0
    currentWeapon = 1
    currentArmor = 1
    curYourPower = 1
    curYourPowerStaminaCost = 2
    removeUseless = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `rpg` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for i in cursor:
        len += 1
    if len != 0:
        sql = "SELECT * FROM `rpg` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            characterName = element['characterName'].encode('utf-8')
            characterClass = element['characterClass'].encode('utf-8')
            characterLevel = element['characterLevel']
            health = element['health']
            armor = element['armor']
            stamina = element['stamina']
            lowDamage = element['lowDamage']
            highDamage = element['highDamage']
            agility = element['agility']
            critChance = element['critChance']
            critMultiplier = element['critMultiplier']
            gold = element['gold']
            experience = element['experience']
            experienceTarget = element['experienceTarget']
            critUpgrades = element['critUpgrades']
            agilityUpgrades = element['agilityUpgrades']
            staminaUpgrades = element['staminaUpgrades']
            currentWeapon = element['currentWeapon']
            currentArmor = element['currentArmor']
            curYourPower = element['curYourPower']
            curYourPowerStaminaCost = element['curYourPowerStaminaCost']
    else:
        sql = "INSERT INTO `rpg`(`id`, `characterName`, `characterClass`, `characterLevel`, `health`, `armor`, `stamina`, `lowDamage`, `highDamage`, `agility`, `critChance`, `critMultiplier`, `gold`, `experience`, `experienceTarget`, `critUpgrades`, `agilityUpgrades`, `staminaUpgrades`, `currentWeapon`, `currentArmor`, `curYourPower`, `curYourPowerStaminaCost`, `removeUseless`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql, (m.chat.id, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, removeUseless))
    sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for i in cursor:
        len += 1
    if len == 0:
        sql = "INSERT INTO `rpg_stat`(`id`) VALUES (%s)"
        cursor.execute(sql, (m.chat.id))
    connection.commit()
    connection.close()
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Добро пожаловать в фанастический ми... да какая разница. Вам нужен сюжет или вы пришли потыкать монстров? Вперёд!', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая перенаправляет пользователя в разные пункты меню.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Задания':
        startQuest(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Лавка':
        magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Персонаж':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Назад' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая показывает характеристики персонажа и предлагает что-то в нем изменить/улучшить.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    clearGame = 0
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if characterName == 'Имени нет':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Назад']])
        msg = bot.send_message(m.chat.id, 'Введите имя персонажа.\n\nОтнеситесь к этому ответственно, ведь позже вы не сможете его сменить просто так!', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    else:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Изменить имя', 'Изменить характер', 'Выбрать Способность', 'Прокачка', 'Статистика', 'Рейтинг игроков', 'Помощь', 'Сбросить прогресс', 'Назад']])
        msg = bot.send_message(m.chat.id, '🦸 ИМЯ ПЕРСОНАЖА: ' + str(characterName) + '\n'
        '🎭 ХАРАКТЕР: ' + str(characterClass) + '\n'
        '💰 ЗОЛОТО: ' + str(gold) + '\n'
        '💪 УРОВЕНЬ: ' + str(characterLevel) + '\n'
        '📖 ОПЫТ: ' + str(experience) + '/' + str(experienceTarget) + '\n'
        '🩸 ЗДОРОВЬЕ: '+ str(health) + '\n'
        '🛡 БРОНЯ: ' + str(armor) + '\n'
        '🧿 ВЫНОСЛИВОСТЬ: ' + str(stamina) + '\n'
        '⚜ СПОСОБНОСТЬ: ' + str(curYourPower) + '\n'
        '⚔ АТАКА: ' + str(lowDamage) + '-' + str(highDamage) + '\n'
        '⚡ ЛОВКОСТЬ: ' + str(agility) + '%\n'
        '🔆 ШАНС КРИТА: ' + str(critChance) + '%\n'
        '💥 МНОЖИТЕЛЬ КРИТА: x' + str(critMultiplier) + '\n\n'
        'Хотите изменить имя и характер или улучшить свои показатели?', reply_markup=keyboard)
        saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
        bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))


def characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame):
    """
    Функция игры "РПГ", которая обрабатывает решение об изменении персонажа и перенаправляет в соответствующие функции.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    clearGame -- переменная, используемая для подтверждения сброса прогресса
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Изменить имя':
        if gold >= 1000:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Назад']])
            msg = bot.send_message(m.chat.id, 'ВНИМАНИЕ! СМЕНА ИМЕНИ ПЕРСОНАЖА ОБОЙДЕТСЯ ВАМ В 1000 ЕДИНИЦ ЗОЛОТА!\n\n'
            'Введите новое имя персонажа:', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Смена имени требует 1000 золота. Попробуйте еще раз, когда накопите.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Изменить характер':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Агрессивный', 'Осторожный', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Выберите характер персонажа', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changeClass(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Выбрать Способность':
        msg = bot.send_message(m.chat.id, powers_list(m, characterLevel, keyboard), reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changePower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Прокачка':
        keyboard.add(*[types.KeyboardButton(name) for name in ['+5 здор-е (30)', '+25 здор-е (150)', '+50 здор-е (300)', '+100 здор-е (500)', '+1 крит (20)', '+1 лов-ть (50)', '+1 вынос-ть (100)',  'Назад']])
        msg = bot.send_message(m.chat.id, 'Выберите что хотите улучшить. Количество золотых монет на улучшение указано в скобках. Количество улучшений может быть ограничено.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Сбросить прогресс':
        keyboard.add(*[types.KeyboardButton(name) for name in ['НЕТ!!!!!!', 'НЕТ!', 'НЕТНЕТНЕТ', 'НЕ НАДО', 'да', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Вы действительно хотите начать новую игру? Это действие нельзя будет отменить...... Подумайте еще раз.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: reallyClearRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Статистика':
        msg = bot.send_message(m.chat.id, rpg_statistics(m, characterLevel), reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Рейтинг игроков':
        msg = bot.send_message(m.chat.id, 'Скоро будет...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Помощь':
        info = rpg_help()
        list = info.split("\n\n")
        string = ''
        for x in range(0, len(list)):
            string += list[x]
            string += '\n\n'
            if x % 7 == 0 and x != 0 or x == len(list)-1:
                bot.send_message(m.chat.id, string.decode('utf-8'))
                string = ''
        msg = bot.send_message(m.chat.id, 'Надеюсь, что теперь у вас не осталось вопросов.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Назад':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: characterMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))


def reallyClearRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame):
    """
    Функция игры "РПГ", которая уточняет, действительно ли вы хотите уничтожить свой прогресс в игре.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    clearGame -- переменная, используемая для подтверждения сброса прогресса
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'НЕТ!!!!!!' or m.text == u'НЕТ!' or m.text == u'НЕТНЕТНЕТ' or m.text == u'НЕ НАДО':
        bot.send_message(m.chat.id, 'Мудрое решение. Не так ли?')
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'да':
        if clearGame == 2:
            bot.send_message(m.chat.id, 'Класс, теперь у вас нет ничего, кроме статистики...')
            clear_RPGBD(m)
            startRPG(m)
        elif clearGame == 0:
            clearGame += 1
            msg = bot.send_message(m.chat.id, 'Я очень надеюсь, что ты промахнулся. Если нет, то подумай что ты делаешь, глупец! Даю тебе еще шанс передумать.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: reallyClearRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
        elif clearGame == 1:
            clearGame += 1
            msg = bot.send_message(m.chat.id, 'Последний шанс. Нажмешь сюда еще раз и пути назад не будет!!! Однако прошу заметить, что статистика не очищается с перерождениями.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: reallyClearRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))
    elif m.text == u'Назад':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: reallyClearRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, clearGame))


def changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая меняет персонажу имя.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    if m.text == u'Назад' and characterName != 'Имени нет':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Назад' and characterName == 'Имени нет':
        msg = bot.send_message(m.chat.id, 'Ты не сможешь вернуться, пока не придумаешь имя персонажу. Попробуй еще раз...')
        bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    else:
        debilTest = m.text
        if debilTest == None:
            msg = bot.send_message(m.chat.id, 'Пожалуйста, не надо так делать, хорошо? Попробуй еще раз...')
            bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif len((m.text).encode('utf-8')) <= 30:
            alreadyExists = 0
            connection = get_connection()
            cursor = connection.cursor()
            sql = "SELECT characterName FROM `rpg`"
            cursor.execute(sql)
            for element in cursor:
                if element['characterName'].lower() == m.text.lower():
                    alreadyExists = 1
            if alreadyExists == 0:
                if gold >= 1000:
                    gold -= 1000
                characterName = (m.text).encode('utf-8')
                bot.send_message(m.chat.id, 'Вы успешно сменили имя.')
                character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            else:
                msg = bot.send_message(m.chat.id, 'Это имя уже занято. Попробуй еще раз...')
                bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Введенное имя слишком длинное. Попробуйте короче')
            bot.register_next_step_handler(msg, lambda m: changeName(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def changeClass(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая меняет персонажу характер.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Назад':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Агрессивный':
        if characterClass == 'Агрессивный':
            msg = bot.send_message(m.chat.id, 'У вас уже выбран этот характер.')
            character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
        else:
            if characterClass == 'Осторожный':
                agility -= 5
            characterClass = 'Агрессивный'
            critChance += 10
            msg = bot.send_message(m.chat.id, 'Вы успешно сменили характер на АГРЕССИВНЫЙ и получили +10 к шансу крита')
            character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Осторожный':
        if characterClass == 'Осторожный':
            msg = bot.send_message(m.chat.id, 'У вас уже выбран этот характер.')
            character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
        else:
            if characterClass == 'Агрессивный':
                critChance -= 10
            characterClass = 'Осторожный'
            agility += 5
            msg = bot.send_message(m.chat.id, 'Вы успешно сменили характер на ОСТОРОЖНЫЙ и получили +5 к ловкости')
            character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changeClass(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, pow, cost):
    """
    Функция игры "РПГ", которая меняет текущую способность игрока.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    pow -- сила, которую нужно выбрать
    cost -- стоимость выбранной силы, которую нужно установить
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if characterLevel >= pow:
        curYourPower = pow
        curYourPowerStaminaCost = cost
        bot.send_message(m.chat.id, 'Вы успешно выбрали способность.', reply_markup=keyboard)
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    else:
        msg = bot.send_message(m.chat.id, 'Вы еще не достигли необходимого уровня.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changePower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def changePower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая позволяет выбрать способность игрока.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Назад':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '1':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 2)
    elif m.text == '2':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 4)
    elif m.text == '3':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 3, 6)
    elif m.text == '4':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 4, 3)
    elif m.text == '5':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 5, 2)
    elif m.text == '6':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 6, 6)
    elif m.text == '7':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 7, 1)
    elif m.text == '8':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 8, 4)
    elif m.text == '9':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 9, 2.5)
    elif m.text == '10':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 10, 5)
    elif m.text == '11':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 11, 9999999)
    elif m.text == '12':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 12, 3)
    elif m.text == '13':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 13, 3)
    elif m.text == '14':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 14, 3.5)
    elif m.text == '15':
        initPower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 15, 4)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: changePower(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая позволяет улучшить статистические показатели игрока.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Назад':
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'+5 здор-е (30)':
        if gold >= 30:
            health += 5
            gold -= 30
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+25 здор-е (150)':
        if gold >= 150:
            health += 25
            gold -= 150
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+50 здор-е (300)':
        if gold >= 300:
            health += 50
            gold -= 300
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+100 здор-е (500)':
        if gold >= 500:
            health += 100
            gold -= 500
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+1 крит (20)':
        if gold >= 20 and critUpgrades < 10:
            critChance += 1
            critUpgrades += 1
            gold -= 20
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold >= 20 and critUpgrades >= 10:
            msg = bot.send_message(m.chat.id, 'Вы больше не можете улучшать критический шанс за монеты.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold <= 20:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+1 лов-ть (50)':
        if gold >= 50 and agilityUpgrades < 10:
            agility += 1
            agilityUpgrades += 1
            gold -= 50
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold >= 50 and agilityUpgrades >= 10:
            msg = bot.send_message(m.chat.id, 'Вы больше не можете улучшать ловкость за монеты.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold <= 50:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'+1 вынос-ть (100)':
        if gold >= 100 and staminaUpgrades < 4:
            stamina += 1
            staminaUpgrades += 1
            gold -= 100
            msg = bot.send_message(m.chat.id, 'Улучшение успешно. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
            saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold >= 100 and staminaUpgrades >= 4:
            msg = bot.send_message(m.chat.id, 'Вы больше не можете улучшать выносливость за монеты.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        elif gold <= 100:
            msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: upgrades(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая перенаправляет в магазин, позволяя купить оружие или броню.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Купить оружие', 'Купить броню', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Хотите купить оружие или броню?', reply_markup=keyboard)
    saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    bot.register_next_step_handler(msg, lambda m: magazinMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def magazinMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая обрабатывает решение пользователя о выборе направления в магазине и перенаправляет в соответствующий.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Купить оружие':
        if currentWeapon > 15:
            msg = bot.send_message(m.chat.id, 'Вы скупили всё существующее в игре оружие. Подождите пока мы добавим новое.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: magazinMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Купить', 'Назад']])
            msg = bot.send_message(m.chat.id, nextWeapon(currentWeapon), reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: weaponUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Купить броню':
        if currentArmor > 15:
            msg = bot.send_message(m.chat.id, 'Вы скупили все существующие в игре брони. Подождите пока мы добавим новые.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: magazinMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Купить', 'Назад']])
            msg = bot.send_message(m.chat.id, nextArmor(currentArmor), reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: armorUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Назад':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: magazinMenu(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, low, high, critCh, critMul, weaponCost):
    """
    Функция игры "РПГ", которая осуществляет улучшение показателей от нового оружия.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    low -- значение, на которое изменится нижняя граница урона
    high -- значение, на которое изменится верхняя граница урона
    critCh -- значение, на которое изменится критический шанс
    critMul -- значение, на которое изменится множитель крита
    weaponCost -- цена текущего оружия
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if gold >= weaponCost:
        lowDamage += low
        highDamage += high
        critChance += critCh
        critMultiplier += critMul
        gold -= weaponCost
        currentWeapon += 1
        if currentWeapon == 16:
            rpg_weapons_achievement(m)
        msg = bot.send_message(m.chat.id, 'Покупка успешна! Вы обновили своё оружие. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
        magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    else:
        msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: weaponUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def weaponUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая позволяет купить новое оружие.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Купить':
        if currentWeapon == 1:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 0, 1, 0, 0, weaponCost1)
        elif currentWeapon == 2:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 1, 0, 0, weaponCost2)
        elif currentWeapon == 3:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 0, 1, 2, 0, weaponCost3)
        elif currentWeapon == 4:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 0, 1, 5, 0, weaponCost4)
        elif currentWeapon == 5:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 1, 0, 0, weaponCost5)
        elif currentWeapon == 6:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 2, 0, 0, weaponCost6)
        elif currentWeapon == 7:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 1, 0, 0, weaponCost7)
        elif currentWeapon == 8:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 2, 5, 0, weaponCost8)
        elif currentWeapon == 9:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 1, 0, 0, weaponCost9)
        elif currentWeapon == 10:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 2, 5, 0, weaponCost10)
        elif currentWeapon == 11:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 2, 0, 0, weaponCost11)
        elif currentWeapon == 12:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 3, 5, 0, weaponCost12)
        elif currentWeapon == 13:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 3, 3, 0, 0, weaponCost13)
        elif currentWeapon == 14:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 4, 4, 0, 0, weaponCost14)
        elif currentWeapon == 15:
            weaponUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 9, 5, 5, 0.3, weaponCost15)
        else:
            msg = bot.send_message(m.chat.id, 'Вы скупили всё существующее в игре оружие. Подождите пока мы добавим новое.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: weaponUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Назад':
        magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: weaponUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, arm, agil, armorCost):
    """
    Функция игры "РПГ", которая осуществляет улучшение показателей от новой брони.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    arm -- значение, на которое изменится показатель брони
    agil -- значение, на которое изменится показатель ловкости
    armorCost -- цена текущей брони
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if gold >= armorCost:
        armor += arm
        agility += agil
        gold -= armorCost
        currentArmor += 1
        if currentArmor == 16:
            rpg_armors_achievement(m)
        msg = bot.send_message(m.chat.id, 'Покупка успешна! Вы обновили свою броню. У вас осталось ' + str(gold) + ' золота.', reply_markup=keyboard)
        magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    else:
        msg = bot.send_message(m.chat.id, 'Недостаточно монет', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: armorUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def armorUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая позволяет купить новую броню.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Купить':
        if currentArmor == 1:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 0, armorCost1)
        elif currentArmor == 2:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 0, armorCost2)
        elif currentArmor == 3:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, -1, armorCost3)
        elif currentArmor == 4:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 0, armorCost4)
        elif currentArmor == 5:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 1, 0, armorCost5)
        elif currentArmor == 6:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, -2, armorCost6)
        elif currentArmor == 7:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 0, armorCost7)
        elif currentArmor == 8:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 2, 0, armorCost8)
        elif currentArmor == 9:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 3, -3, armorCost9)
        elif currentArmor == 10:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 3, 0, armorCost10)
        elif currentArmor == 11:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 3, 0, armorCost11)
        elif currentArmor == 12:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 4, -4, armorCost12)
        elif currentArmor == 13:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 5, 0, armorCost13)
        elif currentArmor == 14:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 7, 0, armorCost14)
        elif currentArmor == 15:
            armorUp(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, 8, -1, armorCost15)
        else:
            msg = bot.send_message(m.chat.id, 'Вы скупили все существующие в игре брони. Подождите пока мы добавим новые.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: armorUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Назад':
        magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: armorUpgrade(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def startQuest(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая позволяет перейти к списку противников.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    if characterName == 'Имени нет':
        bot.send_message(m.chat.id, 'Стоп. Вы собрались идти в бой безымянным героем? Так не пойдет. Сейчас я перенаправлю вас в меню, где вы сможете обозвать вашего персонажа и выбрать ему характер.')
        character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    else:
        if characterClass == 'Не выбран':
            bot.send_message(m.chat.id, 'Серьезно? Вы были в меню персонажа и дали ему имя, но не выбрали характер? Сейчас я перенаправлю вас в меню, где вы сможете это исправить.')
            character(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
        else:
            if currentWeapon == 1 and gold >= weaponCost1:
                bot.send_message(m.chat.id, 'Собираетесь драться с врагами в рукопашной? Лучше купить свой первый меч в лавке.')
                magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            elif currentArmor == 1 and gold >= armorCost1:
                bot.send_message(m.chat.id, 'Собираетесь драться с врагами в обычной тканевой одежде? Лучше купить броню в лавке.')
                magazin(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
            else:
                questsList(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)


def questsList(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая выводит список противников и позволяет сделать свой выбор.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    removeUseless = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `rpg` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        removeUseless = element['removeUseless']
    connection.commit()
    connection.close()
    info = enemies_list(m, removeUseless, characterLevel, keyboard)
    list = info.split("&&&")
    string = ''
    for x in range(0, len(list)):
        string += list[x]
        string += '\n\n'
        if x % 10 == 0 and x != 0 or x == len(list)-1:
            bot.send_message(m.chat.id, string.decode('utf-8'))
            string = ''
    keyboard.add(*[types.KeyboardButton(name) for name in ['Показать врагов', 'Убирать бесполезных', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Выбор за тобой', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: startBattle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, bat, xp, gol, hp, arm, ld, hd, agil, stam, pow, crit, mult):
    """
    Функция игры "РПГ", которая инициализирует показатели противника.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    bat -- номер противника, который устанавливается при инициализации
    xp -- количество получаемого опыта за бой, которое устанавливается при инициализации
    gol -- количество получаемого золота за бой, которое устанавливается при инициализации
    hp -- количество здоровья противника, которое устанавливается при инициализации
    arm -- количество брони противника, которое устанавливается при инициализации
    ld -- нижний предел урона противника, который устанавливается при инициализации
    hd -- верхний предел урона противника, который устанавливается при инициализации
    agil -- шанс ловкости противника, который устанавливается при инициализации
    stam -- количество выносливости противника, которое устанавливается при инициализации
    pow -- количество выносливости, требуемое для активации способности противника, которое устанавливается при инициализации
    crit -- шанс крита противника, который устанавливается при инициализации
    mult -- множитель крита противника, который устанавливается при инициализации
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if characterLevel >= bat:
        curBattle = bat
        if bat != 1 and characterLevel >= bat + 3:
            curGold = 0
            curExp = 0
        else:
            curGold = gol
            curExp = xp
        if characterLevel == 15:
            curExp = 0
        curEnemyHP = hp
        curEnemyArmor = arm
        curEnemyLowDamage = ld
        curEnemyHighDamage = hd
        curEnemyAgility = agil
        curEnemyStamina = stam
        curEnemyPowerStaminaCost = pow
        curEnemyCritChance = crit
        curEnemyCritMultiplier = mult
        lowBleedDamage = 0
        highBleedDamage = 0
        evasionIncreased = 0
        lastEvasion = 0
        bot.send_message(m.chat.id, 'Битва началась!')
        battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    else:
        msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: startBattle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def startBattle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая начинает бой.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    curYourHP = health
    curYourArmor = armor
    curYourStamina = stamina
    curYourLowDamage = lowDamage
    curYourHighDamage = highDamage
    curYourAgility = agility
    curYourCritChance = critChance
    curYourCritMultiplier = critMultiplier
    zombieCounter = -1
    bleedCounter = 0
    enemyStun = 0
    playerStun = 0
    curBattle = 0
    curExp = 0
    curGold = 0
    curEnemyHP = 0
    curEnemyArmor = 0
    curEnemyLowDamage = 0
    curEnemyHighDamage = 0
    curEnemyAgility = 0
    curEnemyStamina = 0
    curEnemyPowerStaminaCost = 0
    curEnemyCritChance = 0
    curEnemyCritMultiplier = 0
    removeUseless = 0
    random.seed(datetime.now())
    if m.text == '1':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 1, 1, 5, 10, 0, 1, 2, 0, 0, 999999999, 2, 1.5)
    elif m.text == '2':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 2, 2, 10, 15, 1, 1, 3, 5, 1, 3, 3, 1.5)
    elif m.text == '3':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 3, 3, 20, 25, 0, 3, 4, 1, 0, 3, 5, 2)
    elif m.text == '4':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 4, 5, 30, 50, 2, 5, 7, 7, 2, 6, 10, 2)
    elif m.text == '5':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 5, 7, 40, 75, 3, 8, 10, 5, 1, 5, 15, 2)
    elif m.text == '6':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 6, 10, 45, 100, 4, 11, 13, 10, 2, 5, 20, 2)
    elif m.text == '7':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 7, 15, 50, 200, 5, 15, 18, 5, 0, 999999999, 25, 1.5)
    elif m.text == '8':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 8, 20, 55, 300, 6, 20, 25, 15, 4, 4, 30, 1.5)
    elif m.text == '9':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 9, 25, 60, 400, 7, 28, 30, 5, 5, 6, 50, 1.5)
    elif m.text == '10':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 10, 30, 65, 500, 8, 31, 33, 15, 5, 5, 32, 1.5)
    elif m.text == '11':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 11, 35, 70, 600, 9, 34, 35, 0, 4, 6, 60, 1.5)
    elif m.text == '12':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 12, 40, 75, 700, 10, 38, 40, 5, 0, 999999999, 35, 1.5)
    elif m.text == '13':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 13, 45, 85, 800, 0, 41, 42, 55, 0, 999999999, 40, 1.5)
    elif m.text == '14':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 14, 50, 100, 1000, 15, 44, 45, 30, 9, 3, 45, 2)
    elif m.text == '15':
        enemyInit(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, 15, 0, 500, 5000, 30, 50, 50, 30, 0, 8, 90, 2)
    elif m.text == u'Показать врагов':
        keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Облик какого противника ты хочешь увидеть?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Убирать бесполезных':
        connection = get_connection()
        cursor = connection.cursor()
        sql = "SELECT * FROM `rpg` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            removeUseless = element['removeUseless']
        if removeUseless == 0:
            removeUseless = 1
            sql = "UPDATE `rpg` SET removeUseless = %s WHERE `id` = %s"
            cursor.execute(sql, (removeUseless, m.chat.id))
            bot.send_message(m.chat.id, 'Противники, которые не приносят опыта больше не будут показываться в списке.\n\n'
            'Нажми сюда еще раз, если захочешь вернуть их. Также ты все равно всегда можешь вызвать их, написав номер нужного противника вручную.', reply_markup=keyboard)
        else:
            removeUseless = 0
            sql = "UPDATE `rpg` SET removeUseless = %s WHERE `id` = %s"
            cursor.execute(sql, (removeUseless, m.chat.id))
            bot.send_message(m.chat.id, 'Противники, которые не приносят опыта вернулись в список.\n\nНажми сюда еще раз, если захочешь снова убрать их.', reply_markup=keyboard)
        connection.commit()
        connection.close()
        questsList(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == u'Назад':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: startBattle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая присылает картинку противника.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == '1':
        bot.send_photo(m.chat.id, photo=open('./enemy1.jpg', 'rb'))
        msg = bot.send_message(m.chat.id, 'Это был Злоумышленник. Показать еще кого-нибудь?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '2':
        if characterLevel >= 2:
            bot.send_photo(m.chat.id, photo=open('./enemy2.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Гоблин. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '3':
        if characterLevel >= 3:
            bot.send_photo(m.chat.id, photo=open('./enemy3.png', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Зомби. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '4':
        if characterLevel >= 4:
            bot.send_photo(m.chat.id, photo=open('./enemy4.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Оборотень. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '5':
        if characterLevel >= 5:
            bot.send_photo(m.chat.id, photo=open('./enemy5.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Минотавр. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '6':
        if characterLevel >= 6:
            bot.send_photo(m.chat.id, photo=open('./enemy6.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Василиск. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '7':
        if characterLevel >= 7:
            bot.send_photo(m.chat.id, photo=open('./enemy7.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Огненный Элементаль. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '8':
        if characterLevel >= 8:
            bot.send_photo(m.chat.id, photo=open('./enemy8.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Инсектоид. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '9':
        if characterLevel >= 9:
            bot.send_photo(m.chat.id, photo=open('./enemy9.png', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Многорукий. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '10':
        if characterLevel >= 10:
            bot.send_photo(m.chat.id, photo=open('./enemy10.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Скорпион. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '11':
        if characterLevel >= 11:
            bot.send_photo(m.chat.id, photo=open('./enemy11.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Двуглавый Гигант. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '12':
        if characterLevel >= 12:
            bot.send_photo(m.chat.id, photo=open('./enemy12.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Могильщик. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '13':
        if characterLevel >= 13:
            bot.send_photo(m.chat.id, photo=open('./enemy13.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Войд. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '14':
        if characterLevel >= 14:
            bot.send_photo(m.chat.id, photo=open('./enemy14.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Пожиратель Героев. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '15':
        if characterLevel >= 15:
            bot.send_photo(m.chat.id, photo=open('./enemy15.jpg', 'rb'))
            msg = bot.send_message(m.chat.id, 'Это был Лидер. Показать еще кого-нибудь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
        else:
            msg = bot.send_message(m.chat.id, 'Вы пока не доросли до этого противника', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == u'Назад':
        questsList(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: showEnemy(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", которая дает принять решение о действии на следующем ходе в бою.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    random.seed(datetime.now())
    string = ''
    if curYourHP > 0:
        if playerStun <= 0:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Атака', 'Способность', 'Пропуск хода', 'Показатели', 'Сдаться']])
            msg = bot.send_message(m.chat.id, 'Ваш ход', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: whatAction(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion))
        else:
            string += '🌀Вы оглушены и пропускаете ход.🌀\n\n'
            playerStun -= 1
            if zombieCounter > 0:
                zombieCounter -= 1
            if zombieCounter == 0:
                curYourHP = 0
            if bleedCounter > 0:
                damag = random.randint(lowBleedDamage, highBleedDamage)
                bleedCounter -= 1
                if curYourPower == 11:
                    damag = 0
                curYourHP -= damag
                if curYourHP <= 0:
                    curYourHP = 0
                string += '❗❗❗Вы получили '+ str(damag) + ' единиц урона от негативного эффекта.❗❗❗'
            bot.send_message(m.chat.id, string, reply_markup=keyboard)
            enemyTurn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    else:
        rpg_deaths_update(m)
        saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Увы, но вас убили...\n\nКуда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def whatAction(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", которая обрабатывает решение пользователя о выборе действия и перенаправляет в соответствующие функции.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Атака':
        attack(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    elif m.text == u'Способность':
        power(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    elif m.text == u'Пропуск хода':
        loseturn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    elif m.text == u'Показатели':
        battleStats(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    elif m.text == u'Сдаться':
        rpg_deaths_update(m)
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Обидно... Вы проиграли. Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: whatAction(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion))


def attack(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", совершающая атаку по противнику.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    string = ''
    random.seed(datetime.now())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if random.randint(1, 100) <= curEnemyAgility:
        string += '⚡ Противник уклонился⚡ \n\n'
        damag = 0
    else:
        damag = random.randint(curYourLowDamage, curYourHighDamage)
        if random.randint(1, 100) <= curYourCritChance:
            damag *= curYourCritMultiplier
            string += '💥Критическое попадание💥\n\n'
        damag -= curEnemyArmor
        curYourStamina += 0.5
        if damag <= 0:
            damag = 0
        curEnemyHP -= damag
    string += '🗡Нанесенный урон: ' + str(damag) + ' 🗡\n\n'
    if zombieCounter > 0:
        zombieCounter -= 1
    if zombieCounter == 0:
        curYourHP = 0
    if bleedCounter > 0:
        damag = random.randint(lowBleedDamage, highBleedDamage)
        bleedCounter -= 1
        if curYourPower == 11:
            damag = 0
        curYourHP -= damag
        if curYourHP <= 0:
            curYourHP = 0
        string += '❗❗❗Вы получили '+ str(damag) + ' единиц урона от негативного эффекта.❗❗❗'
    bot.send_message(m.chat.id, string, reply_markup=keyboard)
    rpg_attacks_update(m)
    enemyTurn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)


def power(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", производящая использование способности персонажа в бою.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    curStrike = 0
    random.seed(datetime.now())
    string = ''
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if curYourStamina >= curYourPowerStaminaCost and curYourPower != 11:
        connection = get_connection()
        cursor = connection.cursor()
        powerUse = 0
        power1use = 0
        power2use = 0
        power3use = 0
        power4use = 0
        power5use = 0
        power6use = 0
        power7use = 0
        power8use = 0
        power9use = 0
        power10use = 0
        power12use = 0
        power13use = 0
        power14use = 0
        power15use = 0
        sql = "SELECT * FROM `rpg_stat` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            powerUse = element['powerUse']
            power1use = element['power1use']
            power2use = element['power2use']
            power3use = element['power3use']
            power4use = element['power4use']
            power5use = element['power5use']
            power6use = element['power6use']
            power7use = element['power7use']
            power8use = element['power8use']
            power9use = element['power9use']
            power10use = element['power10use']
            power12use = element['power12use']
            power13use = element['power13use']
            power14use = element['power14use']
            power15use = element['power15use']
        powerUse += 1
        if curYourPower == 1:
            power1use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
                damag = 0
            else:
                damag = random.randint(curYourLowDamage + 1, curYourHighDamage + 1)
                if random.randint(1, 100) <= curYourCritChance:
                    damag *= curYourCritMultiplier
                    string += '💥Критическое попадание💥\n\n'
                damag -= curEnemyArmor
                if damag <= 0:
                    damag = 0
                curEnemyHP -= damag
            string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
        elif curYourPower == 2:
            power2use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
            else:
                enemyStun = 2
                string += '😴Враг пропустит 2 хода😴\n\n'
        elif curYourPower == 3:
            power3use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
            else:
                curEnemyStamina -= 3
                if curEnemyStamina <= 0:
                    curEnemyStamina = 0
                string += '🧿Вы заставили противника потерять 3 единицы выносливости🧿\n\n'
        elif curYourPower == 4:
            power4use += 1
            curYourStamina -= curYourPowerStaminaCost
            for curStrike in range(3):
                if random.randint(1, 100) <= curEnemyAgility:
                    string += '⚡ Противник уклонился⚡ \n\n'
                    damag = 0
                else:
                    damag = random.randint(curYourLowDamage, curYourHighDamage)
                    if random.randint(1, 100) <= curYourCritChance:
                        damag *= curYourCritMultiplier
                        string += '💥Критическое попадание💥\n\n'
                    damag -= curEnemyArmor
                    if damag <= 0:
                        damag = 0
                    curEnemyHP -= damag
                string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
        elif curYourPower == 5:
            power5use += 1
            curYourStamina -= curYourPowerStaminaCost
            damag = random.randint(curYourLowDamage * 2, curYourHighDamage * 2)
            damag *= curYourCritMultiplier
            string += '💥Критическое попадание💥\n\n'
            if damag <= 0:
                damag = 0
            curEnemyHP -= damag
            string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
        elif curYourPower == 6:
            power6use += 1
            curYourStamina -= curYourPowerStaminaCost
            curYourHP += 100
            if curYourHP > health:
                curYourHP = health
            string += '🏥Вы восстановили немного здоровья🏥\n\n'
        elif curYourPower == 7:
            power7use += 1
            curYourStamina -= curYourPowerStaminaCost
            curYourCritMultiplier += 0.1
            string += '🔱Вы немного увеличили множитель урона своего крита.🔱\n\n'
        elif curYourPower == 8:
            power8use += 1
            curYourStamina -= curYourPowerStaminaCost
            lastEvasion = curYourAgility
            curYourAgility = 100
            evasionIncreased = 1
            string += '🔰Вы увеличили шанс своего уклонения до предела на следующий ход.🔰\n\n'
        elif curYourPower == 9:
            power9use += 1
            curYourStamina -= curYourPowerStaminaCost
            damag = random.randint(curYourLowDamage * 3, curYourHighDamage * 3)
            damag *= curYourCritMultiplier
            string += '💥Критическое попадание💥\n\n'
            if damag <= 0:
                damag = 0
            curEnemyHP -= damag
            string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
        elif curYourPower == 10:
            power10use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
            else:
                curEnemyStamina -= 5
                if curEnemyStamina <= 0:
                    curEnemyStamina = 0
                string += '🧿Вы заставили противника потерять 5 единиц выносливости🧿\n\n'
        elif curYourPower == 12:
            power12use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
            else:
                enemyStun = 3
                string += '😴Враг пропустит 3 хода😴\n\n'
        elif curYourPower == 13:
            power13use += 1
            curYourStamina -= curYourPowerStaminaCost
            for curStrike in range(5):
                if random.randint(1, 100) <= curEnemyAgility:
                    string += '⚡ Противник уклонился⚡ \n\n'
                    damag = 0
                else:
                    damag = random.randint(curYourLowDamage, curYourHighDamage)
                    if random.randint(1, 100) <= curYourCritChance:
                        damag *= curYourCritMultiplier
                        string += '💥Критическое попадание💥\n\n'
                    damag -= curEnemyArmor
                    if damag <= 0:
                        damag = 0
                    curEnemyHP -= damag
                string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
        elif curYourPower == 14:
            power14use += 1
            curYourStamina -= curYourPowerStaminaCost
            curYourHP += 500
            if curYourHP > health:
                curYourHP = health
            string += '⚕⚕Вы значительно восстановили свое здоровье⚕⚕\n\n'
        elif curYourPower == 15:
            power15use += 1
            curYourStamina -= curYourPowerStaminaCost
            if random.randint(1, 100) <= curEnemyAgility:
                string += '⚡ Противник уклонился⚡ \n\n'
                damag = 0
            else:
                curEnemyStamina = 0
                string += '🧿Вы выкачали из противника всю его выносливость🧿\n\n'
        if curYourPower != 7:
            if zombieCounter > 0:
                zombieCounter -= 1
            if zombieCounter == 0:
                curYourHP = 0
            if bleedCounter > 0:
                damag = random.randint(lowBleedDamage, highBleedDamage)
                bleedCounter -= 1
                if curYourPower == 11:
                    damag = 0
                curYourHP -= damag
                if curYourHP <= 0:
                    curYourHP = 0
                string += '❗❗❗Вы получили '+ str(damag) + ' единиц урона от негативного эффекта.❗❗❗'
        bot.send_message(m.chat.id, string)
        sql = "UPDATE `rpg_stat` SET powerUse = %s, power1use = %s, power2use = %s, power3use = %s, power4use = %s, power5use = %s, power6use = %s, power7use = %s, power8use = %s, power9use = %s, power10use = %s, power12use = %s, power13use = %s, power14use = %s, power15use = %s WHERE `id` = %s"
        cursor.execute(sql, (powerUse, power1use, power2use, power3use, power4use, power5use, power6use, power7use, power8use, power9use, power10use, power12use, power13use, power14use, power15use, m.chat.id))
        achCount = 0
        RPG28 = 0
        sql = "SELECT * FROM `achievements` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            achCount = element['achCount']
            RPG28 = element['RPG28']
        if RPG28 == 0 and powerUse >= 1000:
            achCount += 1
            sql = "UPDATE `achievements` SET achCount = %s, RPG28 = %s WHERE `id` = %s"
            cursor.execute(sql, (achCount, 1, m.chat.id))
            bot.send_message(m.chat.id, '🏆Вы открыли достижение "Почувствуй мою мощь!". Подробнее во вкладке "Достижения" в главном меню.🏆', reply_markup=keyboard)
        connection.commit()
        connection.close()
        if curYourPower != 7:
            enemyTurn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
        else:
            battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    else:
        if curYourPower == 11:
            bot.send_message(m.chat.id, '❌Вы выбрали пассивную способность, ее нельзя применить самостоятельно.❌', reply_markup=keyboard)
        else:
            bot.send_message(m.chat.id, '🧿У вас недостаточно выносливости 🧿. Необходимо: ' + str(curYourPowerStaminaCost) + ', а у вас только:  ' + str(curYourStamina), reply_markup=keyboard)
        battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)


def loseturn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", позволяющая игроку пропустить ход.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    string = ''
    random.seed(datetime.now())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    curYourStamina += 1
    string += '💤Вы отдохнули и заработали больше выносливости💤\n\n'
    if zombieCounter > 0:
        zombieCounter -= 1
    if zombieCounter == 0:
        curYourHP = 0
    if bleedCounter > 0:
        damag = random.randint(lowBleedDamage, highBleedDamage)
        bleedCounter -= 1
        if curYourPower == 11:
            damag = 0
        curYourHP -= damag
        if curYourHP <= 0:
            curYourHP = 0
        string += '❗❗❗Вы получили '+ str(damag) + ' единиц урона от негативного эффекта.❗❗❗'
    bot.send_message(m.chat.id, string, reply_markup=keyboard)
    rpg_lost_turns_update(m)
    enemyTurn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)


def enemyTurn(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", которая совершает действие противника.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    string = ''
    random.seed(datetime.now())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if curEnemyHP > 0:
        string += 'Ход противника\n\n'
        if curBattle == 13:
            if random.randint(1, 100) <= 55:
                curEnemyHP += 30
                string += '⚕Войд восстановил немного здоровья⚕\n\n'
                if curEnemyHP > 800:
                    curEnemyHP = 800
        if enemyStun <= 0:
            if curEnemyStamina < curEnemyPowerStaminaCost:
                string += '⭐Противник использовал базовую атаку⭐\n\n'
                if random.randint(1, 100) <= curYourAgility:
                    string += '⚡ Вы уклонились⚡ \n\n'
                    damag = 0
                else:
                    damag = random.randint(curEnemyLowDamage, curEnemyHighDamage)
                    if random.randint(1, 100) <= curEnemyCritChance:
                        damag *= curEnemyCritMultiplier
                        string += '💥Критическое попадание💥\n\n'
                    damag -= curYourArmor
                    curEnemyStamina += 1
                    if damag <= 0:
                        damag = 0
                    curYourHP -= damag
                string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                if curBattle == 7:
                    bleedCounter = 9999999
                    lowBleedDamage = 1
                    highBleedDamage = 5
                    string += 'Гори!!!!!\n\n'
            else:
                string += '🌟🌟Противник использовал способность🌟🌟\n\n'
                curEnemyStamina -= curEnemyPowerStaminaCost
                if curBattle == 2:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                    else:
                        curYourStamina = 0
                        string += '🧿Противник обнулил вашу выносливость🧿\n\n'
                elif curBattle == 3:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                        damag = 0
                    else:
                        damag = 5
                        if random.randint(1, 100) <= curEnemyCritChance:
                            damag *= curEnemyCritMultiplier
                            string += '💥Критическое попадание💥\n\n'
                        damag -= curYourArmor
                        if zombieCounter == -1:
                            zombieCounter = 3
                            string += '\n🧟Вас заразили зомби-инфекцией. Через 3 хода вы превратитесь в зомби и проиграете бой🧟\n\n'
                        if damag <= 0:
                            damag = 0
                        curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                elif curBattle == 4:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                        damag = 0
                    else:
                        damag = random.randint(curEnemyLowDamage + 5, curEnemyHighDamage + 5)
                        if random.randint(1, 100) <= curEnemyCritChance:
                            damag *= curEnemyCritMultiplier
                            string += '💥Критическое попадание💥\n\n'
                        damag -= curYourArmor
                        bleedCounter = 3
                        lowBleedDamage = 1
                        highBleedDamage = 2
                        if damag <= 0:
                            damag = 0
                        curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                    if bleedCounter > 0:
                        string += '🩸Вы будете кровоточить 3 хода🩸\n\n'
                elif curBattle == 5:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                        damag = 0
                    else:
                        playerStun = 1
                        string += '😴Противник оглушил вас на ход😴\n\n'
                elif curBattle == 6:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                        damag = 0
                    else:
                        damag = 10
                        if random.randint(1, 100) <= curEnemyCritChance:
                            damag *= curEnemyCritMultiplier
                            string += '💥Критическое попадание💥\n\n'
                        damag -= curYourArmor
                        bleedCounter = 5
                        lowBleedDamage = 1
                        highBleedDamage = 3
                        if damag <= 0:
                            damag = 0
                        curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                    if bleedCounter > 0:
                        string += '🦠Вы будете отравлены 5 ходов🦠\n\n'
                elif curBattle == 8:
                    for curStrike in range(2):
                        if random.randint(1, 100) <= curYourAgility:
                            string += '⚡ Вы уклонились⚡ \n\n'
                            damag = 0
                        else:
                            damag = random.randint(curEnemyLowDamage, curEnemyHighDamage)
                            if random.randint(1, 100) <= curEnemyCritChance:
                                damag *= curEnemyCritMultiplier
                                string += '💥Критическое попадание💥\n\n'
                            damag -= curYourArmor
                            if damag <= 0:
                                damag = 0
                            curYourHP -= damag
                        string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                elif curBattle == 9:
                    for curStrike in range(4):
                        if random.randint(1, 100) <= curYourAgility:
                            string += '⚡ Вы уклонились⚡ \n\n'
                            damag = 0
                        else:
                            damag = random.randint(curEnemyLowDamage, curEnemyHighDamage)
                            if random.randint(1, 100) <= curEnemyCritChance:
                                damag *= curEnemyCritMultiplier
                                string += '💥Критическое попадание💥\n\n'
                            damag -= curYourArmor
                            if damag <= 0:
                                damag = 0
                            curYourHP -= damag
                        string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                elif curBattle == 10:
                    if random.randint(1, 100) <= curYourAgility:
                        string += '⚡ Вы уклонились⚡ \n\n'
                        damag = 0
                    else:
                        damag = 40
                        if random.randint(1, 100) <= curEnemyCritChance:
                            damag *= curEnemyCritMultiplier
                            string += '💥Критическое попадание💥\n\n'
                        damag -= curYourArmor
                        bleedCounter = 3
                        lowBleedDamage = 3
                        highBleedDamage = 5
                        if damag <= 0:
                            damag = 0
                        curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                    if bleedCounter > 0:
                        string += '🦠Вы будете отравлены 3 хода🦠\n\n'
                elif curBattle == 11:
                    damag = 40
                    damag *= curEnemyCritMultiplier
                    string += '💥Критическое попадание💥\n\n'
                    damag -= curYourArmor
                    if damag <= 0:
                        damag = 0
                    curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                elif curBattle == 14:
                    damag = 100
                    if random.randint(1, 100) <= curEnemyCritChance:
                        damag *= curEnemyCritMultiplier
                        string += '💥Критическое попадание💥\n\n'
                    damag -= curYourArmor
                    bleedCounter = 9999999
                    lowBleedDamage = 35
                    highBleedDamage = 35
                    if damag <= 0:
                        damag = 0
                    curYourHP -= damag
                    string += '🗡Нанесенный урон: ' + str(damag) + '🗡\n\n'
                    if bleedCounter > 0:
                        string += '🩸🩸Вы кровоточите до конца боя🩸🩸\n\n'
                elif curBattle == 15:
                    curYourHP = 0
                    string += '🤡🤡🤡Ваше здоровье обнулили🤡🤡🤡\n\n'
        else:
            enemyStun -= 1
            string += '🌀Враг пропускает ход...🌀\n\n'
        if evasionIncreased == 1:
            evasionIncreased = 0
            curYourAgility = lastEvasion
        if curYourPower == 14:
            if random.randint(1, 100) <= 30:
                curYourHP += 15
                string += '⚕Вы пассивно восстановили немного здоровья⚕\n\n'
                if curYourHP > health:
                    curYourHP = health
        bot.send_message(m.chat.id, string, reply_markup=keyboard)
        battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
    else:
        if curBattle != 12:
            win(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curBattle, curExp, curGold)
        else:
            if random.randint(1, 100) <= 25:
                curEnemyHP = 350
                bot.send_message(m.chat.id, '✝✝У Могильщика сработала его пассивная способность и возродила его с половиной максимального здоровья... ✝✝Ой-ой...')
                if curYourPower == 14:
                    if random.randint(1, 100) <= 30:
                        curYourHP += 15
                        string += '⚕Вы пассивно восстановили немного здоровья⚕\n\n'
                        if curYourHP > health:
                            curYourHP = health
                battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)
            else:
                win(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curBattle, curExp, curGold)


def win(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curBattle, curExp, curGold):
    """
    Функция игры "РПГ", которая отвечает за последствия победы в бою.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    experience += curExp
    gold += curGold
    bot.send_message(m.chat.id, '✨✨✨Ура, вы одолели противника!✨✨✨ \n\n+' + str(curGold) + ' золота 💰 и +' + str(curExp) + ' опыта 📖')
    if gold >= 5000:
        rpg_gold_achievement(m)
    enemy_kills_update(m, curGold, curBattle)
    saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    if experience < experienceTarget or characterLevel == 15:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))
    else:
        levelup(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)


def battleStats(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion):
    """
    Функция игры "РПГ", которая показывает в бою состояния игрока и противника.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    curYourHP -- текущее здоровье персонажа в бою
    curYourArmor -- текущая броня персонажа в бою
    curYourStamina -- текущая выносливость персонажа в бою
    curYourLowDamage -- текущий нижний предел урона персонажа в бою
    curYourHighDamage -- текущий верхний предел урона персонажа в бою
    curYourAgility -- текущая выносливость персонажа
    curYourCritChance -- текущий критический шанс персонажа
    curYourCritMultiplier -- текущий множитель крита персонажа
    zombieCounter -- переменная для способности противника "зомби"
    bleedCounter -- счетчик ходов негативных эффектов на персонаже
    enemyStun -- счетчик пропускаемых противником ходов
    playerStun -- счетчик пропускаемых персонажем ходов
    curBattle -- текущий противник
    curExp -- текущий опыт за бой
    curGold -- текущее золото за бой
    curEnemyHP -- текущее здоровье противника
    curEnemyArmor -- текущая броня противника
    curEnemyLowDamage -- текущий нижний предел урона противника
    curEnemyHighDamage -- текущий верхний предел урона противника
    curEnemyAgility -- текущая ловкость противника
    curEnemyStamina -- текущая выносливость противника
    curEnemyPowerStaminaCost -- количество выносливости, требуемое на активацию способности противника
    curEnemyCritChance -- текущий критический шанс противника
    curEnemyCritMultiplier -- текущий множитель крита противника
    lowBleedDamage -- нижний предел урона негативных эффектов
    highBleedDamage -- верхний предел урона негативных эффектов
    evasionIncreased -- увеличена ли ловкость персонажа
    lastEvasion -- ловкость персонажа до ее увеличения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    string = ''
    string += '🦹Состояние противника:\n\n🩸Здоровье: ' + str(curEnemyHP)
    string += '\n🛡Броня: ' + str(curEnemyArmor)
    if curBattle != 1 and curBattle != 7 and curBattle != 12 and curBattle != 13:
        string += '\n🧿Выносливость: ' + str(curEnemyStamina)
        string += '\n⚜Стоимость силы: ' + str(curEnemyPowerStaminaCost)
    string += '\n⚔Урон: ' + str(curEnemyLowDamage) + '-' + str(curEnemyHighDamage)
    string += '\n⚡Ловкость: ' + str(curEnemyAgility)
    string += '%\n🔆Шанс Крита: ' + str(curEnemyCritChance)
    string += '%\n💥Множитель Крита: x' + str(curEnemyCritMultiplier)
    if enemyStun != 0:
        string += '\n💤Враг пропустит еще ' + str(enemyStun) + ' ход(а/ов)'
    string += '\n\n\n🦸Ваше состояние:\n\n🩸Здоровье: ' + str(curYourHP)
    string += '\n🛡Броня: ' + str(curYourArmor)
    if curYourPower != 11:
        string += '\n🧿Выносливость: ' + str(curYourStamina)
        string += '\n⚜Стоимость силы: ' + str(curYourPowerStaminaCost)
    string += '\n⚔Урон: ' + str(curYourLowDamage) + '-' + str(curYourHighDamage)
    string += '\n⚡Ловкость: ' + str(curYourAgility)
    string += '%\n🔆Шанс Крита: ' + str(curYourCritChance)
    string += '%\n💥Множитель Крита: x' + str(curYourCritMultiplier)
    if zombieCounter != -1:
        string += '\n🧟Ходов до превращения в зомби: ' + str(zombieCounter)
    if bleedCounter != 0:
        string += '\n❗❗Негативный эффект, наносящий урон ' + str(lowBleedDamage) + '-' + str(highBleedDamage) + ' будет активен еще ' + str(bleedCounter) + ' ход(а/ов)❗❗'
    bot.send_message(m.chat.id, string, reply_markup=keyboard)
    battle(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost, curYourHP, curYourArmor, curYourStamina, curYourLowDamage, curYourHighDamage, curYourAgility, curYourCritChance, curYourCritMultiplier, zombieCounter, bleedCounter, enemyStun, playerStun, curBattle, curExp, curGold, curEnemyHP, curEnemyArmor, curEnemyLowDamage, curEnemyHighDamage, curEnemyAgility, curEnemyStamina, curEnemyPowerStaminaCost, curEnemyCritChance, curEnemyCritMultiplier, lowBleedDamage, highBleedDamage, evasionIncreased, lastEvasion)


def levelup(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost):
    """
    Функция игры "РПГ", которая увеличивает уровень игрока в случае, если после боя набрано нужное количество очков опыта.

    m -- сообщение пользователя
    characterName -- имя персонажа
    characterClass -- характер персонажа
    characterLevel -- уровень персонажа
    health -- максимальное здоровье персонажа
    armor -- броня персонажа
    stamina -- выносливость персонажа
    lowDamage -- нижний предел урона
    highDamage -- верхний предел урона
    agility -- ловкость персонажа
    critChance -- шанс критического удара
    critMultiplier -- множитель критического удара
    gold -- накопленное золото
    experience -- накопленный опыт
    experienceTarget -- граница опыта, достижение/пересечение которой дает повышение уровня
    critUpgrades -- количество успешных улучшений критического шанса
    agilityUpgrades -- количество успешных улучшений ловкости
    staminaUpgrades -- количество успешных улучшений выносливости
    currentWeapon -- последнее купленное оружие
    currentArmor -- последняя купленная броня
    curYourPower -- выбранная в данный момент способность
    curYourPowerStaminaCost -- количество выносливости, требуемое на активацию выбранной способности
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    string = '⬆⬆Поздравляем! Уровень повышен!⬆⬆\n\n'
    if characterLevel == 1:
        characterLevel += 1
        experienceTarget = 10
        health += 1
        critChance += 1
        agility += 1
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 2:
        characterLevel += 1
        experienceTarget = 25
        health += 4
        critChance += 2
        agility += 2
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 3:
        characterLevel += 1
        experienceTarget = 50
        health += 5
        critChance += 3
        agility += 3
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 4:
        characterLevel += 1
        experienceTarget = 100
        health += 10
        critChance += 5
        agility += 5
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 5:
        characterLevel += 1
        experienceTarget = 200
        health += 15
        agility += 2
        string += 'Возросли показатели здоровья и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 6:
        characterLevel += 1
        experienceTarget = 350
        health += 30
        critChance += 2
        string += 'Возросли показатели здоровья и критического шанса.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 7:
        characterLevel += 1
        experienceTarget = 500
        health += 50
        agility += 3
        string += 'Возросли показатели здоровья и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 8:
        characterLevel += 1
        experienceTarget = 750
        health += 50
        critChance += 3
        string += 'Возросли показатели здоровья и критического шанса.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 9:
        characterLevel += 1
        experienceTarget = 1000
        health += 100
        critChance += 3
        critMultiplier += 0.5
        stamina += 1
        string += 'Возросли показатели здоровья, критического шанса, критического множителя и выносливости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 10:
        characterLevel += 1
        experienceTarget = 1500
        health += 100
        critChance += 2
        agility += 2
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 11:
        characterLevel += 1
        experienceTarget = 2000
        health += 100
        critChance += 3
        agility += 3
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 12:
        characterLevel += 1
        experienceTarget = 3500
        health += 100
        critChance += 4
        agility += 4
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 13:
        characterLevel += 1
        experienceTarget = 5000
        health += 200
        critChance += 5
        agility += 5
        string += 'Возросли показатели здоровья, критического шанса и ловкости.\n\nДоступны новый противник и новая способность!'
    elif characterLevel == 14:
        characterLevel += 1
        experience = 5000
        health += 300
        critChance += 5
        agility += 5
        critMultiplier += 0.2
        stamina += 1
        string += 'Возросли показатели здоровья, критического шанса, ловкости, критического множителя и выносливости.\n\nВы достигли максимального уровня, поздравляем!'
        string += '\n\nДоступен последний самый главный противник, который вам, скорее всего, еще долго будет не по зубам. Но в сражении с ним вам может помочь новая способность...'
        rpg_level_achievement(m)
    elif characterLevel == 15:
        string += 'Вы достигли максимального уровня и опыт вам больше не нужен.'
    bot.send_message(m.chat.id, string)
    saveRPG(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Задания', 'Лавка', 'Персонаж', 'Назад']])
    msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: rpgMainMenuChoose(m, characterName, characterClass, characterLevel, health, armor, stamina, lowDamage, highDamage, agility, critChance, critMultiplier, gold, experience, experienceTarget, critUpgrades, agilityUpgrades, staminaUpgrades, currentWeapon, currentArmor, curYourPower, curYourPowerStaminaCost))


def startTXT(m):
    """
    Функция игры "Текстовый Квест", которая начинает квест, выводя первое сообщение и первые варианты действий.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    lowDoorTries = 0
    haveAknife = 0
    haveAflashlight = 0
    speedRun = 0
    keyboard.add(*[types.KeyboardButton(name) for name in ['Идти вперед', 'Идти назад', 'Идти влево', 'Идти вправо', 'Никуда не идти']])
    msg = bot.send_message(m.chat.id, 'Вы очнулись в кромешной темноте, не зная где находитесь. Последнее, что вы помните это сильный удар по голове. Его последствия'
    ' до сих пор тревожат вас неприятным покалыванием в затылке. Было бы здорово видеть хоть что-то, но ни зажигалки, ни фонарика в кармане случайно не оказалось.'
    ' Вам нужно решить в какую сторону идти, ведь нельзя же просто сидеть и ждать непонятно чего.', reply_markup=keyboard)
    bot.register_next_step_handler(msg, lambda m: txtChoise1(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))


def txtTryAgain(m):
    """
    Функция игры "Текстовый Квест", которая спрашивает желает ли пользователь начать игру заново в случае поражения.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    deaths = txt_deaths_update(m)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Да', 'Нет']])
    bot.send_message(m.chat.id, '💀Вы погибли ' + str(deaths) + ' раз(а)...💀', reply_markup=keyboard)
    msg = bot.send_message(m.chat.id, '👼Хотите попробовать с начала?👼', reply_markup=keyboard)
    bot.register_next_step_handler(msg, txtRevive)


def txtRevive(m):
    """
    Функция игры "Текстовый Квест", которая обрабатывает решение пользователя о рестарте игры.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Да':
        startTXT(m)
    elif m.text == u'Нет' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtRevive)


def txtChoise1(m, lowDoorTries, haveAknife, haveAflashlight, speedRun):
    """
    Первый набор событий игры "Текстовый Квест", отвечающий за последствия выбора направления после начала игры.

    m -- сообщение пользователя
    lowDoorTries -- количество попыток открывания нижней дверцы шкафчика
    haveAknife -- наличие ножа из шкафчика
    haveAflashlight -- наличие фонарика из шкафчика
    speedRun -- переменная для достижения за кратчайшее прохождение
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Идти вперед':
        bot.send_message(m.chat.id, 'Пройдя вперед вы случайно наступили на какую-то платформу и из стены в вас вылетело с десяток ядовитых дротиков...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти назад':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Идти назад', 'Идти влево', 'Идти вправо', 'Никуда не идти']])
        msg = bot.send_message(m.chat.id, 'Неспешно отойдя назад, вы развернулись и наткнулись на стену. Это значит, что теперь у вас только три варианта куда идти дальше. Но во всяком случае вы '
        'еще живы. Разве это не радует?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise2(m, lowDoorTries, haveAknife, haveAflashlight))
    elif m.text == u'Идти влево':
        bot.send_message(m.chat.id, 'Пройдя влево, вы задели леску и активировали механизм, который спустил курок дробовика, который был направлен прямо вам в голову...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти вправо':
        speedRun += 1
        keyboard.add(*[types.KeyboardButton(name) for name in ['Верхняя дверца', 'Средняя дверца', 'Нижняя дверца', 'Оставить шкафчик']])
        msg = bot.send_message(m.chat.id, 'Вы прошли вправо и вписались в какой-то шкафчик. Наощупь вы поняли, что у него три дверки. Рискнете ли вы открыть хоть одну?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
    elif m.text == u'Никуда не идти':
        bot.send_message(m.chat.id, 'Вы остались на месте и через три часа наконец дождались своего похитителя. Он отвел вас в какую-то лабораторию, где тестировал на вас '
        'различные свои сыворотки, пока вы не погибли...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise1(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))


def txtChoise2(m, lowDoorTries, haveAknife, haveAflashlight):
    """
    Второй набор событий игры "Текстовый Квест", отвечающий за последствия выбора направления при походе назад в первом наборе событий.

    m -- сообщение пользователя
    lowDoorTries -- количество попыток открывания нижней дверцы шкафчика
    haveAknife -- наличие ножа из шкафчика
    haveAflashlight -- наличие фонарика из шкафчика
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Идти назад':
        bot.send_message(m.chat.id, 'Пройдя назад вы случайно наступили на какую-то платформу и из стены в вас вылетело с десяток ядовитых дротиков...🎯', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти влево':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Идти влево', 'Идти назад', 'Никуда не идти']])
        msg = bot.send_message(m.chat.id, 'Вы прошли влево и... опять стена. Вы в углу и у вас только два пути.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise4(m, lowDoorTries, haveAknife, haveAflashlight))
    elif m.text == u'Идти вправо':
        bot.send_message(m.chat.id, 'Пройдя вправо, вы заметили, что пол, на который вы наступили, обваливается, но было уже поздно и теперь вы насажены на огромные колья...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Никуда не идти':
        bot.send_message(m.chat.id, 'Вы остались на месте и через три часа наконец дождались своего похитителя. Он отвел вас в какую-то лабораторию, где тестировал на вас '
        'различные свои сыворотки, пока вы не погибли...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise2(m, lowDoorTries, haveAknife, haveAflashlight))


def txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun):
    """
    Третий набор событий игры "Текстовый Квест", отвечающий за последствия выбора дверцы шкафчика.

    m -- сообщение пользователя
    lowDoorTries -- количество попыток открывания нижней дверцы шкафчика
    haveAknife -- наличие ножа из шкафчика
    haveAflashlight -- наличие фонарика из шкафчика
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    kidnapperIsDead = 0
    haveAkey = 0
    if m.text == u'Верхняя дверца':
        bot.send_message(m.chat.id, 'Вы открыли верхнюю дверцу шкафчика, но услышали странный шелчок. 💥Бум...💥 Кажется вы что-то взорвали. Видимо себя...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Средняя дверца':
        speedRun += 1
        if haveAflashlight == 0:
            haveAflashlight = 1
            msg = bot.send_message(m.chat.id, 'Вы открыли среднюю дверку шкафчика и нашли фонарик 🔦. Отлично. Теперь вы сможете нормально осмотреться.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
        else:
            speedRun = 9999
            msg = bot.send_message(m.chat.id, 'Вы уже открывали эту дверцу. Больше тут ничего нет, честно!', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
    elif m.text == u'Нижняя дверца':
        speedRun += 1
        if lowDoorTries < 3:
            lowDoorTries += 1
            msg = bot.send_message(m.chat.id, 'Вы попытались открыть нижнюю дверцу, но она не поддается 🔒. Может надо посильнее?🤔', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
        elif lowDoorTries == 3:
            lowDoorTries += 1
            haveAknife = 1
            msg = bot.send_message(m.chat.id, 'Ого, вы все-таки открыли дверцу. В выдвинутом ящичке вы нашли нож 🔪. Будем надеяться, что он не пригодится...', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
        elif lowDoorTries > 3:
            speedRun = 9999
            msg = bot.send_message(m.chat.id, 'Эта дверца уже открыта, там больше ничего нет, не сомневайтесь.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
    elif m.text == u'Оставить шкафчик':
        if haveAflashlight == 0:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Идти влево', 'Идти вправо', 'Идти вперед', 'Никуда не идти', 'Вернуться к шкафчику']])
            msg = bot.send_message(m.chat.id, 'Вы оставили шкафчик в покое и решили пойти дальше. Куда теперь?', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise5(m, lowDoorTries, haveAknife, haveAflashlight))
        else:
            speedRun += 1
            keyboard.add(*[types.KeyboardButton(name) for name in ['Пойти на свет', 'Пойти наверх', 'Осмотреть двери']])
            msg = bot.send_message(m.chat.id, 'Поскольку вы нашли в шкафчике фонарик, то теперь вы можете осмотреться, что вы и сделали. На ваше счастье, в фонарике были '
            'батарейки и беспокоиться на этот счет не пришлось. Включив его, вы заметили в другом конце комнаты открытую дверь. Обойдя все ловушки, вы вошли в проход и '
            'оказались в каком-то коридоре. Слева он протянулся очень далеко, но в самом конце виднеется какой-то еле заметный свет. Справа коридор почти сразу заканчивается, '
            'но там видно две двери. Также вы заметили закрученные ступеньки наверх. Неизвестно сколько еще этажей в этом странном месте...', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise6(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))


def txtChoise4(m, lowDoorTries, haveAknife, haveAflashlight):
    """
    Четвертый набор событий игры "Текстовый Квест", отвечающий за последствия выбора направления при походе влево во втором наборе событий.

    m -- сообщение пользователя
    lowDoorTries -- количество попыток открывания нижней дверцы шкафчика
    haveAknife -- наличие ножа из шкафчика
    haveAflashlight -- наличие фонарика из шкафчика
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Идти назад':
        bot.send_message(m.chat.id, 'Вы пошли в направлении, откуда пришли изначально, но зайдя чуть дальше, обнаружили дверь. Почуяв возможную свободу, вы как можно скорее '
        'дернули ручку и на удивление дверь легко открылась. Правда, задействовала механизм, который опустил топор, нанесший вам несовместимые с жизнью повреждения...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти влево':
        speedRun = 9999
        keyboard.add(*[types.KeyboardButton(name) for name in ['Верхняя дверца', 'Средняя дверца', 'Нижняя дверца', 'Оставить шкафчик']])
        msg = bot.send_message(m.chat.id, 'Вы прошли влево и вписались в какой-то шкафчик. Наощупь вы поняли, что у него три дверки. Рискнете ли вы открыть хоть одну?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
    elif m.text == u'Никуда не идти':
        bot.send_message(m.chat.id, 'Вы остались на месте и через три часа наконец дождались своего похитителя. Он отвел вас в какую-то лабораторию, где тестировал на вас '
        'различные свои сыворотки, пока вы не погибли...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise4(m, lowDoorTries, haveAknife, haveAflashlight))


def txtChoise5(m, lowDoorTries, haveAknife, haveAflashlight):
    """
    Пятый набор событий игры "Текстовый Квест" с негативными последствиями выбора направления в случае, если шкафчик не был обыскан достаточно хорошо.

    m -- сообщение пользователя
    lowDoorTries -- количество попыток открывания нижней дверцы шкафчика
    haveAknife -- наличие ножа из шкафчика
    haveAflashlight -- наличие фонарика из шкафчика
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Идти влево':
        bot.send_message(m.chat.id, 'Пройдя влево, вы вернулись в тот угол, но в этот раз зашли дальше и случайно наступили на какую-то платформу. '
        'Ничем хорошим это не кончилось, на вас упал потолок.', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти вправо':
        bot.send_message(m.chat.id, 'Пройдя вправо, вы дошли до стены, но когда коснулись ее, то из нее вылезли огромные стальные шипы и покончили с вашими похождениями.', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Идти вперед':
        bot.send_message(m.chat.id, 'Пройдя вперед, вы заметили, что пол, на который вы наступили, обваливается, но было уже поздно и теперь вы насажены на огромные колья...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Никуда не идти':
        bot.send_message(m.chat.id, 'Вы остались на месте и через три часа наконец дождались своего похитителя. Он отвел вас в какую-то лабораторию, где тестировал на вас '
        'различные свои сыворотки, пока вы не погибли...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Вернуться к шкафчику':
        speedRun = 9999
        keyboard.add(*[types.KeyboardButton(name) for name in ['Верхняя дверца', 'Средняя дверца', 'Нижняя дверца', 'Оставить шкафчик']])
        msg = bot.send_message(m.chat.id, 'Ну что ж, шкафчик никуда не убежал.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise3(m, lowDoorTries, haveAknife, haveAflashlight, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise5(m, lowDoorTries, haveAknife, haveAflashlight))


def txtChoise6(m, haveAknife, kidnapperIsDead, haveAkey, speedRun):
    """
    Шестой набор событий игры "Текстовый Квест", отвечающий за последствия выбора направления после выхода из первой комнаты.

    m -- сообщение пользователя
    haveAknife -- наличие ножа из шкафчика
    kidnapperIsDead -- убил ли пользователь похитителя ножом
    haveAkey -- наличие ключа из лаборатории
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Пойти на свет':
        if kidnapperIsDead == 0:
            if haveAknife == 0:
                bot.send_message(m.chat.id, 'Вы решили пойти на свет и пришли в какую-то странную лабораторию. К сожалению вы здесь оказались не одни. На вас бросился, по всей '
                'видимости, ваш похититель и вернул вас туда, откуда вы пришли, только в этот раз уже как следует связал. Через три часа он вернулся и привел вас в ту же '
                'лабораторию, где ставил над вами опыты, пока вы не погибли.', reply_markup=keyboard)
                txtTryAgain(m)
            else:
                speedRun += 1
                kidnapperIsDead = 1
                keyboard.add(*[types.KeyboardButton(name) for name in ['Осмотреть лабораторию', 'Вернуться обратно']])
                msg = bot.send_message(m.chat.id, 'Вы решили пойти на свет и пришли в какую-то странную лабораторию. К сожалению вы здесь оказались не одни. На вас бросился, по всей '
                'видимости, ваш похититель, но вы вспомнили, что нашли в шкафчике нож 🔪 и применили его для самозащиты, лишив противника жизни. Одной проблемой меньше, но вам все еще '
                'нужно найти выход. Расслабляться не стоит. Что вы решите делать дальше?', reply_markup=keyboard)
                bot.register_next_step_handler(msg, lambda m: txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
        else:
            speedRun = 9999
            keyboard.add(*[types.KeyboardButton(name) for name in ['Осмотреть лабораторию', 'Вернуться обратно']])
            msg = bot.send_message(m.chat.id, 'Вы здесь уже были, но никто не мешает походить еще.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey))
    elif m.text == u'Пойти наверх':
        bot.send_message(m.chat.id, 'Вы пошли по ступенькам вверх, но они все не заканчивались и не заканчивались. Наверху вас ждал тупик, поскольку дверь была заколочена. '
        'Спускаясь обратно, вы случайно запнулись и покатились кубарем, сломав себе шею и закончив приключение.', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Осмотреть двери':
        speedRun += 1
        keyboard.add(*[types.KeyboardButton(name) for name in ['Открыть левую', 'Открыть правую', 'Не трогать их']])
        msg = bot.send_message(m.chat.id, 'Двери абсолютно одинаковые. Вы решили приложить ухо к левой 🚪, чтобы услышать, есть ли что за ней. Никаких звуков. Так же и с правой 🚪. '
        'Стоит ли вообще их открывать?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise8(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise6(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))


def txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey, speedRun):
    """
    Седьмой набор событий игры "Текстовый Квест", отвечающий за последствия выбора в лаборатории.

    m -- сообщение пользователя
    haveAknife -- наличие ножа из шкафчика
    kidnapperIsDead -- убил ли пользователь похитителя ножом
    haveAkey -- наличие ключа из лаборатории
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Осмотреть лабораторию':
        speedRun += 1
        if haveAkey == 0:
            haveAkey = 1
            msg = bot.send_message(m.chat.id, 'Начав осматривать лабораторию, вы сразу же обратили внимание на какой-то ключ 🔑. Возможно он поможет вам выбраться.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
        else:
            speedRun = 9999
            msg = bot.send_message(m.chat.id, 'Повторный осмотр лаборатории не дал вам никаких полезных зацепок. Все, что здесь есть - это странные колбы, устройства и т.п.🔬🧪🧫⚗', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
    elif m.text == u'Вернуться обратно':
        speedRun += 1
        keyboard.add(*[types.KeyboardButton(name) for name in ['Пойти на свет', 'Пойти наверх', 'Осмотреть двери']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise6(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise7(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))


def txtChoise8(m, haveAknife, kidnapperIsDead, haveAkey, speedRun):
    """
    Восьмой набор событий игры "Текстовый Квест", отвечающий за последствия выбора двери в коридоре.

    m -- сообщение пользователя
    haveAknife -- наличие ножа из шкафчика
    kidnapperIsDead -- убил ли пользователь похитителя ножом
    haveAkey -- наличие ключа из лаборатории
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Открыть левую':
        if haveAkey == 0:
            speedRun = 9999
            msg = bot.send_message(m.chat.id, 'Вы попытались открыть дверь, но она закрыта 🔒. Выбить точно не получится.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise8(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
        else:
            speedRun += 1
            keyboard.add(*[types.KeyboardButton(name) for name in ['Проверить', 'Убежать']])
            msg = bot.send_message(m.chat.id, 'Вы попробовали открыть дверь с помощью найденного в лаборатории ключа 🔐. Успех 🔓! Открыв ее, вы пошли по длинному темному коридору, '
            'но в конце концов услышали шум улицы, разговоры. Это выход! ... Или нет? Кажется, это была слуховая галюцинация... Стоп, опять. Но сейчас уже больше похоже на стрельбу и взрывы. '
            'Вы можете пойти и проверить что это или пойти подальше оттуда. Здесь как раз развилка.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: txtChoise9(m, speedRun))
    elif m.text == u'Открыть правую':
        bot.send_message(m.chat.id, 'Дверь легко открылась. Вы вошли в нее, но радоваться было нечему. Вы разбудили трех бойцовских собак, которые не оставили от вас ничего...🐕🐕🐕', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Не трогать их':
        speedRun = 9999
        keyboard.add(*[types.KeyboardButton(name) for name in ['Пойти на свет', 'Пойти наверх', 'Осмотреть двери']])
        msg = bot.send_message(m.chat.id, 'Куда дальше?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise6(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise8(m, haveAknife, kidnapperIsDead, haveAkey, speedRun))


def txtChoise9(m, speedRun):
    """
    Девятый набор событий игры "Текстовый Квест", отвечающий за последствия принятия решения о выборе пути после открытия двери ключом.

    m -- сообщение пользователя
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Проверить':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Подкрасться с ножом', 'Прокрасться на кухню']])
        msg = bot.send_message(m.chat.id, 'Вы смело пошли прямо на звуки и увидели дверной проем. Дверь была открыта. Вы осторожно заглянули одним глазком в комнату и '
        'увидели как какой-то громила смотрел телевизор 📺 и недовольно переключал каналы. Вот что это были за разнообразные звуки... Хм, так может быть вы не похитителя убили? '
        'Хотя какая разница. Все здесь представляют для вас угрозу. Возможно стоит как-то разобраться с этим увальнем. Он сидит спиной к проходу. У вас есть шанс его зарезать...🔪 '
        'С другой стороны, вы можете прокрасться мимо него на... что-то вроде кухни... и поискать что-нибудь там.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise10)
    elif m.text == u'Убежать':
        speedRun += 1
        keyboard.add(*[types.KeyboardButton(name) for name in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50']])
        msg = bot.send_message(m.chat.id, 'Вы со всех ног помчались подальше от тех звуков и пришли к какой-то огромной металлической двери с орнаментом. Больше всего '
        'похоже на выход. Но вот беда. На двери замок... Рядом на стене висит что-то вроде почтовых ящиков. Их здесь 50 штук. Может быть ключ там?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise11(m, speedRun))
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: txtChoise9(m, speedRun))


def txtChoise10(m):
    """
    Десятый набор событий игры "Текстовый Квест", отвечающий за последствия принятия решения в комнате с громилой.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Подкрасться с ножом':
        bot.send_message(m.chat.id, 'Вы аккуратно подкрались к громиле и воткнули нож прямо ему в голову. Он погиб, но перед этим так завопил, что на этот крик из кухни вышел тощий '
        'парень и выпустил в вас все содержимое своего дробовика.', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Прокрасться на кухню':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Осторожно забрать', 'Сразу же стрелять', 'Не трогать его']])
        msg = bot.send_message(m.chat.id, 'Вы осторожно прокрались на кухню, вас никто не заметил. Но там вы увидели какого-то тощего парня, который заснул с дробовиком '
        'в руках. Хм, да с такой штуковиной вам здесь больше не придется никого бояться...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise12)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise10)


def txtChoise11(m, speedRun):
    """
    Одиннадцатый набор событий игры "Текстовый Квест", отвечающий за последствия выбора ящичка.

    m -- сообщение пользователя
    speedRun -- переменная для кратчайшего прохождения
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == '1':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '2':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '3':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '4':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '5':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '6':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '7':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '8':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '9':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '10':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '11':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '12':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '13':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '14':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '15':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '16':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '17':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '18':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '19':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '20':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '21':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '22':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '23':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '24':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '25':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '26':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '27':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '28':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '29':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '30':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '31':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '32':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '33':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '34':
        speedRun += 1
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек и нащупали там ключ 🗝. Вытащив его, вы сразу же попробовали проверить его на замке и... о чудо, дверь '
        'открылась! За дверью были ступеньки, ведущие вверх. Вы прошли по ним и вышли наружу. Получается, все это время вас держали в каких-то катакомбах... Хотя сейчас это '
        'уже неважно, ведь у вас получилось спастись, поздравляем!😀😀😀', reply_markup=keyboard)
        txt_hard_complete(m, speedRun)
        start(m)
    elif m.text == '35':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '36':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '37':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '38':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '39':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '40':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '41':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '42':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '43':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '44':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Ящичек выпустил усыпляющий газ и вы задремали. Но больше не проснулись...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '45':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '46':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '47':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм. Включилась сигнализация и на звук сбежалось штук семь местных охранников. '
        'Отбиваться было бесполезно...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '48':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и вам в руку что-то вкололи. Эта жидкость была очень едкая и боль стала '
        'распространяться по вашему телу. Это было последним, что вы запомнили, будучи в сознании...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '49':
        msg = bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но там не было абсолютно ничего. Возможно, стоит поискать в другом...', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)
    elif m.text == '50':
        bot.send_message(m.chat.id, 'Вы засунули руку в ящичек, но активировали какой-то механизм и ящички вместе с вами взлетели на воздух...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise11)


def txtChoise12(m):
    """
    Двенадцатый набор событий игры "Текстовый Квест", отвечающий за последствия принятия решения в комнате с дробовиком.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Осторожно забрать':
        bot.send_message(m.chat.id, 'Вы как можно тише подкрались к парню и постарались медленно забрать дробовик. Но он проснулся, резко направил его в вас и закончил ваше приключение...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == u'Сразу же стрелять':
        keyboard.add(*[types.KeyboardButton(name) for name in ['Пойти обратно', 'Проверить новую дверь']])
        msg = bot.send_message(m.chat.id, 'Вы как можно тише подкрались к парню и резко направили дробовик ему в голову, пока он держал его в руках. Одним меньше, но на этот '
        'шум прибежал громила. Вы оставили и в нем немало нефункциональных дырок. Не самая разумная трата боеприпасов, осталось их немного, но во всяком случае еще на одного '
        'точно хватит, да и с этими покончено. С дробовиком наперевес, вы пошли дальше. Но из этой комнаты есть другой выход, не только тот, через который вы пришли.', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise13)
    elif m.text == u'Не трогать его':
        bot.send_message(m.chat.id, 'Что ж, вы трусливо стали возвращаться, но именно в этот момент на кухню шел громила. Он голыми руками скрутил вас в бараний рог...', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise12)


def txtChoise13(m):
    """
    Тринадцатый набор событий игры "Текстовый Квест", отвечающий за последствия принятия решения после убийства двух парней.

    m -- сообщение пользователя
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Пойти обратно':
        bot.send_message(m.chat.id, 'Вы пошли обратно и увидели огромную металлическую дверь с орнаментом. Больше всего похоже на выход. Но вот беда. На двери замок... '
        'Хотя заботит ли вас это с вашим дробовиком? Вы уничтожили замок последним выстрелом и открыли дверь. Зайдя внутрь вы поняли, что все это время были в каком-то подвале, '
        'поскольку, только пройдя вверх по ступенькам вы вышли наружу. Поздравляем вас, все получилось!😀😀😀', reply_markup=keyboard)
        txt_easy_complete(m)
        start(m)
    elif m.text == u'Проверить новую дверь':
        bot.send_message(m.chat.id, 'Дверь открылась. Вы вошли. Вроде бы ничто не предвещало беды... Но она случилась. Вы услышали щелчок и стены, между которыми вы шли, '
        'резко соприкоснулись.', reply_markup=keyboard)
        txtTryAgain(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, txtChoise13)


def startEgg(m):
    """
    Функция игры "Разбей Яйцо", которая отвечает за запуск игры.

    m -- сообщение пользователя
    """
    random.seed(datetime.now())
    eggHP = 10
    currentEgg = 1
    clickPower = 1
    clickCriticalChance = 1
    clickCritMultiplier = 1.5
    clickCounter = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `egg` WHERE id = %s"
    cursor.execute(sql, (m.chat.id))
    len = 0
    for kek in cursor:
        len += 1
    if len != 0:
        sql = "SELECT * FROM `egg` WHERE id = %s"
        cursor.execute(sql, (m.chat.id))
        for element in cursor:
            eggHP = element['eggHP']
            currentEgg = element['currentEgg']
            clickPower = element['clickPower']
            clickCriticalChance = element['clickCriticalChance']
            clickCritMultiplier = element['clickCritMultiplier']
            clickCounter = element['clickCounter']
    else:
        sql = "INSERT INTO `egg` (id, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql, (m.chat.id, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))
    connection.commit()
    connection.close()
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(*[types.KeyboardButton(name) for name in ['Клик', 'Состояние', 'Глобальное Яйцо', 'Выход']])
    bot.send_message(m.chat.id, 'Приветствую! Предлагаю тебе разбивать яйца 🥚 и смотреть что там внутри 🐣. Увлекательно, да? А что ты хотел от бота?..', reply_markup=keyboard)
    eggState(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter)


def eggChoise(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter):
    """
    Функция игры "Разбей Яйцо", которая отвечает за последствия выбора пользователя.

    m -- сообщение пользователя
    eggHP -- здоровье текущего яйца
    currentEgg -- номер текущего яйца
    clickPower -- сила клика игрока
    clickCriticalChance -- критический шанс игрока
    clickCritMultiplier -- множитель критического клика
    clickCounter -- общее количество кликов
    """
    random.seed(datetime.now())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Клик' or m.text == u'Клац' or m.text == u'Тык' or m.text == u'Жмяк' or m.text == u'Тыц' or m.text == u'Пумсь' or m.text == u'Тумц' or m.text == u'Дынсь' or m.text == u'Пыпымсь' or m.text == u'к л и к':
        if currentEgg < 11:
            kek = random.randint(1, 10)
            if kek == 1:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Клик', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 2:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Клац', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 3:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тык', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 4:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Жмяк', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 5:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тыц', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 6:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Пумсь', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 7:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тумц', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 8:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Дынсь', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 9:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Пыпымсь', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            elif kek == 10:
                keyboard.add(*[types.KeyboardButton(name) for name in ['к л и к', 'Состояние', 'Глобальное Яйцо', 'Выход']])
            clickCounter += 1
            damag = clickPower
            if random.randint(1, 100) <= clickCriticalChance:
                damag *= clickCritMultiplier
                bot.send_message(m.chat.id, '💥Крит💥')
            eggHP -= damag
            if eggHP < 0:
                eggHP = 0
            msg = bot.send_message(m.chat.id, '-' + str(damag), reply_markup=keyboard)
            if eggHP == 0:
                if currentEgg == 1:
                    currentEgg += 1
                    eggHP = 50
                    clickCriticalChance += 4
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg1smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили шанс критического удара. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 2:
                    currentEgg += 1
                    eggHP = 100
                    clickCriticalChance += 5
                    clickCritMultiplier += 0.2
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg2smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили шанс и множитель критического удара. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 3:
                    currentEgg += 1
                    eggHP = 500
                    clickCriticalChance += 5
                    clickCritMultiplier += 0.3
                    clickPower += 0.5
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg3smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили не только шанс и множитель критического удара, но и силу своего клика. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 4:
                    currentEgg += 1
                    eggHP = 1000
                    clickCritMultiplier += 0.5
                    clickPower += 0.5
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg4smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили множитель критического удара и силу своего клика. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 5:
                    currentEgg += 1
                    eggHP = 2000
                    clickCriticalChance += 10
                    clickPower += 0.5
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg5smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили шанс критического удара и силу своего клика. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 6:
                    currentEgg += 1
                    eggHP = 5000
                    clickCriticalChance += 5
                    clickCritMultiplier += 0.5
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg6smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили шанс и множитель критического удара. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 7:
                    currentEgg += 1
                    eggHP = 10000
                    clickCriticalChance += 5
                    clickPower += 1
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg7smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили шанс критического удара и силу клика. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 8:
                    currentEgg += 1
                    eggHP = 50000
                    clickCriticalChance += 10
                    clickCritMultiplier += 1
                    clickPower += 1
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg8smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили не только шанс и множитель критического удара, но и силу клика. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 9:
                    currentEgg += 1
                    eggHP = 100000
                    clickCriticalChance = 100
                    clickCritMultiplier += 1
                    clickPower += 1
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg9smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Также вы увеличили множитель критического удара и силу клика, а также у вас теперь стопроцентный шанс крита. '
                    'Нажмите кнопку "Состояние", чтобы посмотреть на новое яйцо и на свои обновленные показатели.', reply_markup=keyboard)
                elif currentEgg == 10:
                    currentEgg += 1
                    bot.send_message(m.chat.id, 'Ура, вы разбили это яйцо 🐣! Вот, что в нем было:', reply_markup=keyboard)
                    bot.send_photo(m.chat.id, photo=open('./egg10smashed.png', 'rb'))
                    bot.send_message(m.chat.id, 'Не могу поверить, что говорю это, но у нас кончились яйца...', reply_markup=keyboard)
            egg_achievements_and_save(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter)
            bot.register_next_step_handler(msg, lambda m: eggChoise(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))
        else:
            keyboard.add(*[types.KeyboardButton(name) for name in ['Заново!', 'Не надо...']])
            msg = bot.send_message(m.chat.id, 'Все, стоп, остановись, хватит кликать, все! Конец! Игра окончена, яиц больше нет. Разбивать больше нечего! Можешь только начать заново.', reply_markup=keyboard)
            bot.register_next_step_handler(msg, lambda m: eggReset(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))
    elif m.text == u'Состояние':
        eggState(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter)
    elif m.text == u'Глобальное Яйцо':
        startGlobalEgg(m, clickCounter)
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: eggChoise(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))


def eggState(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter):
    """
    Функция игры "Разбей Яйцо", которая отвечает за вывод информации о текущем яйце и об игроке.

    m -- сообщение пользователя
    eggHP -- здоровье текущего яйца
    currentEgg -- номер текущего яйца
    clickPower -- сила клика игрока
    clickCriticalChance -- критический шанс игрока
    clickCritMultiplier -- множитель критического клика
    clickCounter -- общее количество кликов
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if currentEgg < 11:
        msg = bot.send_message(m.chat.id, '🥚 Яйцо № ' + str(currentEgg) + '\n🛡 Стойкость: ' + str(eggHP) + '\n\n🔨 Урон за клик: ' + str(clickPower) + '\n🔆 Шанс крита: ' + str(clickCriticalChance) + '%\n💥 Множитель крита: x' + str(clickCritMultiplier) + '\n\n💠 Общее количество кликов: ' + str(clickCounter), reply_markup=keyboard)
        if (currentEgg == 1):
            bot.send_photo(m.chat.id, photo=open('./egg1.png', 'rb'))
        elif (currentEgg == 2):
            bot.send_photo(m.chat.id, photo=open('./egg2.png', 'rb'))
        elif (currentEgg == 3):
            bot.send_photo(m.chat.id, photo=open('./egg3.png', 'rb'))
        elif (currentEgg == 4):
            bot.send_photo(m.chat.id, photo=open('./egg4.png', 'rb'))
        elif (currentEgg == 5):
            bot.send_photo(m.chat.id, photo=open('./egg5.png', 'rb'))
        elif (currentEgg == 6):
            bot.send_photo(m.chat.id, photo=open('./egg6.png', 'rb'))
        elif (currentEgg == 7):
            bot.send_photo(m.chat.id, photo=open('./egg7.png', 'rb'))
        elif (currentEgg == 8):
            bot.send_photo(m.chat.id, photo=open('./egg8.png', 'rb'))
        elif (currentEgg == 9):
            bot.send_photo(m.chat.id, photo=open('./egg9.png', 'rb'))
        elif (currentEgg == 10):
            bot.send_photo(m.chat.id, photo=open('./egg10.png', 'rb'))
        bot.register_next_step_handler(msg, lambda m: eggChoise(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))
    else:
        keyboard.add(*[types.KeyboardButton(name) for name in ['Заново!', 'Не надо...']])
        msg = bot.send_message(m.chat.id, 'Ну типа все, ты молодец, прошел игру. Хочешь начать сначала?', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: eggReset(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))


def eggReset(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter):
    """
    Функция игры "Разбей Яйцо", которая отвечает за перезапуск игры.

    m -- сообщение пользователя
    eggHP -- здоровье текущего яйца
    currentEgg -- номер текущего яйца
    clickPower -- сила клика игрока
    clickCriticalChance -- критический шанс игрока
    clickCritMultiplier -- множитель критического клика
    clickCounter -- общее количество кликов
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Заново!':
        eggHP = 10
        currentEgg = 1
        clickPower = 1
        clickCriticalChance = 1
        clickCritMultiplier = 1.5
        bot.send_message(m.chat.id, 'Что ж, разумный выбор. Все показатели сбросятся, но сохранится количество кликов. Игра начинается заново.', reply_markup=keyboard)
        connection = get_connection()
        cursor = connection.cursor()
        sql = "UPDATE `egg` SET `eggHP` = %s, `currentEgg` = %s, `clickPower` = %s, `clickCriticalChance` = %s, `clickCritMultiplier` = %s, `clickCounter` = %s WHERE `id` = %s"
        cursor.execute(sql, (eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter, m.chat.id))
        connection.commit()
        connection.close()
        startEgg(m)
    elif m.text == u'Не надо...':
        bot.send_message(m.chat.id, 'Как хочешь, но раз тебе в этой игре все равно делать нечего, то, пожалуй, я тебя отсюда выброшу.', reply_markup=keyboard)
        start(m)
    elif m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: eggReset(m, eggHP, currentEgg, clickPower, clickCriticalChance, clickCritMultiplier, clickCounter))


def startGlobalEgg(m, clickCounter):
    """
    Функция игры "Разбей Яйцо", которая отвечает за переход в режим глобального сражения всех игроков с одним яйцом.

    m -- сообщение пользователя
    clickCounter -- общее количество кликов за все время
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    random.seed(datetime.now())
    global globalEggHP
    eggNum = 0
    playerDamag = 0
    connection = get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `globalEgg`"
    cursor.execute(sql)
    for element in cursor:
        globalEggHP = element['globalEggHP']
        eggNum = element['eggNum']
    sql = "SELECT * FROM `egg` WHERE `id` = %s"
    cursor.execute(sql, (m.chat.id))
    for element in cursor:
        playerDamag = element['playerDamag']
    connection.commit()
    connection.close()
    if globalEggHP == 0 and eggNum == 0:
        bot.send_message(m.chat.id, 'К сожалению, сейчас нет глобального яйца... Попробуй позже.', reply_markup=keyboard)
        startEgg(m)
    elif globalEggHP == 0 and eggNum > 0:
        bot.send_message(m.chat.id, 'К сожалению, яйцо уже разбили. Вот что в нем было...', reply_markup=keyboard)
        bot.send_photo(m.chat.id, photo=open('./globalEggSmashed.jpg', 'rb'))
        startEgg(m)
    else:
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add(*[types.KeyboardButton(name) for name in ['Клик', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
        bot.send_message(m.chat.id, 'Это режим глобального яйца. Здесь все игроки пытаются уничтожить одно яйцо. Все игроки имеют одинаковые атрибуты, но клики, '
        'совершенные в этом режиме, добавляются к вашему общему количеству кликов.', reply_markup=keyboard)
        globalEggState(m, clickCounter, playerDamag)


def globalEggState(m, clickCounter, playerDamag):
    """
    Функция игры "Разбей Яйцо", которая отвечает за вывод информации о текущем глобальном яйце и об игроке.

    m -- сообщение пользователя
    clickCounter -- общее количество кликов
    playerDamag -- урон, нанесенный игроком
    """
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    msg = bot.send_message(m.chat.id, '🥚 Глобальное яйцо!\n🛡 Стойкость: ' + str(globalEggHP) + '\n\n🔨 Урон за клик: 1\n🔆 Шанс крита: 20%\n💥 Множитель крита: x2\n\n💠 Общее количество кликов: ' + str(clickCounter) + '\nВаш вклад (нанесенный урон): ' + str(playerDamag), reply_markup=keyboard)
    bot.send_photo(m.chat.id, photo=open('./globalEgg.jpg', 'rb'))
    bot.register_next_step_handler(msg, lambda m: globalEggChoise(m, clickCounter, playerDamag))


def globalEggChoise(m, clickCounter, playerDamag):
    """
    Функция игры "Разбей Яйцо", которая отвечает за вывод информации о текущем глобальном яйце и об игроке.

    m -- сообщение пользователя
    clickCounter -- общее количество кликов
    playerDamag -- урон, нанесенный игроком
    """
    global globalEggHP
    killedTheEgg = 0
    random.seed(datetime.now())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    if m.text == u'Клик' or m.text == u'Клац' or m.text == u'Тык' or m.text == u'Жмяк' or m.text == u'Тыц' or m.text == u'Пумсь' or m.text == u'Тумц' or m.text == u'Дынсь' or m.text == u'Пыпымсь' or m.text == u'к л и к':
        if globalEggHP >= 0:
            kek = random.randint(1, 10)
            if kek == 1:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Клик', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 2:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Клац', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 3:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тык', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 4:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Жмяк', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 5:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тыц', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 6:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Пумсь', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 7:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Тумц', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 8:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Дынсь', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 9:
                keyboard.add(*[types.KeyboardButton(name) for name in ['Пыпымсь', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            elif kek == 10:
                keyboard.add(*[types.KeyboardButton(name) for name in ['к л и к', 'Состояние', 'Локальное Яйцо', 'Таблица лидеров', 'Выход']])
            clickCounter += 1
            damag = 1
            if random.randint(1, 100) <= 20:
                damag *= 2
                bot.send_message(m.chat.id, '💥Крит💥')
            globalEggHP -= damag
            playerDamag += damag
            if globalEggHP < 0:
                globalEggHP = 0
            msg = bot.send_message(m.chat.id, '-' + str(damag), reply_markup=keyboard)
            if globalEggHP == 0:
                killedTheEgg = 1
                bot.send_message(m.chat.id, 'УРА! Твой удар оказался решающим и яйцо разбито. Вот что в нем было...', reply_markup=keyboard)
                bot.send_photo(m.chat.id, photo=open('./globalEggSmashed.jpg', 'rb'))
                bot.send_message(m.chat.id, 'А теперь можешь вернуться к обычной игре. Позже появится другое глобальное яйцо.', reply_markup=keyboard)
                startEgg(m)
            else:
                bot.register_next_step_handler(msg, lambda m: globalEggChoise(m, clickCounter, playerDamag))
            connection = get_connection()
            cursor = connection.cursor()
            sql = "UPDATE `globalEgg` SET `globalEggHP` = %s"
            cursor.execute(sql, (globalEggHP))
            connection.commit()
            connection.close()
            global_egg_achievements_and_save(m, clickCounter, playerDamag, killedTheEgg)
        else:
            bot.send_message(m.chat.id, 'К сожалению, яйцо уже разбили. Вот что в нем было...', reply_markup=keyboard)
            bot.send_photo(m.chat.id, photo=open('./globalEggSmashed.jpg', 'rb'))
            bot.send_message(m.chat.id, 'А теперь можешь вернуться к обычной игре. Позже появится другое глобальное яйцо.', reply_markup=keyboard)
            startEgg(m)
    elif m.text == u'Состояние':
        globalEggState(m, clickCounter, playerDamag)
    elif m.text == u'Локальное Яйцо':
        startEgg(m)
    elif m.text == u'Таблица лидеров':
        msg = bot.send_message(m.chat.id, global_egg_leaders(m), reply_markup=keyboard, parse_mode= "Markdown")
        bot.register_next_step_handler(msg, lambda m: globalEggChoise(m, clickCounter, playerDamag))
    elif m.text == u'Выход' or m.text == '/start':
        start(m)
    else:
        msg = bot.send_message(m.chat.id, 'Вам дали варианты - выбирайте из них :)', reply_markup=keyboard)
        bot.register_next_step_handler(msg, lambda m: globalEggChoise(m, clickCounter, playerDamag))


bot.polling(none_stop=True)
#bot.infinity_polling(none_stop=True)