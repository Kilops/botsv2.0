import unittest
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from datetime import datetime
import JijaBot

class TestStringMethods(unittest.TestCase):

    def test_create_keyboard_basic(self):
        keyboard = VkKeyboard(one_time=False)
        keyboard.add_button('Добавить расписание', color=VkKeyboardColor.POSITIVE)
        keyboard.add_button('Вывести расписание', color=VkKeyboardColor.POSITIVE)

        keyboard.add_line()  # Переход на вторую строку
        keyboard.add_button('Прикольные видео', color=VkKeyboardColor.NEGATIVE)

        keyboard.add_line()
        keyboard.add_button('Добавить событие', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('Вывести список событий', color=VkKeyboardColor.PRIMARY)
        keyboard_menu = keyboard.get_keyboard()
        keyboard_test = JijaBot.create_keyboard_basic('меню')
        self.assertEqual(keyboard_test, keyboard_menu)

        keyboard = VkKeyboard(one_time=False)
        keyboard_close = keyboard.get_empty_keyboard()
        keyboard_test = JijaBot.create_keyboard_basic('закрыть')
        self.assertEqual(keyboard_test, keyboard_close)

    def test_get_time(self):
        check_string = '12:00'
        valid_date = datetime.strptime(check_string, '%H:%M').time()
        time_test = JijaBot.get_time('12:00')
        self.assertEqual(time_test, valid_date)
        time_test = JijaBot.get_time('fejghjsgjrugh')
        self.assertEqual(time_test, '0')

    def test_create_week_buttons(self):
        keyboard_Input = VkKeyboard(one_time=False)
        keyboard_Input.add_button('Чётная', color=VkKeyboardColor.PRIMARY)
        keyboard_Input.add_button('Нечётная', color=VkKeyboardColor.PRIMARY)

        keyboard_Input = keyboard_Input.get_keyboard()
        keyboard_test = JijaBot.create_week_buttons()
        self.assertEqual(keyboard_test, keyboard_Input)

    def test_create_timetable_buttons(self):
        keyboard_Input = VkKeyboard(one_time=False)

        keyboard_Input.add_button('Понедельник', color=VkKeyboardColor.PRIMARY)
        keyboard_Input.add_button('Вторник', color=VkKeyboardColor.PRIMARY)

        keyboard_Input.add_line()
        keyboard_Input.add_button('Среда', color=VkKeyboardColor.PRIMARY)
        keyboard_Input.add_button('Четверг', color=VkKeyboardColor.PRIMARY)

        keyboard_Input.add_line()
        keyboard_Input.add_button('Пятница', color=VkKeyboardColor.PRIMARY)
        keyboard_Input.add_button('Суббота', color=VkKeyboardColor.PRIMARY)
        keyboard_Input = keyboard_Input.get_keyboard()
        keyboard_test = JijaBot.create_timetable_buttons()
        self.assertEqual(keyboard_test, keyboard_Input)


if __name__ == '__main__':
    unittest.main()