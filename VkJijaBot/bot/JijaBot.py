from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
import vk_api
from datetime import datetime, date, timedelta
import random
import pymysql.cursors
import requests
import time
import threading
import jok
import pytz
import topsecret

"""

 Это программа Jija бота вконтакте

"""

token = topsecret.get_token()
vk_session = vk_api.VkApi(token=token)

session_api = vk_session.get_api()
longpoll = VkLongPoll(vk_session)

# Ввод данных в бд
def select_schedule_from_database(id, week, day):
    """
       Считывание расписания с базы данных

       :type id: int
       :param id: id пользователя

       :type week: int
       :param week: неделя

       :type day: int
       :param day: день недели

       :returns: string -- Возвращает расписание

    """


    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `timetable_timetable` WHERE id = %s and day = %s and week = %s ORDER BY start"
    cursor.execute(sql, (id, day, week))
    flag = 0
    mode_send = ''
    for i in cursor:
        flag = 1
        mode_send = mode_send + "Предмет: "
        mode_send = mode_send + i['subject']
        mode_send = mode_send + '\n'
        mode_send = mode_send +  "Тип предмета: "
        mode_send = mode_send + i['type_subject']
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Начало: "
        mode_send = mode_send + str(i['start'])
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Окончание: "
        mode_send = mode_send + str(i['timeout'])
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Кабинет: "
        mode_send = mode_send + i['office']
        mode_send = mode_send + '\n\n'
        if(len(mode_send) > 2048):
            send_message(vk_session, 'user_id', id, message=mode_send, keyboard=create_keyboard_basic('меню'))
            mode_send = ''
            flag = 0
    # Проверяем точно ли есть такая запись
    if cursor.fetchall() == ():
        mode_send = "Занятий в этот день нет. Либо вы забыли их внести"
        send_message(vk_session, 'user_id', id, message=mode_send, keyboard=create_keyboard_basic('меню'))
    if flag == 1:
        send_message(vk_session, 'user_id', id, message=mode_send, keyboard=create_keyboard_basic('меню'))
    connection.commit()
    connection.close()
    return

def select_act_from_database(id, date):
    """
       Считывание списка мероприятий с базы данных
       :type id: int
       :param id: id пользователя

       :type week: int
       :param week: неделя

       :type day: int
       :param day: день недели

       :returns: string -- Возвращает список мероприятий
    """

    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `jijkaAction` WHERE id = %s and date = %s"
    cursor.execute(sql, (id, date))
    mode_send = ''
    for i in cursor:
        mode_send = "Дата (ГГГГ-ММ-ДД): " + str(i['date']) + '\n'
        mode_send = mode_send + "Название мероприятия: "
        mode_send = mode_send + i['nameAct']
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Время: "
        mode_send = mode_send + str(i['timeAct'])
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Описание мероприятия: "
        mode_send = mode_send + i['defAct']
        mode_send = mode_send + '\n------------------------'
    #Проверяем точно ли есть такая запись
    if cursor.fetchall() == ():
        mode_send = ""

    connection.commit()
    connection.close()
    return mode_send



# Ввод данных в бд
def insert_schedule_into_database(id, week, day, timestart, timeend, subject, inssubject, office):
    """
       Запись данных в таблицу timetable_timetable

       :type id: int
       :param id: id пользователя

       :type week: int
       :param week: неделя

       :type timestart: time
       :param timestart: время начала занятия

       :type timeend: time
       :param timeend: время окончания занятия

       :type subject: string
       :param subject: название предмета

       :type inssubject: string
       :param inssubject: тип предмета

       :type office: string
       :param office: номер кабинета
    """
    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "INSERT INTO `timetable_timetable`(`id`, `week`, `day`, `start`, `timeout`, `subject`, `type_subject`, `office`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(sql, (id, week, day, timestart, timeend, subject, inssubject, office))
    connection.commit()
    connection.close()

def insert_act_into_database(user_id, date, time, name_act, def_act):
    """
       Считывание данных с базы данных

       :type user_id: int
       :param user_id: id пользователя

       :type week: int
       :param week: неделя

       :type day: int
       :param day: день недели

       :type time: time
       :param time: Время начала мероприятия

       :type name_act: string
       :param name_act: Название мероприятия

       :type def_act: string
       :param def_act: Описание мероприятия
    """

    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "INSERT INTO `jijkaAction`(`id`, `timeAct`, `nameAct`, `defAct`, `date`) VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(sql, (user_id, time, name_act, def_act, date))
    connection.commit()
    connection.close()

    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "INSERT INTO `for_op`(`id`, `timeAct`, `nameAct`, `defAct`, `date`) VALUES (%s, %s, %s, %s, %s)"
    cursor.execute(sql, (user_id, time, name_act, def_act, date))
    connection.commit()
    connection.close()

# Кнопочки недели
def create_week_buttons():
    """

    Функция для вывода кнопок недель

    :return: Возвращает кнопки
    """

    keyboard_Input = VkKeyboard(one_time=False)

    keyboard_Input.add_button('Чётная', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Нечётная', color=VkKeyboardColor.PRIMARY)

    keyboard_Input = keyboard_Input.get_keyboard()
    return keyboard_Input

# Кнопочки дней недели
def create_timetable_buttons():
    """
        Функция для создания кнопок для ввода дней недели

        :return: Возвращает кнопки
    """

    keyboard_Input = VkKeyboard(one_time=False)

    keyboard_Input.add_button('Понедельник', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Вторник', color=VkKeyboardColor.PRIMARY)

    keyboard_Input.add_line()
    keyboard_Input.add_button('Среда', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Четверг', color=VkKeyboardColor.PRIMARY)

    keyboard_Input.add_line()
    keyboard_Input.add_button('Пятница', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Суббота', color=VkKeyboardColor.PRIMARY)

    keyboard_Input = keyboard_Input.get_keyboard()

    return keyboard_Input

# Кнопочки меню
def create_keyboard_basic(response):
    """
        Функция для вывода кнопок бота

        :type response: string
        :param response: Данные котоыре вводит пользователь

        :return: Возвращает кнопки
    """

    keyboard = VkKeyboard(one_time=False)

    if response.lower() == 'меню':

        keyboard.add_button('Добавить расписание', color=VkKeyboardColor.POSITIVE)
        keyboard.add_button('Вывести расписание', color=VkKeyboardColor.POSITIVE)

        keyboard.add_line()  # Переход на вторую строку
        keyboard.add_button('Прикольные видео', color=VkKeyboardColor.NEGATIVE)

        keyboard.add_line()
        keyboard.add_button('Добавить событие', color=VkKeyboardColor.PRIMARY)
        keyboard.add_button('Вывести список событий', color=VkKeyboardColor.PRIMARY)

    elif response.lower() == 'закрыть' or response.lower() == 'привет':
        return keyboard.get_empty_keyboard()

    else:
        keyboard.add_button('меню', color=VkKeyboardColor.POSITIVE)

    keyboard = keyboard.get_keyboard()
    return keyboard

def send_message(vk_session, id_type, id, message=None, attachment=None, keyboard=None):
    """
        Функция для отправки сообщения пользователю

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type id_type: string
        :param id_type: Название метода

        :type id:  id пользователя
        :param id: Переменная данной сессии

        :type message:  string
        :param message: сообщение

        :type keyboard:  VkKeyboard
        :param keyboard: клавиатура

    """
    if message == "":
        return ""
    vk_session.method('messages.send',
                      {id_type: id, 'message': message, 'random_id': random.randint(-2147483648, +2147483648),
                       "attachment": attachment, 'keyboard': keyboard})

# Выбор дня чтобы не держать этот хлам в основной функции
def keyboard_time_v2():
    """
        Функция для вывода кнопок типа предмета

        :return: Возвращает кнопки
    """
    keyboard_Input = VkKeyboard(one_time=False)
    keyboard_Input.add_button('Практика', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Лекция', color=VkKeyboardColor.POSITIVE)
    keyboard_Input = keyboard_Input.get_keyboard()
    return keyboard_Input

def keyboard_start():
    """
        Функция для вывода кнопок начала работы

        :return: Возвращает кнопки
    """
    keyboard_Input = VkKeyboard(one_time=False)
    keyboard_Input.add_button('Меню', color=VkKeyboardColor.PRIMARY)
    keyboard_Input.add_button('Справка', color=VkKeyboardColor.POSITIVE)
    keyboard_Input = keyboard_Input.get_keyboard()
    return keyboard_Input

def get_time(response):
    """
        Функция для вывода сообщения о том как вводить время

        :type response: string
        :param response: время

        :return: 0 если это не время и time -- если полученные данные время
    """
    try:
        valid_date = datetime.strptime(response, '%H:%M').time()
        return valid_date
    except ValueError:
        return '0'

       # valid_date = datetime.strptime(response, '%H:%M:%S').time()


def get_Week(vk_session, user_id):
    """
        Функция для вввода дня недели

        :type response: string
        :param response: время

        :return: 0 если это не время и time -- если полученные данные время
    """
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                if response == "чётная":
                    keyboard = create_timetable_buttons()
                    send_message(vk_session, 'user_id', event.user_id, message="Выберите день недели",
                                 keyboard=keyboard)
                    return 2
                elif response == "нечётная":
                    keyboard = create_timetable_buttons()
                    send_message(vk_session, 'user_id', event.user_id, message="Выберите день недели",
                                 keyboard=keyboard)
                    return 1
                else:
                    send_message(vk_session, 'user_id', user_id,
                                 message='Такой недели нет. \n Попробуй снова')

def get_Week_for_act(vk_session, user_id):
    """
        Функция для вввода дня недели для события

        :type response: string
        :param response: время

        :return: 0 если это не время и time -- если полученные данные время
    """
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                if response == "чётная":
                    return 2
                elif response == "нечётная":
                    return 1
                else:
                    send_message(vk_session, 'user_id', user_id,
                                 message='Такой недели нет. \n Попробуй снова')

def get_date(vk_session, user_id):
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                date1 = datetime.strptime(response, '%Y:%m:%d')
                return date1


def get_day(vk_session, user_id):
    """
        Функция для получения данных о дней недели

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                if response == 'понедельник':
                    return 1
                elif response == 'вторник':
                    return 2
                elif response == 'среда':
                    return 3
                elif response == 'четверг':
                    return 4
                elif response == 'пятница':
                    return 5
                elif response == 'суббота':
                    return 6
                else:
                    send_message(vk_session, 'user_id', user_id,
                                 message='Такого дня недели нет.')

def get_schedule (vk_session, user_id):
    """
        Функция для получения данных о расписание пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    keyboard = create_week_buttons()
    send_message(vk_session, 'user_id', user_id,
                 message='Выберите неделю',
                 keyboard=keyboard
                 )
    week = get_Week(vk_session, user_id)
    day = get_day(vk_session, user_id)
    select_schedule_from_database(user_id, week, day)

def get_act(vk_session, user_id):
    """
        Функция для получения данных о мероприятиях пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    #keyboard = create_week_buttons()
    send_message(vk_session, 'user_id', user_id,
                 message='События на неделю: \n'
                 )

    cur_date = date.today()
    days = (1, 2, 3, 4, 5, 6)
    for elem in days:
        counter = timedelta(elem)
        send_message(vk_session, 'user_id', user_id, message=select_act_from_database(user_id, cur_date + counter), keyboard=create_keyboard_basic('меню'))

def get_timeStart(vk_session, user_id):
    """
        Функция для получения данных о начале занятий

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message='Начало занятия. Введите время в формате H:M \n например  00:00', keyboard=create_keyboard_basic('закрыть'))
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                check = get_time(response)
                if check == '0':
                    send_message(vk_session, 'user_id', event.user_id,
                                 message='Ошибка время должно быть в формате HH:MM\n Повторите ввод')
                else:
                    return check

def get_time_act(vk_session, user_id):
    """
        Функция для получения данных о времени мероприятия

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message='Введите время мероприятия в формате H:M \n например  00:00')
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                check = get_time(response)
                if check == '0':
                    send_message(vk_session, 'user_id', event.user_id,
                                 message='Ошибка время должно быть в формате HH:MM\n Повторите ввод')
                else:
                    return check

def get_timeEnd(vk_session, user_id):
    """
        Функция для получения данных об окончание занятия

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message='Окончание занятия. Введите время в формате H:M \n например  00:00')
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text.lower()
                check = get_time(response)
                if check == '0':
                    send_message(vk_session, 'user_id', event.user_id,
                                 message='Ошибка время должно быть в формате HH:MM\n Повторите ввод')
                else:
                    return check

def add_schedule_info (vk_session, user_id, week, day):
    timeStart = get_timeStart(vk_session, user_id)
    timeEnd = get_timeEnd(vk_session, user_id)
    subject = get_subject(vk_session, user_id)
    inssubject = get_inssubject(vk_session, user_id)
    office = get_office(vk_session, user_id)
    insert_schedule_into_database(user_id, week, day, timeStart, timeEnd, subject, inssubject, office)
    keyboard_Input_schedule = VkKeyboard(one_time=False)
    keyboard_Input_schedule.add_button('Да', color=VkKeyboardColor.PRIMARY)
    keyboard_Input_schedule.add_button('Нет', color=VkKeyboardColor.NEGATIVE)
    keyboard_Input_schedule = keyboard_Input_schedule.get_keyboard()
    send_message(vk_session, 'user_id', user_id, message='Данные приняты\n Хотите ли вы продолжить ввод? да\нет', keyboard=keyboard_Input_schedule)


def add_schedule (vk_session, user_id):
    """
        Функция для отправки данных в базу данных

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    keyboard = create_week_buttons()
    send_message(vk_session, 'user_id', user_id,
                 message='Выберите неделю',
                 keyboard=keyboard
                 )
    week = get_Week(vk_session, user_id)
    day = get_day(vk_session, user_id)
    add_schedule_info (vk_session, user_id, week, day)
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                if event.text.lower() == 'да':
                    add_schedule_info (vk_session, user_id, week, day)
                elif event.text.lower() == 'нет':
                    send_message(vk_session, 'user_id', user_id, message='Ввод завершён', keyboard=keyboard_start())
                    return
                else:
                    send_message(vk_session, 'user_id', user_id, message='Такой команды нет', keyboard=create_keyboard_basic('закрыть'))

def get_subject(vk_session, user_id):
    """
        Функция для получения данных о предмете пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """
    send_message(vk_session, 'user_id', user_id, message='Введите название предмета', keyboard=create_keyboard_basic('закрыть'))
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text
                if len(response) < 100:
                    return response
                else:
                    send_message(vk_session, 'user_id', user_id, message='Уменьшите кол-во символов в название предмета.\n Название предмета не должно превышать 100 символов', keyboard=create_keyboard_basic('закрыть'))

def get_name_act(vk_session, user_id):
    """
        Функция для получения данных о название мероприятия пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message='Введите название мероприятия', keyboard=create_keyboard_basic('закрыть'))
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text
                return response

def get_inssubject(vk_session, user_id):
    """
        Функция для получения данных о типе предмета пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message="Выберите или введите тип предмета", keyboard = keyboard_time_v2())
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text
                if len(response) < 100:
                    return response
                else:
                    send_message(vk_session, 'user_id', user_id, message='Уменьшите кол-во символов.\n В типе предмета кол-во символов не должно превышать 100 символов', keyboard = keyboard_time_v2())


def get_def_act(vk_session, user_id):
    """
        Функция для получения данных о дополнительной информации о мероприятии пользователя

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """

    send_message(vk_session, 'user_id', user_id, message='Введите описание мероприятия', keyboard=create_keyboard_basic('закрыть'))
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text
                return response

def get_office(vk_session, user_id):
    """
        Функция для получения данных о номере кабинета

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """
    send_message(vk_session, 'user_id', user_id, message='Введите номер кабинета', keyboard=create_keyboard_basic('закрыть'))
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW and user_id == event.user_id:
            if event.from_user and not (event.from_me):
                response = event.text
                if len(response) < 10:
                    return response
                else:
                    send_message(vk_session, 'user_id', user_id, message='Уменьшите кол-во символов.\n В номере кабинета кол-во символо не должно превышать 10', keyboard=create_keyboard_basic('закрыть'))


def add_act(vk_session, user_id):
    """
        Функция для добавления мероприятия в базу данных

        :type vk_session: VkApi
        :param vk_session: Переменная данной сессии

        :type user_id: string
        :param user_id: id пользователя

    """
    send_message(vk_session, 'user_id', user_id,
                 message='Введите дату в формате ГГГГ:ММ:ДД',
                 keyboard=create_keyboard_basic('закрыть')
                )
    date = get_date(vk_session, user_id)
    time = get_time_act(vk_session, user_id)
    name_act = get_name_act(vk_session, user_id)
    def_act = get_def_act(vk_session, user_id)
    insert_act_into_database(user_id, date, time, name_act, def_act)
    send_message(vk_session, 'user_id', user_id, message='Данные приняты', keyboard=create_keyboard_basic('меню'))


def waitingFoSomething():
    connection = topsecret.get_connection()
    cursor = connection.cursor()
    sql = "SELECT * FROM `for_op` ORDER BY date, timeAct ASC"
    cursor.execute(sql, ())
    mode_send = ''
    for i in cursor:
        hours = 0
        time_delta = 0
        user_id = i['id']
        date_act = i['date']
        time_act = i['timeAct']
        mode_send = "Дата (ГГГГ-ММ-ДД): " + str(i['date']) + '\n'
        mode_send = mode_send + "Название мероприятия: "
        mode_send = mode_send + i['nameAct']
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Время: "
        mode_send = mode_send + str(i['timeAct'])
        mode_send = mode_send + '\n'
        mode_send = mode_send + "Описание мероприятия: "
        mode_send = mode_send + i['defAct']
        mode_send = mode_send + '\n------------------------'
        #Проверяем точно ли есть такая запись
        time_delta = (date_act - date.today()).days
        now = datetime.now().time()
        time_str = now.strftime("%H:%M")
        time = datetime.strptime(time_str, '%H:%M').time()
        startTime = (datetime.min + time_act).time()
        if(time.hour > startTime.hour):
            hours = 24 - time.hour + startTime.hour
            time_delta -= 1
        else:
            hours = startTime.hour - time.hour
        if (time_delta == 0 and hours == 7):
            send_message(vk_session, 'user_id', user_id, message='Скоро что-то будет!', keyboard=create_keyboard_basic('закрыть'))
            send_message(vk_session, 'user_id', user_id, message=mode_send, keyboard=create_keyboard_basic('закрыть'))
            connection = topsecret.get_connection()
            cursor = connection.cursor()
            sql = "DELETE FROM `for_op` ORDER BY date, timeAct ASC LIMIT 1"
            cursor.execute(sql, ())
            connection.commit()
            connection.close()
            cursor.close()
    if cursor.fetchall() == ():
        mode_send = ""
        return
    connection.commit()
    connection.close()
    cursor.close()
    #send_message(vk_session, 'user_id', user_id, message=select_act_from_database(user_id, date_act), keyboard=create_keyboard_basic('закрыть'))
    #vremya_v_secundah = (date - date.today()).seconds

"""
+ user[0]['first_name'] +  ' ' + user[0]['last_name']
"""


def main():
    """
        Основная функция бота
    """


    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            print('Сообщение пришло в: ' + str(datetime.strftime(datetime.now(), "%H:%M:%S")))
            print('Текст сообщения: ' + str(event.text))
            print(event.user_id)
            print('---------------------------------')
            response = event.text
            keyboard = create_keyboard_basic(response)
            if event.from_user and not (event.from_me):
                if response.lower() == 'привет':
                    user = vk_session.method("users.get", {"user_ids": event.user_id})
                    text_hello = 'Приветствую'
                    keyboard=keyboard_start()
                    send_message(vk_session, 'user_id', event.user_id,
                                 message=text_hello,
                                 keyboard=keyboard
                                 )

                elif response.lower() == 'справка':

                    keyboard=keyboard_start()
                    text_help = ''
                    text_help = text_help + 'Руководство пользователя\n'
                    text_help = text_help + 'Данная программа предназначена для использования в социальной сети вконтакте \n'
                    text_help = text_help + 'В возможности Jija бот входит: \n'
                    text_help = text_help + '1)Ввод и вывод расписания \n'
                    text_help = text_help + '2)Создание и вывод списка мероприятий \n'
                    text_help = text_help + '3)Оповещение о предстоящем мероприятии в виде сообщения в вк \n\n\n'
                    text_help = text_help + 'Вы можете ввести команду или просто нажать на кнопку ниже\n\n'
                    send_message(vk_session, 'user_id', event.user_id,
                                 message=text_help,
                                 keyboard=keyboard
                                 )
                    text_help_2 = ''
                    text_help_2 = text_help_2 + 'Доступные команды: \n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    text_help_2 = text_help_2 + 'Команда:меню\n'
                    text_help_2 = text_help_2 + 'Открывает меню бота\n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    text_help_2 = text_help_2 + 'Команда:вывести расписание\n'
                    text_help_2 = text_help_2 + 'Позволяет вывести расписание, которое вы ввели раньше\n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    text_help_2 = text_help_2 + 'Команда:"вывести список событий"\n'
                    text_help_2 = text_help_2 + 'Позволяет вывести список событий, которое вы ввели раньше\n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    text_help_2 = text_help_2 + 'Команда:"добавить событие"\n'
                    text_help_2 = text_help_2 + 'Позволяет добавить событие\n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    text_help_2 = text_help_2 + 'Команда:"вывести расписание"\n'
                    text_help_2 = text_help_2 + 'Позволяет добавить расписание\n'
                    text_help_2 = text_help_2 + '---------------------------------------------------\n'
                    send_message(vk_session, 'user_id', event.user_id,
                                message=text_help_2,
                                keyboard=keyboard
                                )

                elif response.lower() == 'меню':
                    keyboard = create_keyboard_basic(response)
                    send_message(vk_session, 'user_id', event.user_id,
                                 message='Меню',
                                 keyboard=keyboard
                                 )
                elif response.lower() == 'вывести расписание':
                    get_schedule(vk_session, event.user_id)
                elif response.lower() == 'добавить расписание':
                    add_schedule(vk_session, event.user_id)
                elif response.lower() == 'прикольные видео':
                    random.seed(datetime.now())
                    src = jok.jokes()
                    rand_seed = random.randrange(1, len(src))
                    send_message(vk_session, 'user_id', event.user_id,
                                message=src[rand_seed])

                elif response.lower() == 'вывести список событий':
                    get_act(vk_session, event.user_id)
                elif response.lower() == 'добавить событие':
                    add_act(vk_session, event.user_id)
                elif response.lower() == 'закрыть':
                    keyboard=create_keyboard_basic('закрыть')
                    send_message(vk_session, 'user_id', event.user_id, message="До свидания",
                                 keyboard=keyboard)
                else:
                    keyboard=keyboard_start()
                    text_help = ''
                    text_help = text_help + 'Вы ввели несуществующую команду\n'
                    text_help = text_help + 'Возможные команды для начала работы:\n'
                    text_help = text_help + '1)"Меню" - вывод кнопок на экран.\n'
                    text_help = text_help + '2)"Справка"- выводит информацию функционале о бота.\n'
                    text_help = text_help + '3)"Закрыть" - закрывает клавиатуру. Если она открыта.\n'
                    text_help = text_help + 'Вы можете как ввести команду, так и просто нажать на кнопку ниже\n'
                    send_message(vk_session, 'user_id', event.user_id,
                                 message=text_help,
                                 keyboard=keyboard
                                 )

def waiting():
    while(1):
        waitingFoSomething()
        now = datetime.now().time()
        time_str = now.strftime("%H:%M")
        print('я живой\n' + time_str)
        time.sleep(1800)


if __name__ == '__main__':
    myThreadik = threading.Thread(target=waiting)
    myThreadik.start()
    main()
